# Notices for R package 'rj.gd' - Graphics Device for RJSrv for GNU R


## Declared Project Licenses

See file [LICENSE.note](pkg/LICENSE.note).

A copy of the licenses is contained in the file [LICENSE](pkg/LICENSE).


## Third-party Content

### JavaGD

The package includes content derived from software developed by Simon Urbanek <simon.urbanek@r-project.org>
and others as part of the JavaGD project.
