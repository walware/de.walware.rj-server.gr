//
//  JavaGD - Java Graphics Device for R
//
//  Copyright (c) 2004, 2025 Simon Urbanek and others.
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation;
//  version 2.1 of the License.
//  
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//  
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Simon Urbanek <simon.urbanek@r-project.org> - initial API and implementation (org.rosuda.javagd)
// Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation for RJ
//

package de.walware.rj.server.javagd;

import java.awt.Graphics;


public class JGDBufferedPanel extends JGDPanel {
    public long lastSyncAttempt, lastPaint;
    public long syncDelay = 200;
    public long refreshGranularity = 400;
    
    public boolean updateDelayed = false;
    public boolean updateLocked = false;
    public Refresher ref;
    
    public JGDBufferedPanel(double w, double h) {
        this((int)w, (int)h);
    }

    public JGDBufferedPanel(int w, int h) {
        super(w, h);
        (this.ref = new Refresher(this)).start();
    }        

    public void superPC(Graphics g) {
        super.paintComponent(g);
    }

    @Override
	public void closeDisplay() { super.closeDisplay(); if (this.ref!=null) {
		this.ref.active=false;
	} this.ref=null; }

    @Override
	public synchronized void syncDisplay(boolean finish) {
        //System.out.println("Sync("+finish+")");
        if (!finish) {
            this.lastSyncAttempt=System.currentTimeMillis();
            this.updateLocked=true;
            return;
        }

        this.updateLocked=false;
        if (System.currentTimeMillis()-this.lastSyncAttempt>this.syncDelay) {
            //System.out.println("Sync allowed, ("+(System.currentTimeMillis()-lastSyncAttempt)+" ms)");
            repaint();
            this.updateDelayed=false;
        }
		else {
			this.updateDelayed=true;
		}
        this.lastSyncAttempt=System.currentTimeMillis();
    }
    
    @Override
	public synchronized void paintComponent(Graphics g) {
        //System.out.println("BP: paint");
        superPC(g);
        this.lastPaint=this.lastSyncAttempt=System.currentTimeMillis();        
    }
    
    class Refresher extends Thread {
        JGDBufferedPanel c;
        boolean active;

        public Refresher(JGDBufferedPanel c) {
            this.c=c;
        }

        @Override
		public void run() {
            this.active=true;
            while (this.active) {
                try {
                    Thread.sleep(JGDBufferedPanel.this.refreshGranularity);
                } catch (Exception e) {}
                if (!this.active) {
					break;
				}
                //System.out.println("BP: Refresher: delayed="+c.updateDelayed+", locked="+c.updateLocked+", delta="+(System.currentTimeMillis()-c.lastSyncAttempt));
                if (this.c.updateDelayed && !this.c.updateLocked && (System.currentTimeMillis()-this.c.lastSyncAttempt>this.c.syncDelay)) {
					this.c.syncDisplay(true);
				}
            }
            this.c=null;
        }
    }
    
}
