//
//  JavaGD - Java Graphics Device for R
//
//  Created by Simon Urbanek on Thu Aug 05 2004.
//  Copyright (c) 2004, 2025 Simon Urbanek and others.
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation;
//  version 2.1 of the License.
//  
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//  
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Simon Urbanek <simon.urbanek@r-project.org> - initial API and implementation (org.rosuda.javagd)
// Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation for RJ
//

package de.walware.rj.server.javagd;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.lang.reflect.Method;
import java.util.Vector;

import javax.swing.JPanel;


public class JGDPanel extends JPanel implements GDContainer, MouseListener {
    Vector l;
    boolean listChanged;
    public static boolean forceAntiAliasing=true;
    GDState gs;
    Dimension lastSize;
    public int devNr=-1;
    Dimension prefSize;

    public JGDPanel(double w, double h) {
        this((int)w, (int)h);
    }

    public JGDPanel(int w, int h) {
        super(true);
        setOpaque(true);
        setSize(w, h);
        this.prefSize=new Dimension(w,h);
        this.l=new Vector();
        this.gs=new GDState();
        this.gs.f=new Font(null,0,12);
        setSize(w,h);
        this.lastSize=getSize();
	addMouseListener(this);
        setBackground(Color.white);
    }

    @Override
	public GDState getGState() { return this.gs; }

    @Override
	public void setDeviceNumber(int dn) { this.devNr=dn; }
    @Override
	public int getDeviceNumber() { return this.devNr; }
    @Override
	public void closeDisplay() {}
    
    public synchronized void cleanup() {
        reset();
        this.l=null;
    }

    LocatorSync lsCallback=null;

    @Override
	public synchronized boolean prepareLocator(LocatorSync ls) {
	if (this.lsCallback!=null && this.lsCallback!=ls) {
		this.lsCallback.triggerAction(null);
	}
	this.lsCallback=ls;
	
	return true;
    }

    // MouseListener for the Locator support
    @Override
	public void mouseClicked(MouseEvent e) {
	if (this.lsCallback!=null) {
	    double[] pos = null;
	    if ((e.getModifiers()&InputEvent.BUTTON1_MASK)>0 && (e.getModifiers()&(InputEvent.BUTTON2_MASK|InputEvent.BUTTON3_MASK))==0) { // B1 = return position
		pos = new double[2];
		pos[0] = e.getX();
		pos[1] = e.getY();
	    }

	    // pure security measure to make sure the trigger doesn't mess with the locator sync object
	    LocatorSync ls=this.lsCallback;
	    this.lsCallback=null; // reset the callback - we'll get a new one if necessary
	    ls.triggerAction(pos);
	}
    }

    @Override
	public void mousePressed(MouseEvent e) {}
    @Override
	public void mouseReleased(MouseEvent e) {}
    @Override
	public void mouseEntered(MouseEvent e) {}
    @Override
	public void mouseExited(MouseEvent e) {}

    public void initRefresh() {
        //System.out.println("resize requested");
        try { // for now we use no cache - just pure reflection API for: Rengine.getMainEngine().eval("...")
			Class<?> c=Class.forName("org.rosuda.JRI.Rengine");
			if (c == null) {
				System.out.println(">> can't find Rengine, automatic resizing disabled. [c=null]");
			}
			else {
				Method m= c.getMethod("getMainEngine", new Class[] {});
				Object o= m.invoke(null, new Object[] {});
				if (o != null) {
					m= c.getMethod("eval", new Class[] { String.class });
					Object[] pars= new Object[1];
					pars[0]= "try(.Call(\"javaGDresize\", " + this.devNr + "L)), PACKAGE= \"rj.gd\", silent= TRUE)";
					m.invoke(o, pars);
				}
			}
        } catch (Exception e) {
            System.out.println(">> can't find Rengine, automatic resizing disabled. [x:"+e.getMessage()+"]");
        }
    }

    @Override
	public void syncDisplay(boolean finish) {
        repaint();
    }
    
    public synchronized Vector getGDOList() { return this.l; }

    @Override
	public synchronized void add(GDObject o) {
        this.l.add(o);
        this.listChanged=true;
    }

    @Override
	public synchronized void reset() {
        this.l.removeAllElements();
        this.listChanged=true;
    }

    @Override
	public Dimension getPreferredSize() {
        return new Dimension(this.prefSize);
    }
    
    @Override
	public synchronized void paintComponent(Graphics g) {
        super.paintComponent(g);
        Dimension d=getSize();
        if (!d.equals(this.lastSize)) {
            initRefresh();
            this.lastSize=d;
            return;
        }

        if (forceAntiAliasing) {
            Graphics2D g2=(Graphics2D) g;
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        }

        int i=0, j=this.l.size();
        g.setFont(this.gs.f);
        g.setClip(0,0,d.width,d.height); // reset clipping rect
        g.setColor(Color.white);
        g.fillRect(0,0,d.width,d.height);
        while (i<j) {
            GDObject o=(GDObject) this.l.elementAt(i++);
            o.paint(this, this.gs, g);
        }
    }
    
}
