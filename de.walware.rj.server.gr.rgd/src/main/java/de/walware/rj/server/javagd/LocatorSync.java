//
//  JavaGD - Java Graphics Device for R
//
//  Copyright (c) 2004, 2025 Simon Urbanek and others.
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation;
//  version 2.1 of the License.
//  
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//  
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// Simon Urbanek <simon.urbanek@r-project.org> - initial API and implementation (org.rosuda.javagd)
// Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation for RJ
//

package de.walware.rj.server.javagd;


/** a simple synchronization class that can be used by a separate thread to wake JavaGD from waiting for a locator result. The waiting thread calls {@link #waitForAction()} which returns only after another thread calls {@link #triggerAction}. */
public class LocatorSync {
    private double[] locResult=null;
    private boolean notificationArrived=false;

    /** this internal method waits until {@link #triggerAction} is called by another thread. It is implemented by using {@link #wait()} and checking {@link #notificationArrived}.
     * @return result supplied when {@link #triggerAction} was called - essentially the retuls to be returned by locator
     */
    public synchronized double[] waitForAction() {
        while (!this.notificationArrived) {
            try {
                wait();
            } catch (InterruptedException e) {
            }
        }
	this.notificationArrived=false;
	return this.locResult;
    }

    /** this methods awakens {@link #waitForAction()}. It is implemented by setting {@link #notificationArrived} to <code>true</code>, setting {@link #locResult} to the passed result and finally calling {@link #notifyAll()}.
     * @param result result to pass to {@link #waitForAction()}
     */
    public synchronized void triggerAction(double[] result) {
	this.locResult=result;
        this.notificationArrived=true;
        notifyAll();
    }
}
