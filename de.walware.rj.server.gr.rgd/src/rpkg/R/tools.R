 #=============================================================================#
 # Copyright (c) 2004, 2025 Simon Urbanek and others.
 # 
 # All rights reserved. This program and the accompanying materials
 # are made available under the terms of the Apache License v2.0
 # which accompanies this distribution, and is available at
 # https://www.apache.org/licenses/LICENSE-2.0
 # 
 # Contributors:
 #     Simon Urbanek <simon.urbanek@r-project.org> - initial API and implementation (org.rosuda.javagd)
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================#


.rj.getGDVersion <- function() {
	initLib()
	
	v <- .Call("javaGDversion", PACKAGE= "rj.gd")
	list(major= v[1]%/%65536, minor= (v[1]%/%256)%%256, patch= (v[1]%%256), numeric= v[1])
}

.rj.setGDDisplayParameters <- function(dpiX= 100L, dpiY= 100L, aspect= 1L) {
	invisible(.Call("javaGDsetDisplayParam", as.double(c(dpiX, dpiY, aspect)), PACKAGE= "rj.gd"))
}

