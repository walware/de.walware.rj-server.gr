 #=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # All rights reserved. This program and the accompanying materials
 # are made available under the terms of the Apache License v2.0
 # which accompanies this distribution, and is available at
 # https://www.apache.org/licenses/LICENSE-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================#


## RJ graphic device

rj.GD <- function(name = "rj.gd", width = 7, height = 7, size.unit = "in",
		xpinch = NA, ypinch = NA, canvas = "white",
		pointsize = 12, gamma = 1.0) {
	initLib()
	
	if (!size.unit %in% c("in", "px")) {
		error(paste("Illegal argument: unsupported unit", size.unit))
	}
	size.unit <- switch(size.unit, "px" = 1L, 0L)
	
	invisible(.Call("newJavaGD", name,
					width, height, size.unit,
					xpinch, ypinch, canvas,
					pointsize, gamma,
					PACKAGE= "rj.gd" ))
}

.rj.getGDJavaObject <- function(devNr) {
	a <- .Call("javaGDobjectCall", devNr - 1L, PACKAGE= "rj.gd")
	if (!is.null(a)) {
		if (exists(".jmkref")) a <- .jmkref(a)
		else stop(".jmkref is not available. Please use rJava 0.3 or higher.")
	}
}

