AC_INIT(rj.gd, 4.0, stephan.wahlbrink@walware.de)

AC_CONFIG_SRCDIR([src/javaGD.c])
AC_CONFIG_HEADERS([src/config.h])


AC_MSG_CHECKING(R config)
: ${R_HOME=`R RHOME`}
if test -z "${R_HOME}"; then
    AC_MSG_ERROR(cannot determine R_HOME)
fi
RBIN="${R_HOME}/bin/R"
R_CC=`"${RBIN}" CMD config CC`
R_CFLAGS=`"${RBIN}" CMD config CFLAGS`

AC_MSG_RESULT([R config set
    R_HOME   : ${R_HOME}])

AC_SUBST(R_HOME)
export R_HOME


# if user did not specify CC then we use R's settings.
# if CC was set then user is responsible for CFLAGS as well!
if test -z "${CC}"; then
	CC="${R_CC}"
	CPP="${R_CC} -E"
	CFLAGS="${R_CFLAGS}"
fi

AC_PROG_CC
AC_LANG(C)


# Checks for libraries.

# Checks for header files.
AC_HEADER_SYS_WAIT
AC_CHECK_HEADERS([string.h sys/time.h unistd.h])

# Checks for typedefs, structures, and compiler characteristics.
AC_C_CONST

# Checks for library functions.
AC_FUNC_MALLOC
AC_CHECK_FUNCS([memset mkdir rmdir select socket])


AC_MSG_CHECKING(Java support in R)
: ${JAVA_HOME=`"${RBIN}" CMD config JAVA_HOME|sed 's/ERROR:.*//'`}

if test -z "${JAVA_HOME}"; then
    AC_MSG_ERROR([cannot determine JAVA_HOME
R is not configured with Java support. Please run
    R CMD javareconf
as root to add Java support to R.

If you don't have root privileges, run
    R CMD javareconf -e
to set all Java-related variables and then install the package.])
else
    AC_MSG_RESULT([...])
fi

: ${JAVA=`"${RBIN}" CMD config JAVA|sed 's/ERROR:.*//'`}
: ${JAVA_CPPFLAGS=`"${RBIN}" CMD config JAVA_CPPFLAGS|sed 's/ERROR:.*//'`}
: ${JAVA_LIBS=`"${RBIN}" CMD config JAVA_LIBS|sed 's/ERROR:.*//'`}

AC_MSG_CHECKING([Java version])
JAVA_VERSION=`"${JAVA}" -version 2>&1 | sed -n 's:^.* version "::p' | sed 's:".*::'`
if test -z "${JAVA_VERSION}"; then
    AC_MSG_WARN([**** Cannot detect Java version - the java -version output is unknown! ****])
else
    AC_MSG_RESULT([${JAVA_VERSION}])
fi

have_all_java=yes
if test -z "${JAVA_HOME}" \
        || test -z "${JAVA}" \
        || test -z "${JAVA_CPPFLAGS}" \
        || test -z "${JAVA_LIBS}"; then
    have_all_java=no
fi

if test ${have_all_java} = no; then
    AC_MSG_ERROR([cannot determine complete java config
    JAVA_HOME: ${JAVA_HOME}
    JAVA     : ${JAVA}
    JAVA_CPPFLAGS: ${JAVA_CPPFLAGS}
    JAVA_LIBS: ${JAVA_LIBS}
R is not configured with full Java support. Please make sure 
an JDK is installed and run
    R CMD javareconf
as root to add Java support to R.

If you don't have root privileges, run
    R CMD javareconf -e
to set all Java-related variables and then install the package.])
fi

JAVA_INC="${JAVA_CPPFLAGS}"

if test `echo foo | sed -e 's:foo:bar:'` = bar; then
    JAVA_CPPFLAGS0=`echo ${JAVA_CPPFLAGS} | sed -e 's:$(JAVA_HOME):${JAVA_HOME}:g'`
    JAVA_LIBS0=`echo ${JAVA_LIBS} | sed -e 's:$(JAVA_HOME):${JAVA_HOME}:g'`
    JAVA_LD_LIBRARY_PATH0=`echo ${JAVA_LD_LIBRARY_PATH} | sed -e 's:$(JAVA_HOME):${JAVA_HOME}:g'`
else
    AC_MSG_WARN([sed is not working properly - the configuration may fail])
    JAVA_CPPFLAGS0="${JAVA_CPPFLAGS}"
    JAVA_LIBS0="${JAVA_LIBS}"
    JAVA_LD_LIBRARY_PATH0="${JAVA_LD_LIBRARY_PATH}"
fi

LIBS="${LIBS} ${JAVA_LIBS0}"
CFLAGS="${CFLAGS} ${XTRA_CF} ${JAVA_CPPFLAGS0}"
LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${JAVA_LD_LIBRARY_PATH0}"

AC_MSG_CHECKING([whether JNI programs can be compiled])
AC_LINK_IFELSE([AC_LANG_SOURCE(
#include <jni.h>
int main(void) {
    jobject o;
    return 0;
}
		)],
	[AC_MSG_RESULT(yes)],
	[AC_MSG_ERROR([Cannot compile a simple JNI program. See config.log for details.])])

AC_MSG_RESULT([Java config set
    JAVA_HOME: ${JAVA_HOME}
    JAVA     : ${JAVA}
    JAVAC    : ${JAVAC}
    JAVA_LIBS: ${JAVA_LIBS}
])

AC_SUBST(JAVA_HOME)
AC_SUBST(JAVA)
AC_SUBST(JAVAC)
AC_SUBST(JAVA_LIBS)
AC_SUBST(JAVA_INC)
AC_SUBST(JAVA_LD)
AC_SUBST(XTRA_CF)
AC_SUBST(XTRA_LD)


AC_CONFIG_FILES([src/Makevars])
AC_OUTPUT
