/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # All rights reserved. This program and the accompanying materials
 # are made available under the terms of the Apache License v2.0
 # which accompanies this distribution, and is available at
 # https://www.apache.org/licenses/LICENSE-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

#include "rjutil.h"

#include <R_ext/Error.h>

extern Rboolean utf8locale;


#define RJ_MESSAGE_PREFIX "[RJ-RSrv.RGD/JNI] "


static int rjinit= 0;

static JavaVM *jVM;

static jclass jcClass= NULL;
static jclass jcByteArray= NULL;

static jclass jcRJClassLoader= NULL;
static jmethodID jmClassForName= NULL;
static jobject joRJClassLoader= NULL;
static jmethodID jmRJClassLoaderAddClassPath= NULL;

static jbyteArray joByteArrayEmpty= NULL;
static jbyteArray joByteArrayNA= NULL;


static int rj_init(JNIEnv *env) {
	jclass jc;
	jmethodID jm;
	jobject jo;
	
	(*env)->GetJavaVM(env, &jVM);
	
	const char *errorMessage= "Failed to init JNI utils (%s).";
	
	jc= (*env)->FindClass(env, "java/lang/Class");
	if (!jc) {
		handleJError(env, RJ_ERROR_CWARNING, errorMessage, "jni.FindClass(Class)");
		return -1;
	}
	jcClass= (*env)->NewGlobalRef(env, jc);
	if (!jcClass) {
		handleJError(env, RJ_ERROR_CWARNING, errorMessage, "jni.NewGlobalRef(jcClass)");
		return -1;
	}
	(*env)->DeleteLocalRef(env, jc);
	
	jc= (*env)->FindClass(env, "[B");
	if (!jc) {
		handleJError(env, RJ_ERROR_CWARNING, errorMessage, "jni.FindClass(byte[])");
		return -1;
	}
	jcByteArray= (*env)->NewGlobalRef(env, jc);
	if (!jcByteArray) {
		handleJError(env, RJ_ERROR_CWARNING, errorMessage, "jni.NewGlobalRef(jcByteArray)");
		return -1;
	}
	(*env)->DeleteLocalRef(env, jc);
	
	jm= (*env)->GetStaticMethodID(env, jcClass, "forName", "(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;");
	if (!jm) {
		handleJError(env, RJ_ERROR_CWARNING, errorMessage, "getting method 'java.lang.Class#forName(String, boolean, ClassLoader)'");
		return -1;
	}
	jmClassForName= jm;
	
	jc= (*env)->FindClass(env, "RJavaClassLoader");
	if (!jc) {
		handleJError(env, RJ_ERROR_CWARNING, errorMessage, "finding class 'RJavaClassLoader'");
		return -11;
	}
	jcRJClassLoader= (*env)->NewGlobalRef(env, jc);
	if (!jcRJClassLoader) {
		handleJError(env, RJ_ERROR_CWARNING, errorMessage, "creating ref for 'jcRJClassLoader'");
		return -11;
	}
	(*env)->DeleteLocalRef(env, jc);
	
	jm= (*env)->GetStaticMethodID(env, jcRJClassLoader, "getPrimaryLoader", "()LRJavaClassLoader;");
	if (!jm) {
		handleJError(env, RJ_ERROR_CWARNING, errorMessage, "getting static method 'RJavaClassLoader#getPrimaryLoader()'");
		return -12;
	}
	jo= (*env)->CallStaticObjectMethod(env, jcRJClassLoader, jm);
	if (!jo) {
		handleJError(env, RJ_ERROR_CWARNING, errorMessage, "calling 'RJavaClassLoader#getPrimaryLoader()'");
		return -12;
	}
	joRJClassLoader= (*env)->NewGlobalRef(env, jo);
	if (!joRJClassLoader) {
		handleJError(env, RJ_ERROR_CWARNING, errorMessage, "creating ref for 'joRJavaClassLoader'");
		return -12;
	}
	(*env)->DeleteLocalRef(env, jo);
	
	jm= (*env)->GetMethodID(env, jcRJClassLoader, "addClassPathUtf8", "([B)V");
	if (!jm) {
		handleJError(env, RJ_ERROR_CWARNING, errorMessage, "getting method 'RJavaClassLoader#addClassPathUtf8(byte[])'");
		return -21;
	}
	jmRJClassLoaderAddClassPath= jm;
	
	jo= (*env)->NewByteArray(env, 0);
	if (!jo) {
		handleJError(env, RJ_ERROR_CWARNING, errorMessage, "jni.NewByteArray(0)");
		return -31;
	}
	joByteArrayEmpty= (*env)->NewGlobalRef(env, jo);
	if (!joByteArrayEmpty) {
		handleJError(env, RJ_ERROR_CWARNING, errorMessage, "jni.NewGlobalRef(joByteArrayEmpty)");
		return -31;
	}
	(*env)->DeleteLocalRef(env, jo);
	
	return 1;
}

void checkRJUtil(JNIEnv *env) {
	if (rjinit != 1) {
		rjinit= rj_init(env);
#ifdef JRI_DEBUG
		printf("initRJUtil: %d\n", rjinit);
		fflush(stdout);
#endif
	}
	if (rjinit != 1) {
		handleJError(env, RJ_ERROR_RERROR, "Failed to initialize Java bindings (%d).", rjinit);
	}
}

JavaVM *getJVM(void) {
	return jVM;
}

JNIEnv *getJEnv(void) {
	JNIEnv *env;
	jint rc;
	
	if (!jVM) {
		jsize n;
		if ((rc= JNI_GetCreatedJavaVMs(&jVM, 1, &n)) != JNI_OK) {
			handleJError(0, RJ_ERROR_CWARNING, "Failed to get Java VM (return.code= %d).", (int) rc);
			return NULL;
		}
		if (n < 1) {
			return NULL;
		}
	}
	
	if ((rc= (*jVM)->AttachCurrentThread(jVM, (void**) &env, 0)) != JNI_OK) {
		handleJError(0, RJ_ERROR_CWARNING, "Failed to attach thread to Java VM (return.code= %d).", (int) rc);
		return NULL;
	}
	return env;
}

void addJClassPath(JNIEnv *env, jbyteArray jPath) {
	(*env)->CallObjectMethod(env, joRJClassLoader, jmRJClassLoaderAddClassPath,
			jPath );
	(*env)->DeleteLocalRef(env, jPath);
}


jbyteArray newJUtf8StringC(JNIEnv *env, const char *nc) {
	const void *vmax= vmaxget();
	const char *c= (utf8locale) ? nc : Rf_reEnc(nc, CE_NATIVE, CE_UTF8, 1);
	const size_t length= strlen(c);
	if (!length) {
		return joByteArrayEmpty;
	}
	jbyteArray bytes= (*env)->NewByteArray(env, length);
	if (bytes) {
		(*env)->SetByteArrayRegion(env, bytes, 0, length, (jbyte *)c);
	}
	vmaxset(vmax);
	return bytes;
}

jbyteArray newJUtf8StringS(JNIEnv *env, SEXP cS) {
	if (!cS) {
		return NULL;
	}
	if (cS == R_NaString) {
		return joByteArrayNA;
	}
	if (cS == R_BlankString) {
		return joByteArrayEmpty;
	}
	const void *vmax= vmaxget();
	const char *rc= CHAR(cS);
	cetype_t cType= Rf_getCharCE(cS);
	const char *c= (cType == CE_UTF8 || (utf8locale && cType == CE_NATIVE)) ?
			rc : Rf_reEnc(rc, cType, CE_UTF8, 1);
	const size_t length= (c == rc) ? LENGTH(cS) : strlen(c);
	jbyteArray bytes= (*env)->NewByteArray(env, length);
	if (!bytes) {
		vmaxset(vmax);
		return NULL;
	}
	(*env)->SetByteArrayRegion(env, bytes, 0, length, (jbyte *)c);
	vmaxset(vmax);
	return bytes;
}


jclass getJClass(JNIEnv *env, const char *name, int flags) {
	jclass jc;
	
	{	jstring js= (*env)->NewStringUTF(env, name);
		if (!js) {
			handleJError(env, flags, "getJClass.name: " JAVA_CREATE_STRING_ERROR_MESSAGE);
			return 0;
		}
		jc= (*env)->CallStaticObjectMethod(env, jcClass, jmClassForName, js, JNI_TRUE,
				joRJClassLoader );
		(*env)->DeleteLocalRef(env, js);
	}
	if (!jc) {
		handleJError(env, flags, "getJClass: Failed to get Java class '%s'.", name);
		return 0;
	}
	
	if ((flags & RJ_GLOBAL_REF)) {
		jclass global= (*env)->NewGlobalRef(env, jc);
		(*env)->DeleteLocalRef(env, jc);
		if (!global) {
			handleJError(env, flags, "getJClass: Failed to create ref for Java class '%s'.", name);
			return 0;
		}
		return global;
	}
	else {
		return jc;
	}
}

jmethodID getJMethod(JNIEnv *env, jclass jClass, const char *name, const char *sig, int flags) {
	jmethodID jm;
	
	jm= (*env)->GetMethodID(env, jClass, name, sig);
	if (!jm) {
		handleJError(env, flags, "getJMethod: Failed to get Java method '%s'.", name);
		return NULL;
	}
	
#ifdef JGD_DEBUG
	printf("getJMethod: '%s' -> class= %p, mid= %p\n", sig, jClass, jm);
	fflush(stdout);
#endif
	
	return jm;
}


static void handleError(unsigned int flags, const char *msg) {
	switch (flags & RJ_ERROR_MASK) {
	case RJ_ERROR_RWARNING:
		Rf_warning(RJ_MESSAGE_PREFIX "%s", msg);
		return;
	case RJ_ERROR_RERROR:
		Rf_error(RJ_MESSAGE_PREFIX "%s", msg);
		return;
	default:
		fprintf(stderr, RJ_MESSAGE_PREFIX "%s\n", msg);
		fflush(stderr);
		return;
	}
}

void handleJError(JNIEnv *env, unsigned int flags, const char *message, ...) {
	if (env && (*env)->ExceptionCheck(env) == JNI_TRUE) {
#ifdef JGD_DEBUG
		(*env)->ExceptionDescribe(env);
#endif
		(*env)->ExceptionClear(env);
	}
	
	if ((flags & RJ_ERROR_MASK)) {
		va_list ap;
		char msg[1024];
		msg[1023]= 0;
		
		va_start(ap, message);
		vsnprintf(msg, sizeof(msg), message, ap);
		va_end(ap);
		
		handleError(flags, msg);
	}
}

void checkJError(JNIEnv *env, unsigned int flags, const char *message, ...) {
	if (env && (*env)->ExceptionCheck(env) == JNI_TRUE) {
#ifdef JGD_DEBUG
		(*env)->ExceptionDescribe(env);
#endif
		(*env)->ExceptionClear(env);
	}
	else {
		return;
	}
	
	if ((flags & RJ_ERROR_MASK)) {
		va_list ap;
		char msg[1024];
		msg[1023]= 0;
		
		va_start(ap, message);
		vsnprintf(msg, sizeof(msg), message, ap);
		va_end(ap);
		
		handleError(flags, msg);
	}
}

void clearJError(JNIEnv *env) {
	if (env && (*env)->ExceptionCheck(env) == JNI_TRUE) {
#ifdef JGD_DEBUG
		(*env)->ExceptionDescribe(env);
#endif
		(*env)->ExceptionClear(env);
	}
}

void handleCError(JNIEnv *env, unsigned int flags, const char *message, ...) {
	if ((flags & RJ_ERROR_MASK)) {
		va_list ap;
		char msg[1024];
		msg[1023]= 0;
		
		va_start(ap, message);
		vsnprintf(msg, sizeof(msg), message, ap);
		va_end(ap);
		
		handleError(flags, msg);
	}
}

