/*=============================================================================#
 # JavaGD - Java Graphics Device
 # 
 # Copyright (c) 2004, 2025 Simon Urbanek and others.
 #
 # Licensed under the Apache License, Version 2.0 (the "License");
 # you may not use this file except in compliance with the License.
 # You may obtain a copy of the License at
 # http://www.apache.org/licenses/LICENSE-2.0
 # Unless required by applicable law or agreed to in writing, software
 # distributed under the License is distributed on an "AS IS" BASIS,
 # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 # See the License for the specific language governing permissions and
 # limitations under the License.
 #=============================================================================*/

#ifndef __JGD_TALK_H__
#define __JGD_TALK_H__

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include "javaGD.h"

Rboolean createJavaGD(newJavaGDDesc *xd);
void initJavaGD(newJavaGDDesc *xd, double *width, double *height, int *unit, double *xpi, double *ypi);
void setupJavaGDfunctions(NewDevDesc *dd);
void openJavaGD(NewDevDesc *dd);

void getJavaGDPPI(NewDevDesc *dd, double *xpi, double *ypi);

#endif
