/*=============================================================================#
 # JavaGD - Java Graphics Device
 # 
 # Copyright (c) 2004, 2025 Simon Urbanek and others.
 #
 # Licensed under the Apache License, Version 2.0 (the "License");
 # you may not use this file except in compliance with the License.
 # You may obtain a copy of the License at
 # http://www.apache.org/licenses/LICENSE-2.0
 # Unless required by applicable law or agreed to in writing, software
 # distributed under the License is distributed on an "AS IS" BASIS,
 # WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 # See the License for the specific language governing permissions and
 # limitations under the License.
 #=============================================================================*/

#include "javaGD.h"
#include "jGDtalk.h"
#include "rjutil.h"
#include <Rdefines.h>


/* Device Driver Actions */

#define jgdCheckExceptions chkX

#ifdef JGD_DEBUG
#define gdWarning(S) { printf("[JavaGD warning] %s\n", S); jgdCheckExceptions(getJEnv()); }
#else
#define gdWarning(S)
#endif

#if R_VERSION < R_Version(2,11,0)
#error This JavaGD needs at least R version 2.11.0
#endif


static void newJavaGD_Size(double *left, double *right, double *bottom, double *top,
		NewDevDesc *dd);
static void newJavaGD_Mode(int mode,
		NewDevDesc *dd);
static void newJavaGD_NewPage(R_GE_gcontext *gc,
		NewDevDesc *dd);
static Rboolean newJavaGD_NewPageConfirm(
		NewDevDesc *dd);
static void newJavaGD_Close(
		NewDevDesc *dd);

static void newJavaGD_Activate(
		NewDevDesc *dd);
static void newJavaGD_Deactivate(
		NewDevDesc *dd);

static void newJavaGD_Clip(double x0, double x1, double y0, double y1,
		NewDevDesc *dd);
static SEXP newJavaGD_setClipPath(SEXP path, SEXP ref,
		NewDevDesc *dd);
static void newJavaGD_releaseClipPath(SEXP ref,
		NewDevDesc *dd);
static SEXP newJavaGD_setMask(SEXP path, SEXP ref,
		NewDevDesc *dd);
static void newJavaGD_releaseMask(SEXP ref,
		NewDevDesc *dd);

static SEXP newJavaGD_setPattern(SEXP pattern,
		NewDevDesc *dd);
static void newJavaGD_releasePattern(SEXP ref,
		NewDevDesc *dd);

static void newJavaGD_Line(double x1, double y1, double x2, double y2,
		R_GE_gcontext *gc,
		NewDevDesc *dd);
static void newJavaGD_Rect(double x0, double y0, double x1, double y1,
		R_GE_gcontext *gc,
		NewDevDesc *dd);
static void newJavaGD_Circle(double x, double y, double r,
		R_GE_gcontext *gc,
		NewDevDesc *dd);
static void newJavaGD_Polyline(int n, double *x, double *y,
		R_GE_gcontext *gc,
		NewDevDesc *dd);
static void newJavaGD_Polygon(int n, double *x, double *y,
		R_GE_gcontext *gc,
		NewDevDesc *dd);
static void newJavaGD_Path(double *x, double *y, int npoly, int *nper, Rboolean winding,
		R_GE_gcontext *gc,
		NewDevDesc *dd);

static void newJavaGD_MetricInfo(int c,
		R_GE_gcontext *gc,
		double* ascent, double* descent, double* width,
		NewDevDesc *dd);
static double newJavaGD_StrWidth(const char *str, 
		R_GE_gcontext *gc,
		NewDevDesc *dd);
static double newJavaGD_StrWidthUTF8(const char *str, 
		R_GE_gcontext *gc,
		NewDevDesc *dd);
static void newJavaGD_Text(double x, double y, const char *str,
		double rot, double hadj,
		R_GE_gcontext *gc,
		NewDevDesc *dd);
static void newJavaGD_TextUTF8(double x, double y, const char *str,
		double rot, double hadj,
		R_GE_gcontext *gc,
		NewDevDesc *dd);

static void newJavaGD_Raster(unsigned int *raster, int w, int h,
		double x, double y, double width, double height,
		double rot, Rboolean interpolate,
		R_GE_gcontext *gc,
		NewDevDesc *dd);
static SEXP newJavaGD_Cap(
		NewDevDesc *dd);

static Rboolean newJavaGD_Locator(double *x, double *y,
		NewDevDesc *dd);


static R_GE_gcontext lastGC; /** last graphics context. the API send changes, not the entire context, so we cache it for comparison here */

char *jarClassPath = ".";

static jclass jcGDInterface;
static jmethodID jmGDInterfaceActivate;
static jmethodID jmGDInterfaceCircle;
static jmethodID jmGDInterfaceClip;
static jmethodID jmGDInterfaceClose;
static jmethodID jmGDInterfaceDeactivate;
static jmethodID jmGDInterfaceFlush;
static jmethodID jmGDInterfaceGetPPI;
static jmethodID jmGDInterfaceInit;
static jmethodID jmGDInterfaceLocator;
static jmethodID jmGDInterfaceLine;
static jmethodID jmGDInterfaceMetricInfo;
static jmethodID jmGDInterfaceMode;
static jmethodID jmGDInterfaceNewPage;
static jmethodID jmGDInterfaceNewPageConfirm;
static jmethodID jmGDInterfaceOpen;
static jmethodID jmGDInterfacePath;
static jmethodID jmGDInterfacePolygon;
static jmethodID jmGDInterfacePolyline;
static jmethodID jmGDInterfaceRect;
static jmethodID jmGDInterfaceSize;
static jmethodID jmGDInterfaceStrWidth;
static jmethodID jmGDInterfaceText;
static jmethodID jmGDInterfaceRaster;
static jmethodID jmGDInterfaceCap;
static jmethodID jmGDInterfaceSetColor;
static jmethodID jmGDInterfaceSetFill;
static jmethodID jmGDInterfaceSetLine;
static jmethodID jmGDInterfaceSetFont;


/** check exception for the given environment. The exception is printed only in JGD_DEBUG mode. */
static void chkX(JNIEnv *env)
{
    jthrowable t=(*env)->ExceptionOccurred(env);
    if (t) {
#ifdef JGD_DEBUG
		(*env)->ExceptionDescribe(env);
#endif
        (*env)->ExceptionClear(env);
    }
}

#define checkGC(e,xd,gc) sendGC(e,xd,gc,0)

/** check changes in GC and issue corresponding commands if necessary */
static void sendGC(JNIEnv *env, newJavaGDDesc *xd, R_GE_gcontext *gc, int sendAll) {
    if (sendAll || gc->col != lastGC.col) {
		(*env)->CallVoidMethod(env, xd->talk, jmGDInterfaceSetColor, gc->col);
		chkX(env);
    }

    if (sendAll || gc->fill != lastGC.fill)  {
		(*env)->CallVoidMethod(env, xd->talk, jmGDInterfaceSetFill, gc->fill);
		chkX(env);
    }

	if (sendAll || gc->lwd != lastGC.lwd || gc->lty != lastGC.lty
			|| gc->lend != lastGC.lend || gc->ljoin != lastGC.ljoin || gc->lmitre != lastGC.lmitre) {
		(*env)->CallVoidMethod(env, xd->talk, jmGDInterfaceSetLine, gc->lwd, gc->lty,
				(jbyte) gc->lend, (jbyte) gc->ljoin, (jfloat) gc->lmitre );
		chkX(env);
	}

    if (sendAll || gc->cex!=lastGC.cex || gc->ps!=lastGC.ps || gc->lineheight!=lastGC.lineheight || gc->fontface!=lastGC.fontface || strcmp(gc->fontfamily, lastGC.fontfamily)) {
		jbyteArray jFontFamily= newJUtf8StringC(env, gc->fontfamily);
		(*env)->CallVoidMethod(env, xd->talk, jmGDInterfaceSetFont, gc->cex, gc->ps, gc->lineheight, gc->fontface, jFontFamily);
		chkX(env);
    }
    memcpy(&lastGC, gc, sizeof(lastGC));
}

/* re-set the GC - i.e. send commands for all monitored GC entries */
static void sendAllGC(JNIEnv *env, newJavaGDDesc *xd, R_GE_gcontext *gc) {
    /*
    printf("Basic GC:\n col=%08x\n fill=%08x\n gamma=%f\n lwd=%f\n lty=%08x\n cex=%f\n ps=%f\n lineheight=%f\n fontface=%d\n fantfamily=\"%s\"\n\n",
	 gc->col, gc->fill, gc->gamma, gc->lwd, gc->lty,
	 gc->cex, gc->ps, gc->lineheight, gc->fontface, gc->fontfamily);
     */
    sendGC(env, xd, gc, 1);
}

/*------- the R callbacks begin here ... ------------------------*/

static void newJavaGD_Size(double *left, double *right,  double *bottom, double *top,
		NewDevDesc *dd) {
	newJavaGDDesc *xd = (newJavaGDDesc *) dd->deviceSpecific;
	JNIEnv *env= getJEnv();
	
	if (!env || !xd || !xd->talk) return;
	
	jdoubleArray o= (*env)->CallObjectMethod(env, xd->talk, jmGDInterfaceSize);
	if (o) {
		jdouble *ac=(jdouble*)(*env)->GetDoubleArrayElements(env, o, 0);
		if (!ac) {
			(*env)->DeleteLocalRef(env, o);
			handleJError(env, RJ_ERROR_RERROR, "gdSize.result: jni.GetDoubleArrayElements(...) failed.");
			return;
		}
		*left=ac[0]; *right=ac[1]; *bottom=ac[2]; *top=ac[3];
		(*env)->ReleaseDoubleArrayElements(env, o, ac, JNI_ABORT);
		(*env)->DeleteLocalRef(env, o);
	} else gdWarning("gdSize: gdSize returned null");
	chkX(env);
}

static void newJavaGD_Mode(int mode, NewDevDesc *dd) {
	newJavaGDDesc *xd = (newJavaGDDesc *) dd->deviceSpecific;
	JNIEnv *env= getJEnv();
	
	if (!env || !xd || !xd->talk) return;
	
	(*env)->CallVoidMethod(env, xd->talk, jmGDInterfaceMode, mode);
	chkX(env);
}

static void newJavaGD_NewPage(R_GE_gcontext *gc, NewDevDesc *dd) {
	newJavaGDDesc *xd = (newJavaGDDesc *) dd->deviceSpecific;
	JNIEnv *env= getJEnv();
	
	if (!env || !xd || !xd->talk) return;
	
	(*env)->CallVoidMethod(env, xd->talk, jmGDInterfaceNewPage, gc->fill);
	chkX(env);
	
	if (R_ALPHA(gc->fill) != 0) { // bg
		int savedCol = gc->col;
		gc->col = 0x00ffffff;
		sendAllGC(env, xd, gc);
		
		(*env)->CallVoidMethod(env, xd->talk, jmGDInterfaceRect, 
				dd->left, dd->top, dd->right, dd->bottom);
		chkX(env);
		
		gc->col = savedCol;
		checkGC(env, xd, gc);
	} else {
		sendAllGC(env, xd, gc);
	}
}

static Rboolean newJavaGD_NewPageConfirm(NewDevDesc *dd) {
	newJavaGDDesc *xd = (newJavaGDDesc *) dd->deviceSpecific;
	JNIEnv *env= getJEnv();
	
	if (!env || !xd || !xd->talk) return FALSE;
	
	jboolean handled = (*env)->CallBooleanMethod(env, xd->talk, jmGDInterfaceNewPageConfirm);
	
	return (handled == JNI_TRUE) ? TRUE : FALSE;
}

static int newJavaGD_HoldFlush(NewDevDesc *dd, int level) {
	newJavaGDDesc *xd = (newJavaGDDesc *) dd->deviceSpecific;
	JNIEnv *env= getJEnv();
	
	if (!xd) return 0;
	
	int ol = xd->holdlevel;
	xd->holdlevel += level;
	if (xd->holdlevel < 0) {
		xd->holdlevel = 0;
	}
	
	if (!env || !xd->talk) return xd->holdlevel;
	
	if (xd->holdlevel == 0) /* flush */
		(*env)->CallVoidMethod(env, xd->talk, jmGDInterfaceFlush, 1);
	else if (ol == 0)/* first hold */
		(*env)->CallVoidMethod(env, xd->talk, jmGDInterfaceFlush, 0);
	chkX(env);
	
	return xd->holdlevel;
}

static void newJavaGD_Close(NewDevDesc *dd) {
	newJavaGDDesc *xd = (newJavaGDDesc *) dd->deviceSpecific;
	JNIEnv *env= getJEnv();
	
	if (!env || !xd || !xd->talk) return;
	
	(*env)->CallVoidMethod(env, xd->talk, jmGDInterfaceClose);
	chkX(env);
}

static void newJavaGD_Activate(NewDevDesc *dd) {
	newJavaGDDesc *xd = (newJavaGDDesc *) dd->deviceSpecific;
	JNIEnv *env= getJEnv();
	
	if (!env || !xd || !xd->talk) return;
	
	(*env)->CallVoidMethod(env, xd->talk, jmGDInterfaceActivate);
	chkX(env);
}

static void newJavaGD_Deactivate(NewDevDesc *dd) {
	newJavaGDDesc *xd = (newJavaGDDesc *) dd->deviceSpecific;
	JNIEnv *env= getJEnv();
	
	if (!env || !xd || !xd->talk) return;
	
	(*env)->CallVoidMethod(env, xd->talk, jmGDInterfaceDeactivate);
	chkX(env);
}

static void newJavaGD_Clip(double x0, double x1, double y0, double y1,
		NewDevDesc *dd) {
	newJavaGDDesc *xd = (newJavaGDDesc *) dd->deviceSpecific;
	JNIEnv *env= getJEnv();
	
	if (!env || !xd || !xd->talk) return;
	
	if (x0 > x1) {
		double tmp = x0;
		x0 = x1;
		x1 = tmp;
	}
	if (y0 > y1) {
		double tmp = y0;
		y0 = y1;
		y1 = tmp;
	}
	(*env)->CallVoidMethod(env, xd->talk, jmGDInterfaceClip, x0, x1, y0, y1);
	chkX(env);
}

static SEXP newJavaGD_setClipPath(SEXP path, SEXP ref,
		NewDevDesc *dd) {
	return R_NilValue;
}

static void newJavaGD_releaseClipPath(SEXP ref,
		NewDevDesc *dd) {
}

static SEXP newJavaGD_setMask(SEXP path, SEXP ref,
		NewDevDesc *dd) {
	return R_NilValue;
}

static void newJavaGD_releaseMask(SEXP ref,
		NewDevDesc *dd) {
}

static SEXP newJavaGD_setPattern(SEXP pattern,
		NewDevDesc *dd) {
	return R_NilValue;
}

static void newJavaGD_releasePattern(SEXP ref,
		NewDevDesc *dd) {
}

static void newJavaGD_Line(double x1, double y1, double x2, double y2,
		R_GE_gcontext *gc, NewDevDesc *dd) {
	newJavaGDDesc *xd = (newJavaGDDesc *) dd->deviceSpecific;
	JNIEnv *env= getJEnv();
	
	if (!env || !xd || !xd->talk) return;
	
	checkGC(env, xd, gc);
	
	(*env)->CallVoidMethod(env, xd->talk, jmGDInterfaceLine, x1, y1, x2, y2);
	chkX(env);
}

static void newJavaGD_Rect(double x0, double y0, double x1, double y1,
		R_GE_gcontext *gc, NewDevDesc *dd) {
	newJavaGDDesc *xd = (newJavaGDDesc *) dd->deviceSpecific;
	JNIEnv *env= getJEnv();
	
	if (!env || !xd || !xd->talk) return;
	
	if (x0 > x1) {
		double tmp = x0;
		x0 = x1;
		x1 = tmp;
	}
	if (y0 > y1) {
		double tmp = y0;
		y0 = y1;
		y1 = tmp;
	}
	
	checkGC(env, xd, gc);
	
	(*env)->CallVoidMethod(env, xd->talk, jmGDInterfaceRect, x0, y0, x1, y1);
	chkX(env);
}

static void newJavaGD_Circle(double x, double y, double r,
		R_GE_gcontext *gc,  NewDevDesc *dd) {
	newJavaGDDesc *xd = (newJavaGDDesc *) dd->deviceSpecific;
	JNIEnv *env= getJEnv();
	
	if (!env || !xd || !xd->talk) return;
	
	checkGC(env, xd, gc);
	
	(*env)->CallVoidMethod(env, xd->talk, jmGDInterfaceCircle, x, y, r);
	chkX(env);
}

static void newJavaGD_MetricInfo(int c, R_GE_gcontext *gc,
		double* ascent, double* descent, double* width,
		NewDevDesc *dd) {
	newJavaGDDesc *xd = (newJavaGDDesc *) dd->deviceSpecific;
	JNIEnv *env= getJEnv();
	
	if (!env || !xd || !xd->talk) return;
	
	checkGC(env, xd, gc);
	
	if (c < 0) c = -c;
	jdoubleArray o= (*env)->CallObjectMethod(env, xd->talk, jmGDInterfaceMetricInfo, c);
	if (o) {
		jdouble *ac=(jdouble*)(*env)->GetDoubleArrayElements(env, o, 0);
		if (!ac) {
			(*env)->DeleteLocalRef(env, o);
			handleJError(env, RJ_ERROR_RERROR, "gdMetricInfo.info: jni.GetDoubleArrayElements(...) failed.");
			return;
		}
		*ascent=ac[0]; *descent=ac[1]; *width=ac[2];
		(*env)->ReleaseDoubleArrayElements(env, o, ac, JNI_ABORT);
		(*env)->DeleteLocalRef(env, o);
	}
	chkX(env);
}

static jdoubleArray newDoubleArrayPoly(JNIEnv *env, int n, double *ct) {
	jdoubleArray da = (*env)->NewDoubleArray(env, n);
	if (!da) {
		handleJError(env, RJ_ERROR_RERROR, "gdPoly*/gdPath.data: jni.NewDoubleArray(%d) failed.", n);
		return NULL;
	}
	if (n>0) {
		jdouble *dae;
		dae = (*env)->GetDoubleArrayElements(env, da, 0);
		if (!dae) {
			(*env)->DeleteLocalRef(env, da);
			handleJError(env, RJ_ERROR_RERROR, "gdPoly*/gdPath.data: jni.GetDoubleArrayElements(...) failed.");
			return NULL;
		}
		memcpy(dae,ct,sizeof(double)*n);
		(*env)->ReleaseDoubleArrayElements(env, da, dae, 0);
	}
	chkX(env);
	return da;
}

static void newJavaGD_Path(double *x, double *y, int npoly, int *nper, Rboolean winding,
		R_GE_gcontext *gc, NewDevDesc *dd) {
	newJavaGDDesc *xd = (newJavaGDDesc *) dd->deviceSpecific;
	JNIEnv *env= getJEnv();
	jintArray jNPer;
	jdoubleArray jX, jY;
	
	if (!env || !xd || !xd->talk) return;
	
	checkGC(env, xd, gc);
	
	jNPer = (*env)->NewIntArray(env, npoly);
	if (!jNPer) {
		handleJError(env, RJ_ERROR_RERROR, "gdPath.nper: jni.NewIntArray(%d) failed.", npoly);
		return;
	}
	(*env)->SetIntArrayRegion(env, jNPer, 0, npoly, (jint *) nper);
	{	int n = 0;
		for (int i = 0; i < npoly; ++i) {
			n += nper[i];
		}
		jX = newDoubleArrayPoly(env, n, x);
		jY = newDoubleArrayPoly(env, n, y);
	}
	
	(*env)->CallVoidMethod(env, xd->talk, jmGDInterfacePath, (jint) npoly, jNPer, jX, jY,
			(winding == TRUE) ? 1 : 0);
	(*env)->DeleteLocalRef(env, jNPer);
	(*env)->DeleteLocalRef(env, jX); 
	(*env)->DeleteLocalRef(env, jY);
	chkX(env);
}

static void newJavaGD_Polyline(int n, double *x, double *y,
		R_GE_gcontext *gc, NewDevDesc *dd) {
	newJavaGDDesc *xd = (newJavaGDDesc *) dd->deviceSpecific;
	JNIEnv *env= getJEnv();
	jdoubleArray xa, ya;
	
	if (!env || !xd || !xd->talk) return;
	
	checkGC(env, xd, gc);
	
	xa = newDoubleArrayPoly(env, n, x);
	ya = newDoubleArrayPoly(env, n, y);
	
	(*env)->CallVoidMethod(env, xd->talk, jmGDInterfacePolyline, n, xa, ya);
	(*env)->DeleteLocalRef(env, xa); 
	(*env)->DeleteLocalRef(env, ya);
	chkX(env);
}

static void newJavaGD_Polygon(int n, double *x, double *y,
		R_GE_gcontext *gc, NewDevDesc *dd) {
	newJavaGDDesc *xd = (newJavaGDDesc *) dd->deviceSpecific;
	JNIEnv *env= getJEnv();
	jdoubleArray xa, ya;
	
	if (!env || !xd || !xd->talk) return;
	
	checkGC(env, xd, gc);
	
	xa = newDoubleArrayPoly(env, n, x);
	ya = newDoubleArrayPoly(env, n, y);
	
	(*env)->CallVoidMethod(env, xd->talk, jmGDInterfacePolygon, n, xa, ya);
	(*env)->DeleteLocalRef(env, xa); 
	(*env)->DeleteLocalRef(env, ya);
	chkX(env);
}

static const char *convertToUTF8(const char *str,
		R_GE_gcontext *gc) {
	return Rf_reEnc(str, (gc->fontface == 5) ? CE_SYMBOL : CE_NATIVE, CE_UTF8, 1);
}

static double newJavaGD_StrWidthUTF8(const char *str, 
		R_GE_gcontext *gc, NewDevDesc *dd) {
	newJavaGDDesc *xd = (newJavaGDDesc *) dd->deviceSpecific;
	JNIEnv *env= getJEnv();
	double width;
	
	if (!env || !xd || !xd->talk) return 0.0;
	
	checkGC(env, xd, gc);
	
	const size_t strLength= strlen(str);
	jbyteArray jStr= (*env)->NewByteArray(env, strLength);
	if (!jStr) {
		handleJError(env, RJ_ERROR_RERROR, "gdStrWidth.str: " JAVA_CREATE_STRING_ERROR_MESSAGE);
		return 0.0;
	}
	(*env)->SetByteArrayRegion(env, jStr, 0, strLength, (jbyte *)str);
	width = (*env)->CallDoubleMethod(env, xd->talk, jmGDInterfaceStrWidth, jStr);
	(*env)->DeleteLocalRef(env, jStr);
	chkX(env);
	
	return width;
}

static double newJavaGD_StrWidth(const char *str,
		R_GE_gcontext *gc, NewDevDesc *dd) {
	const void *vmax= vmaxget();
	double width= newJavaGD_StrWidthUTF8(convertToUTF8(str, gc), gc, dd);
	vmaxset(vmax);
	return width;
}

static void newJavaGD_TextUTF8(double x, double y, const char *str, double rot, double hadj,
		R_GE_gcontext *gc, NewDevDesc *dd) {
	newJavaGDDesc *xd = (newJavaGDDesc *) dd->deviceSpecific;
	JNIEnv *env= getJEnv();
	
	if (!env || !xd || !xd->talk) return;
	
	checkGC(env, xd, gc);
	
	const size_t strLength= strlen(str);
	jbyteArray jStr= (*env)->NewByteArray(env, strLength);
	if (!jStr) {
		handleJError(env, RJ_ERROR_RERROR, "gdText.str: " JAVA_CREATE_STRING_ERROR_MESSAGE);
		return;
	}
	(*env)->SetByteArrayRegion(env, jStr, 0, strLength, (jbyte *)str);
	(*env)->CallVoidMethod(env, xd->talk, jmGDInterfaceText, x, y, jStr, rot, hadj);
	(*env)->DeleteLocalRef(env, jStr);
	chkX(env);
}

static void newJavaGD_Text(double x, double y, const char *str, double rot, double hadj,
		R_GE_gcontext *gc, NewDevDesc *dd) {
	const void *vmax= vmaxget();
	newJavaGD_TextUTF8(x, y, convertToUTF8(str, gc), rot, hadj, gc, dd);
	vmaxset(vmax);
}

static void newJavaGD_Raster(unsigned int *raster, int w, int h,
		double x, double y, double width, double height,
		double rot, Rboolean interpolate,
		R_GE_gcontext *gc, NewDevDesc *dd) {
	newJavaGDDesc *xd = (newJavaGDDesc *) dd->deviceSpecific;
	JNIEnv *env= getJEnv();
	int i, j;
	
	if (!env || !xd || !xd->talk) return;
	
	int withAlpha = 0;
	int count = w * h;
	jbyteArray jData= (*env)->NewByteArray(env, count * 4);
	if (!jData) {
		handleJError(env, RJ_ERROR_RERROR, "gdRaster.data: jni.NewByteArray(%d) failed.", count * 4);
		return;
	}
	{	jbyte* ba = (*env)->GetByteArrayElements(env, jData, 0);
		if (!ba) {
			(*env)->DeleteLocalRef(env, jData);
			handleJError(env, RJ_ERROR_RERROR, "gdRaster.data: jni.GetByteArrayElements(...) failed.");
			return;
		}
		for (i = 0, j = 0; i < count; i++, j+=4) {
			ba[j] = R_BLUE(raster[i]);
			ba[j+1] = R_GREEN(raster[i]);
			ba[j+2] = R_RED(raster[i]);
			if ((ba[j+3] = R_ALPHA(raster[i])) != (jbyte) 0xff) {
				withAlpha = 1;
			}
		}
		(*env)->ReleaseByteArrayElements(env, jData, ba, 0);
	}
	(*env)->CallVoidMethod(env, xd->talk, jmGDInterfaceRaster, jData, (withAlpha) ? JNI_TRUE : JNI_FALSE,
			w, h, x, y, width, height, rot, interpolate );
	(*env)->DeleteLocalRef(env, jData);
	chkX(env);
}

static SEXP newJavaGD_Cap(NewDevDesc *dd) {
	newJavaGDDesc *xd = (newJavaGDDesc *) dd->deviceSpecific;
	JNIEnv *env= getJEnv();
	
	if (!env || !xd || !xd->talk) return R_NilValue;
	
	SEXP sRaster = R_NilValue;
	jint dim[] = { -1, -1 };
	jintArray jDim = (*env)->NewIntArray(env, 2);
	if (!jDim) {
		handleJError(env, RJ_ERROR_RERROR, "gdCap.dim: jni.NewIntArray(%d) failed.", 2);
		return R_NilValue;
	}
	(*env)->SetIntArrayRegion(env, jDim, 0, 2, dim);
	jbyteArray jRaster = (*env)->CallObjectMethod(env, xd->talk, jmGDInterfaceCap, jDim);
	if (jRaster) {
		(*env)->GetIntArrayRegion(env, jDim, 0, 2, dim);
		int count = dim[0] * dim[1];
		
		PROTECT(sRaster= Rf_allocVector(INTSXP, count));
		{	unsigned int *raster= (unsigned int *) INTEGER(sRaster);
			jbyte* ba = (*env)->GetPrimitiveArrayCritical(env, jRaster, 0);
			for (int i = 0, j = 0; i < count; i++, j+=4) {
				raster[i] = R_RGB(ba[j+2], ba[j+1], ba[j+0]);
			}
			(*env)->ReleasePrimitiveArrayCritical(env, jRaster, ba, JNI_ABORT);
		}
		{	SEXP rDim;
			PROTECT(rDim= Rf_allocVector(INTSXP, 2));
			INTEGER(rDim)[0] = dim[0];
			INTEGER(rDim)[1] = dim[1];
			Rf_setAttrib(sRaster, R_DimSymbol, rDim);
		}
		UNPROTECT(2);
		
		(*env)->DeleteLocalRef(env, jRaster);
	}
	
	(*env)->DeleteLocalRef(env, jDim);
	chkX(env);
	
	return sRaster;
}

static Rboolean newJavaGD_Locator(double *x, double *y, NewDevDesc *dd) {
	newJavaGDDesc *xd = (newJavaGDDesc *) dd->deviceSpecific;
	JNIEnv *env= getJEnv();
	
	if (!env || !xd || !xd->talk) return FALSE;
	
	jdoubleArray o= (*env)->CallObjectMethod(env, xd->talk, jmGDInterfaceLocator);
	if (o) {
		jdouble *ac=(jdouble*)(*env)->GetDoubleArrayElements(env, o, 0);
		if (!ac) {
			(*env)->DeleteLocalRef(env, o);
			handleJError(env, RJ_ERROR_RERROR, "gdLocator.coord: jni.GetDoubleArrayElements(...) failed.");
			return FALSE;
		}
		*x=ac[0]; *y=ac[1];
		(*env)->ReleaseDoubleArrayElements(env, o, ac, JNI_ABORT);
		(*env)->DeleteLocalRef(env, o);
		chkX(env);
		return TRUE;
	}
	chkX(env);
	
	return FALSE;
}


/*-----------------------------------------------------------------------*/

/** fill the R device structure with callback functions */
void setupJavaGDfunctions(NewDevDesc *dd) {
    dd->close = newJavaGD_Close;
    dd->activate = newJavaGD_Activate;
    dd->deactivate = newJavaGD_Deactivate;
    dd->mode = newJavaGD_Mode;
    dd->size = newJavaGD_Size;
    dd->newPage = newJavaGD_NewPage;
    dd->canClip = TRUE;
    dd->clip = newJavaGD_Clip;
    dd->rect = newJavaGD_Rect;
    dd->circle = newJavaGD_Circle;
    dd->line = newJavaGD_Line;
    dd->path = newJavaGD_Path;
    dd->polyline = newJavaGD_Polyline;
    dd->polygon = newJavaGD_Polygon;
	dd->raster = newJavaGD_Raster;
	dd->cap = newJavaGD_Cap;
	
	dd->canHAdj = 2;
	dd->useRotatedTextInContour = TRUE;
	dd->metricInfo = newJavaGD_MetricInfo;
	dd->strWidth = newJavaGD_StrWidth;
	dd->text = newJavaGD_Text;
	dd->hasTextUTF8 = TRUE;
	dd->wantSymbolUTF8 = TRUE;
	dd->strWidthUTF8 = newJavaGD_StrWidthUTF8;
	dd->textUTF8 = newJavaGD_TextUTF8;
	
	dd->locator = newJavaGD_Locator;
	dd->newFrameConfirm = newJavaGD_NewPageConfirm;
	
#if R_GE_version >= 9
	dd->haveTransparency= 2;
	dd->haveTransparentBg= 3;
	dd->haveRaster= 2;
	dd->haveCapture= 2;
	dd->haveLocator= 2;
	dd->holdflush= newJavaGD_HoldFlush;
#if R_GE_version >= 13
    dd->setClipPath= newJavaGD_setClipPath;
    dd->releaseClipPath= newJavaGD_releaseClipPath;
    dd->setMask= newJavaGD_setMask;
    dd->releaseMask= newJavaGD_releaseMask;
    dd->setPattern= newJavaGD_setPattern;
    dd->releasePattern= newJavaGD_releasePattern;
#endif
#endif
}

/*--------- Java Initialization -----------*/

#ifdef Win32
#define PATH_SEPARATOR ';'
#else
#define PATH_SEPARATOR ':'
#endif
#define USER_CLASSPATH "."

#ifdef JNI_VERSION_1_2 
static JavaVMInitArgs vm_args;
static JavaVMOption *vm_options;
#else
#warning "** Java/JNI 1.2 or higher is required **"
** ERROR: Java/JNI 1.2 or higher is required **
/* we can't use #error to signal this on Windows due to a bug in the way dependencies are generated */
#endif

int initJVM(char *user_classpath) {
	JavaVM *jvm;
	JNIEnv *env;
	jint res;
	char *classpath;
	size_t cLength;
	int total_num_properties, propNum = 0;
	
    if(!user_classpath)
        /* use the CLASSPATH environment variable as default */
        user_classpath = (char*) getenv("CLASSPATH");
    if(!user_classpath) user_classpath = "";
    
    vm_args.version = JNI_VERSION_1_2;
    if(JNI_GetDefaultJavaVMInitArgs(&vm_args) != JNI_OK)
      error("Java/JNI 1.2 or higher is required");
        
    total_num_properties = 3; /* leave room for classpath and optional jni debug */
        
    vm_options = (JavaVMOption *) calloc(total_num_properties, sizeof(JavaVMOption));
    vm_args.version = JNI_VERSION_1_2;
    vm_args.options = vm_options;
    vm_args.ignoreUnrecognized = JNI_TRUE;
	
	cLength= strlen("-Djava.class.path=") + strlen(user_classpath) + 1;
	classpath= (char*)calloc(cLength, sizeof *classpath);
	if (!classpath) {
		error("Cannot allocate memory for classpath");
	}
	snprintf(classpath, cLength, "-Djava.class.path=%s", user_classpath);
	
    vm_options[propNum++].optionString = classpath;   
    
    /* vm_options[propNum++].optionString = "-verbose:class,jni"; */
    vm_args.nOptions = propNum;
    /* Create the Java VM */
    res = JNI_CreateJavaVM(&jvm,(void **)&env, &vm_args);

    if (res != 0 || env == NULL) {
      error("Cannot create Java Virtual Machine");
      return -1;
    }
    
    return 0;
}

/*---------------- R-accessible functions -------------------*/

SEXP RJgd_initLib(SEXP cp) {
	if (!Rf_isString(cp)) {
		error("cp is not a string vector");
	}
	
	JNIEnv *env= getJEnv();
	
	if (!getJVM()) {
		initJVM(jarClassPath);
		env= getJEnv();
	}
	if (!env) {
		error("missing JNIEnv");
	}
	checkRJUtil(env);
	
	int l= LENGTH(cp);
	for (int i = 0; i < l; i++) {
		SEXP elt= STRING_ELT(cp, i);
		if (elt != R_NaString) {
			addJClassPath(env, newJUtf8StringS(env, elt));
		}
	}
	
	return R_NilValue;
}

Rboolean createJavaGD(newJavaGDDesc *xd) {
	JNIEnv *env= getJEnv();
	jclass jc = 0;
	jobject jo = 0;
	
	if (!getJVM()) {
		initJVM(jarClassPath);
		env= getJEnv();
	}
	if (!env) {
		return FALSE;
	}
	checkRJUtil(env);
	
	char *customClass = getenv("RJGD_CLASS_NAME");
	if (!customClass) { 
		customClass = "de.walware.rj.internal.server.gr.rgd.JavaGD";
	}
	
	if (!jcGDInterface) {
		jclass jc = getJClass(env, "de.walware.rj.server.javagd.GDInterface", (RJ_GLOBAL_REF | RJ_ERROR_RERROR));
		jmGDInterfaceActivate= getJMethod(env, jc, "gdActivate", "()V", RJ_ERROR_RERROR);
		jmGDInterfaceCircle= getJMethod(env, jc, "gdCircle", "(DDD)V", RJ_ERROR_RERROR);
		jmGDInterfaceClip= getJMethod(env, jc, "gdClip", "(DDDD)V", RJ_ERROR_RERROR);
		jmGDInterfaceClose= getJMethod(env, jc, "gdClose", "()V", RJ_ERROR_RERROR);
		jmGDInterfaceDeactivate= getJMethod(env, jc, "gdDeactivate", "()V", RJ_ERROR_RERROR);
		jmGDInterfaceFlush= getJMethod(env, jc, "gdFlush", "(Z)V", RJ_ERROR_RERROR);
		jmGDInterfaceGetPPI= getJMethod(env, jc, "gdPPI", "()[D", RJ_ERROR_RERROR);
		jmGDInterfaceInit= getJMethod(env, jc, "gdInit", "(DDIDDI)[D", RJ_ERROR_RERROR);
		jmGDInterfaceLocator= getJMethod(env, jc, "gdLocator", "()[D", RJ_ERROR_RERROR);
		jmGDInterfaceLine= getJMethod(env, jc, "gdLine", "(DDDD)V", RJ_ERROR_RERROR);
		jmGDInterfaceMetricInfo= getJMethod(env, jc, "gdMetricInfo", "(I)[D", RJ_ERROR_RERROR);
		jmGDInterfaceMode= getJMethod(env, jc, "gdMode", "(I)V", RJ_ERROR_RERROR);
		jmGDInterfaceNewPage= getJMethod(env, jc, "gdNewPage", "()V", RJ_ERROR_RERROR);
		jmGDInterfaceNewPageConfirm= getJMethod(env, jc, "gdNewPageConfirm", "()Z", RJ_ERROR_RERROR);
		jmGDInterfaceOpen= getJMethod(env, jc, "gdOpen", "(I)V", RJ_ERROR_RERROR);
		jmGDInterfacePath= getJMethod(env, jc, "gdPath", "(I[I[D[DI)V", RJ_ERROR_RERROR);
		jmGDInterfacePolygon= getJMethod(env, jc, "gdPolygon", "(I[D[D)V", RJ_ERROR_RERROR);
		jmGDInterfacePolyline= getJMethod(env, jc, "gdPolyline", "(I[D[D)V", RJ_ERROR_RERROR);
		jmGDInterfaceRect= getJMethod(env, jc, "gdRect", "(DDDD)V", RJ_ERROR_RERROR);
		jmGDInterfaceSize= getJMethod(env, jc, "gdSize", "()[D", RJ_ERROR_RERROR);
		jmGDInterfaceStrWidth= getJMethod(env, jc, "gdStrWidth", "([B)D", RJ_ERROR_RERROR);
		jmGDInterfaceText= getJMethod(env, jc, "gdText", "(DD[BDD)V", RJ_ERROR_RERROR);
		jmGDInterfaceRaster= getJMethod(env, jc, "gdRaster", "([BZIIDDDDDZ)V", RJ_ERROR_RERROR);
		jmGDInterfaceCap= getJMethod(env, jc, "gdCap", "([I)[B", RJ_ERROR_RERROR);
		jmGDInterfaceSetColor= getJMethod(env, jc, "gdcSetColor", "(I)V", RJ_ERROR_RERROR);
		jmGDInterfaceSetFill= getJMethod(env, jc, "gdcSetFill", "(I)V", RJ_ERROR_RERROR);
		jmGDInterfaceSetLine= getJMethod(env, jc, "gdcSetLine", "(DIBBF)V", RJ_ERROR_RERROR);
		jmGDInterfaceSetFont= getJMethod(env, jc, "gdcSetFont", "(DDDI[B)V", RJ_ERROR_RERROR);
		jcGDInterface= jc;
	}
	
	jc = getJClass(env, customClass, (RJ_GLOBAL_REF | RJ_ERROR_RERROR));
	{	jmethodID jm = (*env)->GetMethodID(env, jc, "<init>", "()V");
		if (!jm) {
			(*env)->DeleteLocalRef(env, jc);
			handleJError(env, RJ_ERROR_RERROR, "Cannot find default constructor for GD class '%s'.", customClass);
			return FALSE;
		}
		jo = (*env)->NewObject(env, jc, jm);
		if (!jo) {
			(*env)->DeleteLocalRef(env, jc);
			handleJError(env, RJ_ERROR_RERROR, "Cannot instantiate object of GD class '%s'.", customClass);
			return FALSE;
		}
	}
	
	xd->talk = (*env)->NewGlobalRef(env, jo);
	(*env)->DeleteLocalRef(env, jo);
	xd->talkClass = jc;
	
	if (!xd->talk) {
		chkX(env);
		gdWarning("Rjgd_NewDevice: talk is null");
		return FALSE;
	}
	
	return TRUE;
}

void initJavaGD(newJavaGDDesc *xd, double *width, double *height, int *unit, double *xpi, double *ypi) {
	JNIEnv *env= getJEnv();
	
	if (!env || !xd || !xd->talk) return;
	
	jdoubleArray jo= (*env)->CallObjectMethod(env, xd->talk, jmGDInterfaceInit,
			(jdouble) *width, (jdouble) *height, (jint) *unit,
			(jdouble) *xpi, (jdouble) *ypi, xd->canvas);
	if (jo) {
		jdouble *ac = (jdouble*)(*env)->GetDoubleArrayElements(env, jo, 0);
		if (!ac) {
			if (*unit != 1) {
				*width = 672.0;
				*height = 672.0;
			}
			(*env)->DeleteLocalRef(env, jo);
			handleJError(env, RJ_ERROR_RERROR, "init.size: jni.GetDoubleArrayElements(...) failed.");
			return;
		}
		*width = ac[0];
		*height = ac[1];
		(*env)->ReleaseDoubleArrayElements(env, jo, ac, JNI_ABORT);
		(*env)->DeleteLocalRef(env, jo);
	} else {
		gdWarning("gdInit: method returned null");
		if (*unit != 1) {
			*width = 672.0;
			*height = 672.0;
		}
	}
	chkX(env);
}

void openJavaGD(NewDevDesc *dd)
{	
	newJavaGDDesc *xd = (newJavaGDDesc *) dd->deviceSpecific;
	int devNr = ndevNumber(dd);
	
	JNIEnv *env= getJEnv();
	
	if (!env || !xd || !xd->talk || !jmGDInterfaceOpen) return;
	
	(*env)->CallVoidMethod(env, xd->talk, jmGDInterfaceOpen, (jint) devNr);
	chkX(env);
}

void getJavaGDPPI(NewDevDesc *dd, double *xpi, double *ypi) {
	newJavaGDDesc *xd = (newJavaGDDesc *) dd->deviceSpecific;
	JNIEnv *env= getJEnv();
	
	if (!env || !xd || !xd->talk) return;
	
	*xpi = 96.0;
	*ypi = 96.0;
	
	jdoubleArray jo= (*env)->CallObjectMethod(env, xd->talk, jmGDInterfaceGetPPI);
	if (jo) {
		jdouble *ac = (jdouble*)(*env)->GetDoubleArrayElements(env, jo, 0);
		if (!ac) {
			(*env)->DeleteLocalRef(env, jo);
			handleJError(env, RJ_ERROR_RERROR, "getPPI.values: jni.GetDoubleArrayElements(...) failed.");
			return;
		}
		if (ac[0] > 0.0 && ac[1] > 0.0) {
			*xpi = ac[0];
			*ypi = ac[1];
		}
		(*env)->ReleaseDoubleArrayElements(env, jo, ac, JNI_ABORT);
		(*env)->DeleteLocalRef(env, jo);
	} else {
		gdWarning("getPPI: method returned null, using default");
	}
	chkX(env);
}
