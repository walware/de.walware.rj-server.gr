/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # All rights reserved. This program and the accompanying materials
 # are made available under the terms of the Apache License v2.0
 # which accompanies this distribution, and is available at
 # https://www.apache.org/licenses/LICENSE-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

// Utility methods for R package libs using JNI with R-Java classloader (from JRI or compatible)


#ifndef RJUTIL_H
#define RJUTIL_H

#include <Rdefines.h>
#include <jni.h>


// static const unsigned int
#define RJ_ERROR_MASK         0b00000111
#define RJ_ERROR_RWARNING     0b00000010
#define RJ_ERROR_RERROR       0b00000011
#define RJ_ERROR_CWARNING     0b00000100
#define RJ_GLOBAL_REF         0b00010000
#define RJ_ERR_ON_CONV        0b00100000


void checkRJUtil(JNIEnv *env);
JavaVM *getJVM(void);
JNIEnv *getJEnv(void);

void addJClassPath(JNIEnv *env, jbyteArray path);


jbyteArray newJUtf8StringC(JNIEnv *env, const char *s);
jbyteArray newJUtf8StringCF(JNIEnv *env, const char *s, int flags, const char *operation);
jbyteArray newJUtf8StringS(JNIEnv *env, SEXP cS);


jclass getJClass(JNIEnv *env, const char *name, int flags);
jmethodID getJMethod(JNIEnv *env, jclass jClass, const char *name, const char *sig, int flags);


void handleJError(JNIEnv *env, unsigned int flags, const char *message, ...);
void checkJError(JNIEnv *env, unsigned int flags, const char *message, ...);
void clearJError(JNIEnv *env);

#define JAVA_CREATE_STRING_ERROR_MESSAGE "Failed to create Java string."


#endif
