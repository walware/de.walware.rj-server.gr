#ifndef _DEV_JAVAGD_H
#define _DEV_JAVAGD_H

#define JAVAGD_VER 0x04000B /* 4.0.11 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <R.h>
#include <Rversion.h>
#include <Rinternals.h>
#include <R_ext/GraphicsEngine.h>
#include <R_ext/GraphicsDevice.h>
#include <jni.h>

#ifndef NewDevDesc
#define NewDevDesc DevDesc
#endif


typedef struct {
    /* Graphics Parameters */
	
    double cex;				/* Character expansion */
    
    int lty;				/* Line type */
    double lwd;				/* Line width */
    
    int col;				/* Color */
    int fill;				/* Fill color */
    int canvas;				/* Canvas color */
    
    int fontface;			/* Typeface */
    int fontsize;			/* Size in points */
    int basefontface;		/* Initial Typeface */
    int basefontsize;		/* Initial Size in points */


    jobject talk; /* object associated with this graphics */
    jclass  talkClass; /* class of the talk object (cached) */

    int holdlevel;                      /* current hold level (0=no holding) */

} newJavaGDDesc;

#endif

