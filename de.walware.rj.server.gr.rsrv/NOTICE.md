# Notices for R package 'rj' - RJSrv for GNU R

This package provides an RJSrv (R Server for RJ) implementation for GNU R.


## Declared Project Licenses

See file [LICENSE.note](pkg/LICENSE.note).

A copy of the licenses is contained in the file [LICENSE](pkg/LICENSE).


## Third-party Content

### Eclipse StatET

The package includes content derived from software developed by the Eclipse Foundation as part of
the StatET project (https://www.eclipse.org/statet).

### JRI

The package includes content derived from software developed by Simon Urbanek <simon.urbanek@r-project.org>
and others as part of the JRI project.
