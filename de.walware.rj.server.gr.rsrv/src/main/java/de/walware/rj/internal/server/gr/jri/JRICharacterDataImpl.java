/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Apache License, Version 2.0 which is available at
 # https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package de.walware.rj.internal.server.gr.jri;

import java.io.IOException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.impl.RCharacter32Store;


@NonNullByDefault
public class JRICharacterDataImpl extends RCharacter32Store {
	
	
	public JRICharacterDataImpl(final @Nullable String[] values) {
		super(values);
	}
	
	public JRICharacterDataImpl(final @Nullable String[] values, final int length) {
		super(values, length);
	}
	
	public JRICharacterDataImpl(final RJIO io, final int l) throws IOException {
		super(io, l);
	}
	
	
	public @Nullable String[] getJRIValueArray() {
		final int l= length();
		if (this.charValues.length == l) {
			return this.charValues;
		}
		final @Nullable String[] array= new @Nullable String[l];
		System.arraycopy(this.charValues, 0, array, 0, l);
		return array;
	}
	
}
