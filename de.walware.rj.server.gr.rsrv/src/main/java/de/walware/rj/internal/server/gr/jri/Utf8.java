/*=============================================================================#
 # Copyright (c) 2022, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Apache License, Version 2.0 which is available at
 # https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink - initial API and implementation
 #=============================================================================*/

package de.walware.rj.internal.server.gr.jri;

import java.nio.charset.StandardCharsets;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;


@NonNullByDefault
public final class Utf8 {
	
	
	public static final byte[] S_NEWLINE= "\n".getBytes(StandardCharsets.UTF_8); //$NON-NLS-1$
	public static final byte[] S_C_NEWLINE= "c\n".getBytes(StandardCharsets.UTF_8); //$NON-NLS-1$
	
	public static final byte[] S_BROWSE_PROMPT= "Browse[".getBytes(StandardCharsets.UTF_8); //$NON-NLS-1$
	public static final byte[] S_EXIT= "exit".getBytes(StandardCharsets.UTF_8); //$NON-NLS-1$
	
	
	public static boolean startsWith(final byte[] s1, final byte[] s2) {
		final int l= s2.length;
		if (l == 0) {
			return true;
		}
		if (s1.length < l) {
			return false;
		}
		for (int i= 0; i < l; i++) {
			if (s1[i] != s2[i]) {
				return false;
			}
		}
		return true;
	}
	
	public static int indexOf(final byte[] s1, final byte[] s2) {
		final int l= s2.length;
		if (l == 0) {
			return 0;
		}
		if (s1.length < l) {
			return -1;
		}
		final int lastStart= s1.length - l;
		ITER_START: for (int start= 0; start <= lastStart; start++) {
			if (s1[start] == s2[0]) {
				for (int i= start + 1, j= 1; j < l; i++, j++) {
					if (s1[i] != s2[j]) {
						continue ITER_START;
					}
				}
				return start;
			}
		}
		return -1;
	}
	
	public static boolean contains(final byte[] s1, final byte[] s2) {
		return (indexOf(s1, s2) >= 0);
	}
	
	
	private Utf8() {
	}
	
}
