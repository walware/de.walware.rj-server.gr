/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Apache License, Version 2.0 which is available at
 # https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package de.walware.rj.internal.server.gr.jri;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.isAnyNull;
import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullElse;

import static org.eclipse.statet.rj.data.RObjectFactory.F_ONLY_STRUCT;

import static de.walware.rj.internal.server.gr.jri.JRISrvErrors.CODE_DATA_ASSIGN_DATA;
import static de.walware.rj.internal.server.gr.jri.JRISrvErrors.CODE_DATA_COMMON;
import static de.walware.rj.internal.server.gr.jri.JRISrvErrors.CODE_DATA_EVAL_DATA;
import static de.walware.rj.internal.server.gr.jri.JRISrvErrors.LOGGER;

import java.lang.reflect.Field;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.logging.Level;

import org.rosuda.JRI.REXP;
import org.rosuda.JRI.Rengine;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RCharacterStore;
import org.eclipse.statet.rj.data.RDataFrame;
import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.REnvironment;
import org.eclipse.statet.rj.data.RFactorStore;
import org.eclipse.statet.rj.data.RLanguage;
import org.eclipse.statet.rj.data.RList;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RObjectFactory;
import org.eclipse.statet.rj.data.RReference;
import org.eclipse.statet.rj.data.RS4Object;
import org.eclipse.statet.rj.data.RStore;
import org.eclipse.statet.rj.data.RVector;
import org.eclipse.statet.rj.data.impl.DefaultRObjectFactory;
import org.eclipse.statet.rj.data.impl.RCharacter32Store;
import org.eclipse.statet.rj.data.impl.RDataFrame32Impl;
import org.eclipse.statet.rj.data.impl.RFactor32Store;
import org.eclipse.statet.rj.data.impl.RFactorStructStore;
import org.eclipse.statet.rj.data.impl.RFunctionImpl;
import org.eclipse.statet.rj.data.impl.RMissingImpl;
import org.eclipse.statet.rj.data.impl.RNullImpl;
import org.eclipse.statet.rj.data.impl.ROtherImpl;
import org.eclipse.statet.rj.data.impl.RPromiseImpl;
import org.eclipse.statet.rj.data.impl.RReferenceImpl;
import org.eclipse.statet.rj.data.impl.RS4ObjectImpl;
import org.eclipse.statet.rj.data.impl.SimpleRList;
import org.eclipse.statet.rj.server.RjsException;
import org.eclipse.statet.rj.server.rh.Handle;
import org.eclipse.statet.rj.server.rh.RhEngine;


@NonNullByDefault
public final class JRISrvRni {
	
	
	public static final byte EVAL_MODE_DEFAULT= 0;
	public static final byte EVAL_MODE_FORCE= 1;
	public static final byte EVAL_MODE_DATASLOT= 2;
	
	public static class RNullPointerException extends RjsException {
		
		private static final long serialVersionUID= 1L;
		
		public RNullPointerException() {
			super(0, "R engine returned unexpected null pointer (out of memory?).");
		}
		
	}
	
	
	private static class Arg {
		
		private final long symP;
		private final long valueP;
		
		public Arg(final long symP, final long valueP) {
			this.symP= symP;
			this.valueP= valueP;
		}
		
		public Arg(final long valueP) {
			this.symP= 0;
			this.valueP= valueP;
		}
		
	}
	
	
	private static final String[] EMPTY_STRING_ARRAY= new String[0];
	private static final RObject[] EMPTY_ROBJECT_ARRAY= new RObject[0];
	private static final RReferenceImpl NULL_REF= new RReferenceImpl(0, RObject.TYPE_NULL, RObject.CLASSNAME_NULL);
	private static final String[] DATA_NAME_ARRAY= new String[] { ".Data" }; //$NON-NLS-1$
	
	
	private final Rengine rEngine;
	private final GRhEngine rhEngine;
	
	public final long NULL_P;
	public final long Unbound_P;
	public final long MissingArg_P;
	
	public final long Base_EnvP;
	public final long BaseNamespace_EnvP;
	public final long Global_EnvP;
	public final long Empty_EnvP;
	public final long Autoload_EnvP;
	
	public final long Assign_SymP;
	public final long Block_SymP;
	public final long Ellipsis_SymP;
	public final long at_SymP;
	public final long dim_SymP;
	public final long edit_SymP;
	public final long encoding_SymP;
	public final long expr_SymP;
	public final long error_SymP;
	public final long imaginary_SymP;
	public final long id_SymP;
	public final long isGeneric_SymP;
	public final long fdef_SymP;
	public final long filename_SymP;
	public final long flags_SymP;
	public final long function_SymP;
	public final long from_SymP;
	public final long lengthOut_SymP;
	public final long name_SymP;
	public final long names_SymP;
	public final long ns_SymP;
	public final long on_SymP;
	public final long onExit_SymP;
	public final long original_SymP;
	public final long output_SymP;
	public final long real_SymP;
	public final long signature_SymP;
	public final long value_SymP;
	public final long what_SymP;
	public final long where_SymP;
	public final long which_SymP;
	public final long wd_SymP;
	public final long x_SymP;
	public final long z_SymP;
	private final Map<String, Handle> symbolMap= new HashMap<>();
	private final Map<String, Handle> stringMap= new HashMap<>();
	
	public final long TRUE_BoolP;
	public final long FALSE_BoolP;
	
	public final long appFilePath_SymP;
	public final long appElementId_SymP;
	
	private final long class_SymP;
	private final long dimnames_SymP;
	private final long rowNames_SymP;
	private final long levels_SymP;
	private final long new_SymP;
	private final long newClass_SymP;
	private final long parent_SymP;
	private final long slotNULL_SymP;
	
	private final long factorClassStringP;
	private final long orderedClassStringP;
	private final long dataframeClassStringP;
	private final long newEnvFunP;
	private final long complexFunP;
	private final long ReFunP;
	private final long ImFunP;
	private final long optionsSymP;
	private final long seqIntFunP;
	
	private final long getNamespaceFunP;
	private final long getNamespaceExportedNamesFunP;
	private final long getNamespaceExportedValueFunP;
	
	private final long tryCatchFunP;
	private final long slotNamesFunP;
	private final long getFHeaderFunP;
	private final long deparseLinesXCallP;
	private final long evalErrorHandlerExprP;
	private final long rniTempEvalClassExprP;
	
	public final long rniSafeBaseExecEnvP;
	public final long rniSafeGlobalExecEnvP;
	
	private final long rniTempEnvP;
	
	public final long rjNamespaceEnvP;
	private final long rjTmpEnvP;
	
	public final long evalDummy_ExprP;
	
	private int rniProtectedCounter;
	
	private int stackPos= 0;
	
	private int currentDepth;
	private final int[] currentDepthStack= new int[255];
	private int maxDepth;
	private final int[] maxDepthStack= new int[255];
	
	private int maxEnvsLength= 10000;
	private final int[] maxEnvsLengthStack= new int[255];
	private int maxListsLength= 10000;
	private final int[] maxListsLengthStack= new int[255];
	
	private boolean rniTempEvalAssigned;
	
	boolean rniInterrupted;
	
	private final StringBuilder sBuilder= new StringBuilder();
	
	
	public JRISrvRni(final Rengine rEngine) throws RjsException {
		this.rEngine= rEngine;
		this.rhEngine= new GRhEngine(rEngine);
		
		final int savedProtected= saveProtected();
		try {
			this.NULL_P= this.rEngine.rniSpecialObject(Rengine.SO_NilValue);
			this.Unbound_P= this.rEngine.rniSpecialObject(Rengine.SO_UnboundValue);
			this.MissingArg_P= this.rEngine.rniSpecialObject(Rengine.SO_MissingArg);
			this.Base_EnvP= this.rEngine.rniSpecialObject(Rengine.SO_BaseEnv);
			this.BaseNamespace_EnvP= this.rEngine.rniSpecialObject(Rengine.SO_BaseNamespaceEnv);
			this.Global_EnvP= this.rEngine.rniSpecialObject(Rengine.SO_GlobalEnv);
			this.Empty_EnvP= this.rEngine.rniSpecialObject(Rengine.SO_EmptyEnv);
			{	final long p= this.rEngine.rniEval(this.rEngine.rniInstallSymbol(".AutoloadEnv"), //$NON-NLS-1$
						this.Base_EnvP );
				this.Autoload_EnvP= (p != 0 && this.rEngine.rniExpType(p) == REXP.ENVSXP) ? p : 0;
			}
			
			this.Assign_SymP= installSymbol("<-"); //$NON-NLS-1$
			this.Block_SymP= installSymbol("{"); //$NON-NLS-1$
			this.Ellipsis_SymP= installSymbol("..."); //$NON-NLS-1$
			this.at_SymP= installSymbol("at"); //$NON-NLS-1$
			this.newClass_SymP= installSymbol("Class"); //$NON-NLS-1$
			this.class_SymP= installSymbol("class"); //$NON-NLS-1$
			this.dim_SymP= installSymbol("dim"); //$NON-NLS-1$
			this.dimnames_SymP= installSymbol("dimnames"); //$NON-NLS-1$
			this.edit_SymP= installSymbol("edit"); //$NON-NLS-1$
			this.encoding_SymP= installSymbol("encoding"); //$NON-NLS-1$
			this.error_SymP= installSymbol("error"); //$NON-NLS-1$
			this.expr_SymP= installSymbol("expr"); //$NON-NLS-1$
			this.fdef_SymP= installSymbol("fdef"); //$NON-NLS-1$
			this.filename_SymP= installSymbol("filename"); //$NON-NLS-1$
			this.flags_SymP= installSymbol("flags"); //$NON-NLS-1$
			this.function_SymP= installSymbol("function"); //$NON-NLS-1$
			this.from_SymP= installSymbol("from"); //$NON-NLS-1$
			this.id_SymP= installSymbol("id"); //$NON-NLS-1$
			this.isGeneric_SymP= installSymbol("isGeneric"); //$NON-NLS-1$
			this.imaginary_SymP= installSymbol("imaginary"); //$NON-NLS-1$
			this.lengthOut_SymP= installSymbol("length.out"); //$NON-NLS-1$
			this.levels_SymP= installSymbol("levels"); //$NON-NLS-1$
			this.name_SymP= installSymbol("name"); //$NON-NLS-1$
			this.names_SymP= installSymbol("names"); //$NON-NLS-1$
			this.new_SymP= installSymbol("new"); //$NON-NLS-1$
			this.ns_SymP= installSymbol("ns"); //$NON-NLS-1$
			this.on_SymP= installSymbol("on"); //$NON-NLS-1$
			this.onExit_SymP= installSymbol("on.exit"); //$NON-NLS-1$
			this.optionsSymP= installSymbol("options"); //$NON-NLS-1$
			this.original_SymP= installSymbol("original"); //$NON-NLS-1$
			this.output_SymP= installSymbol("output"); //$NON-NLS-1$
			this.parent_SymP= installSymbol("parent"); //$NON-NLS-1$
			this.real_SymP= installSymbol("real"); //$NON-NLS-1$
			this.rowNames_SymP= installSymbol("row.names"); //$NON-NLS-1$
			this.signature_SymP= installSymbol("signature"); //$NON-NLS-1$
			this.slotNULL_SymP= this.rEngine.rniInstallSymbol("\u0001NULL\u0001"); //$NON-NLS-1$
			this.value_SymP= installSymbol("value"); //$NON-NLS-1$
			this.what_SymP= installSymbol("what"); //$NON-NLS-1$
			this.where_SymP= installSymbol("where"); //$NON-NLS-1$
			this.which_SymP= installSymbol("which"); //$NON-NLS-1$
			this.wd_SymP= installSymbol("wd"); //$NON-NLS-1$
			this.x_SymP= installSymbol("x"); //$NON-NLS-1$
			this.z_SymP= installSymbol("z"); //$NON-NLS-1$
			
			this.appFilePath_SymP= this.rEngine.rniInstallSymbol("statet.Path");
			this.appElementId_SymP= this.rEngine.rniInstallSymbol("statet.ElementId");
			
			this.TRUE_BoolP= checkAndPreserve(this.rEngine.rniPutBoolArray(
					new boolean[] { true } ));
			this.FALSE_BoolP= checkAndPreserve(this.rEngine.rniPutBoolArray(
					new boolean[] { false } ));
			
			this.orderedClassStringP= checkAndPreserve(this.rEngine.rniPutStringArray(
					new String[] { "ordered", "factor" } )); //$NON-NLS-1$ //$NON-NLS-2$
			this.factorClassStringP= checkAndPreserve(this.rEngine.rniPutString("factor")); //$NON-NLS-1$
			this.dataframeClassStringP= checkAndPreserve(this.rEngine.rniPutString("data.frame")); //$NON-NLS-1$
			
			this.newEnvFunP= checkAndPreserve(this.rEngine.rniEval(
					installSymbol("new.env"), //$NON-NLS-1$
					this.Base_EnvP ));
			
			this.complexFunP= checkAndPreserve(this.rEngine.rniEval(
					installSymbol("complex"), //$NON-NLS-1$
					this.Base_EnvP ));
			this.ReFunP= checkAndPreserve(this.rEngine.rniEval(
					installSymbol("Re"), //$NON-NLS-1$
					this.Base_EnvP ));
			this.ImFunP= checkAndPreserve(this.rEngine.rniEval(
					installSymbol("Im"), //$NON-NLS-1$
					this.Base_EnvP ));
			
			this.getNamespaceFunP= checkAndPreserve(this.rEngine.rniEval(
					installSymbol("getNamespace"), //$NON-NLS-1$
					this.Base_EnvP ));
			this.getNamespaceExportedNamesFunP= checkAndPreserve(this.rEngine.rniEval(
					installSymbol("getNamespaceExports"), //$NON-NLS-1$
					this.Base_EnvP ));
			this.getNamespaceExportedValueFunP= checkAndPreserve(this.rEngine.rniEval(
					installSymbol("getExportedValue"), //$NON-NLS-1$
					this.Base_EnvP ));
			
			this.seqIntFunP= checkAndPreserve(this.rEngine.rniEval(
					installSymbol("seq.int"), //$NON-NLS-1$
					this.Base_EnvP ));
			
			this.tryCatchFunP= checkAndPreserve(this.rEngine.rniEval(
					installSymbol("tryCatch"), //$NON-NLS-1$
					this.Base_EnvP ));
			
			{	final long paste0FunP= protect(this.rEngine.rniEval(
						installSymbol("paste"), //$NON-NLS-1$
						this.Base_EnvP ));
				final long deparseFunP= protect(this.rEngine.rniEval(
						installSymbol("deparse"), //$NON-NLS-1$
						this.Base_EnvP ));
				
				final long controlSymP= this.rEngine.rniInstallSymbol("control"); //$NON-NLS-1$
				final long backtickSymP= this.rEngine.rniInstallSymbol("backtick"); //$NON-NLS-1$
				final long widthCutoffSymP= this.rEngine.rniInstallSymbol("width.cutoff"); //$NON-NLS-1$
				final long collapseSymP= this.rEngine.rniInstallSymbol("collapse"); //$NON-NLS-1$
				
				final long deparseControlValueP= protect(this.rEngine.rniPutStringArray(
						new String[] { "keepInteger", "keepNA" } )); //$NON-NLS-1$ //$NON-NLS-2$
				
				{	// function(x)paste(deparse(expr=args(name=x),control=c("keepInteger", "keepNA"),width.cutoff=500L),collapse="")
					final long fArgsP= protect(this.rEngine.rniCons(
							this.MissingArg_P, this.NULL_P,
							this.x_SymP, false ));
					final long argsFunP= protect(this.rEngine.rniEval(
							installSymbol("args"), //$NON-NLS-1$
							this.Base_EnvP ));
					final long argsCallP= protect(installCall(argsFunP, new Arg[] {
							new Arg(this.name_SymP, this.x_SymP)
					}));
					final long deparseCallP= protect(installCall(deparseFunP, new Arg[] {
							new Arg(this.expr_SymP, argsCallP),
							new Arg(controlSymP, deparseControlValueP),
							new Arg(backtickSymP, this.TRUE_BoolP),
							new Arg(widthCutoffSymP, this.rEngine.rniPutIntArray(new int[] { 500 }))
					}));
					final long fBodyP= installCall(paste0FunP, new Arg[] {
							new Arg(deparseCallP),
							new Arg(collapseSymP, this.rEngine.rniPutString(""))
					});
					this.getFHeaderFunP= checkAndPreserve(this.rEngine.rniEval(this.rEngine.rniCons(
							this.function_SymP, this.rEngine.rniCons(
									fArgsP, this.rEngine.rniCons(
											fBodyP, this.NULL_P,
											0, false ),
									0, false ),
							0, true ),
							this.Base_EnvP ));
				}
				
				// deparse(expr= x,control= c("keepInteger", "keepNA"), backtick= TRUE, width.cutoff=80L)
				this.deparseLinesXCallP= checkAndPreserve(installCall(deparseFunP, new Arg[] {
						new Arg(this.expr_SymP, this.x_SymP),
						new Arg(controlSymP, deparseControlValueP),
						new Arg(backtickSymP, this.TRUE_BoolP),
						new Arg(widthCutoffSymP, this.rEngine.rniPutIntArray(new int[] { 100 }))
				}));
			}
			
			this.evalErrorHandlerExprP= checkAndPreserve(this.rEngine.rniCons(
					this.rEngine.rniEval(this.rEngine.rniParse("function(e){" +
							"s<-raw(5);" +
							"class(s)<-\".rj.eval.error\";" +
							"attr(s,\"error\")<-e;" +
							"attr(s,\"output\")<-paste(capture.output(print(e)),collapse=\"\\n\");" +
							"invisible(s);}", 1 ), 0 ), this.NULL_P,
					this.error_SymP, false ));
			
			this.rniTempEvalClassExprP= checkAndPreserve(this.rEngine.rniParse("class(x);", 1));
			
			this.evalDummy_ExprP= checkAndPreserve(this.rEngine.rniParse("1+1;", 1));
			
			this.rniSafeBaseExecEnvP= checkAndPreserve(this.rEngine.rniEval(this.rEngine.rniCons(
							this.newEnvFunP, this.rEngine.rniCons(
									this.Base_EnvP, this.NULL_P,
									this.parent_SymP, false ),
							0, true ),
					this.Base_EnvP ));
			this.rniSafeGlobalExecEnvP= checkAndPreserve(createNewEnv(this.Global_EnvP));
			
			this.rjNamespaceEnvP= checkAndPreserve(getNamespaceEnvP("rj"));
			getNamespaceEnvP("methods");
			this.slotNamesFunP= checkAndPreserve(this.rEngine.rniEval(
					this.rEngine.rniParse("methods::.slotNames", 1), //$NON-NLS-1$
					this.Base_EnvP ));
			
			this.rniTempEnvP= checkAndPreserve(createNewEnv(this.Global_EnvP));
			
			this.rjTmpEnvP= this.rEngine.rniGetVarResolve(".rj.tmp", this.rjNamespaceEnvP);
			
			if (LOGGER.isLoggable(Level.FINER)) {
				final StringBuilder sb= new StringBuilder("Rni Pointers:");
				
				final Field[] fields= getClass().getDeclaredFields();
				for (final Field field : fields) {
					final String name= field.getName();
					if (name.endsWith("P") && Long.TYPE.equals(field.getType())) {
						sb.append("\n\t");
						sb.append(name.substring(0, name.length() - 1));
						sb.append("= ");
						try {
							final long p= field.getLong(this);
							sb.append("0x");
							sb.append(Long.toHexString(p));
						}
						catch (final Exception e) {
							sb.append(e.getMessage());
						}
					}
				}
				
				LOGGER.log(Level.FINER, sb.toString());
			}
		}
		finally {
			looseProtected(savedProtected);
		}
	}
	
	
	public Rengine getREngine() {
		return this.rEngine;
	}
	
	public RhEngine getRhEngine() {
		return this.rhEngine;
	}
	
	public long protect(final long p) {
		this.rEngine.rniProtect(p);
		this.rniProtectedCounter++;
		return p;
	}
	
	public long checkAndProtect(final long p) throws RNullPointerException {
		if (p == 0) {
			throw new RNullPointerException();
		}
		this.rEngine.rniProtect(p);
		this.rniProtectedCounter++;
		return p;
	}
	
	public int saveProtected() {
		return this.rniProtectedCounter;
	}
	
	public void looseProtected(final int saved) {
		final int n= this.rniProtectedCounter - saved;
		if (n > 0) {
			this.rEngine.rniUnprotect(n);
			this.rniProtectedCounter= saved;
		}
	}
	
	public void looseProtectedOpt(final int saved) {
		final int n= this.rniProtectedCounter - saved;
		if (n > 500) {
			this.rEngine.rniUnprotect(n);
			this.rniProtectedCounter= saved;
		}
	}
	
	public long check(final long p) throws RNullPointerException {
		if (p == 0) {
			throw new RNullPointerException();
		}
		return p;
	}
	
	public long checkAndPreserve(final long p) throws RNullPointerException {
		if (p == 0) {
			throw new RNullPointerException();
		}
		this.rEngine.rniPreserve(p);
		return p;
	}
	
	public void newDataLevel(final int maxDepth, final int maxEnvLength, final int maxListLength) {
		this.currentDepthStack[this.stackPos]= this.currentDepth;
		this.currentDepth= 0;
		this.maxDepthStack[this.stackPos]= this.maxDepth;
		this.maxDepth= maxDepth;
		this.maxEnvsLengthStack[this.stackPos]= this.maxEnvsLength;
		this.maxEnvsLength= maxEnvLength;
		this.maxListsLengthStack[this.stackPos]= this.maxListsLength;
		this.maxListsLength= maxListLength;
		
		this.stackPos++;
	}
	
	public void exitDataLevel() {
		this.stackPos--;
		this.currentDepth= this.currentDepthStack[this.stackPos];
		this.maxDepth= this.maxDepthStack[this.stackPos];
		this.maxEnvsLength= this.maxEnvsLengthStack[this.stackPos];
		this.maxListsLength= this.maxListsLengthStack[this.stackPos];
		
		if (this.rniTempEvalAssigned) {
			this.rEngine.rniAssignVarBySym(this.x_SymP, this.NULL_P, this.rniTempEnvP);
			this.rniTempEvalAssigned= false;
		}
	}
	
	
	public Handle installSymbolHandle(final String name) throws RNullPointerException {
		Handle handle= this.symbolMap.get(name);
		if (handle == null) {
			final long p= check(this.rEngine.rniInstallSymbol(name));
			handle= new Handle(p);
			this.symbolMap.put(name, handle);
		}
		return handle;
	}
	
	public long installSymbol(final String name) throws RNullPointerException {
		Handle handle= this.symbolMap.get(name);
		if (handle == null) {
			final long p= check(this.rEngine.rniInstallSymbol(name));
			handle= new Handle(p);
			this.symbolMap.put(name, handle);
		}
		return handle.p;
	}
	
	public Handle installStringHandle(final String name) throws RNullPointerException {
		Handle handle= this.stringMap.get(name);
		if (handle == null) {
			final long p= checkAndPreserve(this.rEngine.rniPutString(name));
			handle= new Handle(p);
			this.stringMap.put(name, handle);
		}
		return handle;
	}
	
	public long installString(final String name) throws RNullPointerException {
		Handle handle= this.stringMap.get(name);
		if (handle == null) {
			final long p= checkAndPreserve(this.rEngine.rniPutString(name));
			handle= new Handle(p);
			this.stringMap.put(name, handle);
		}
		return handle.p;
	}
	
	public long createFCall(final String name, final RList args) throws RjsException {
		long argsP= this.NULL_P;
		for (int i= (int) args.getLength() - 1; i >= 0; i--) {
			final String argName= args.getName(i);
			final RObject argValue= args.get(i);
			final long argValueP;
			if (argValue != null) {
				argValueP= assignDataObject(argValue);
			}
			else {
				argValueP= this.MissingArg_P;
			}
			argsP= protect(this.rEngine.rniCons(argValueP, argsP,
					(argName != null) ? installSymbol(argName) : 0, false ));
		}
		long funP;
		if (name.indexOf(':') > 0) {
			funP= this.rEngine.rniParse(name, 1);
			long[] list;
			if (funP != 0 && (list= this.rEngine.rniGetVector(funP)) != null && list.length == 1) {
				funP= list[0];
			}
			else {
				throw new RjsException(CODE_DATA_COMMON | 0x4, "The reference to the function is invalid.");
			}
		}
		else {
			funP= installSymbol(name);
		}
		return this.rEngine.rniCons(funP, argsP, 0, true);
	}
	
	public long resolveExpression(final String expression) throws RjsException {
		final long exprP= this.rEngine.rniParse(expression, -1);
		if (this.rEngine.rniExpType(exprP) != REXP.EXPRSXP) {
			throw new RjsException((CODE_DATA_COMMON | 0x3),
					"The specified expression is invalid (syntax error)." );
		}
		final long[] expressionsP= this.rEngine.rniGetVector(exprP);
		if (expressionsP == null || expressionsP.length != 1) {
			throw new RjsException((CODE_DATA_COMMON | 0x3),
					"The specified expression is invalid (not a single expression)." );
		}
		return expressionsP[0];
	}
	
	public long resolveEnvironment(final @Nullable RObject data) throws RjsException {
		if (data == null) {
			return this.Global_EnvP;
		}
		try {
			long envP= 0;
			switch (data.getRObjectType()) {
			case RObject.TYPE_REFERENCE:
				envP= ((RReference) data).getHandle();
				break;
			case RObject.TYPE_LANGUAGE:
				envP= evalExpr(assignDataObject(data),
						0, CODE_DATA_COMMON );
				break;
			default:
				throw new RjsException(CODE_DATA_COMMON, "Unsupported specification.");
			}
			if (envP == 0 || this.rEngine.rniExpType(envP) != REXP.ENVSXP) {
				throw new RjsException(CODE_DATA_COMMON, "Not an environment.");
			}
			return envP;
		}
		catch (final RjsException e) {
			throw new RjsException(CODE_DATA_COMMON | 0xa, "Could not resolve the environment.", e);
		}
	}
	
	public long evalExpr(long exprP, final long envP, final int code) throws RjsException {
		exprP= this.rEngine.rniCons(
				this.tryCatchFunP, this.rEngine.rniCons(
						exprP, this.evalErrorHandlerExprP,
						this.expr_SymP, false ),
				0, true );
		final long objP= this.rEngine.rniEval(exprP, envP);
		if (objP == 0) {
			if (this.rniInterrupted) {
				throw new CancellationException();
			}
			throw new IllegalStateException("JRI returned error code " + objP + " (pointer= 0x" + Long.toHexString(exprP) + ")");
		}
		protect(objP);
		if (this.rEngine.rniExpType(objP) == REXP.RAWSXP) {
			final String className1= getClass1(objP);
			if (className1 != null && className1.equals(".rj.eval.error")) {
				String message= nonNullElse(
						this.rEngine.rniGetAttrStringBySym(objP, this.output_SymP),
						"<no information available>" );
				switch (code) {
				case (CODE_DATA_EVAL_DATA | 0x3):
					message= "An error occurred when evaluation the specified expression in R " + message + ".";
					break;
				case (CODE_DATA_EVAL_DATA | 0x4):
					message= "An error occurred when evaluation the function in R " + message + ".";
					break;
				case (CODE_DATA_ASSIGN_DATA | 0x3):
					message= "An error occurred when assigning the value to the specified expression in R " + message + ".";
					break;
				case (CODE_DATA_ASSIGN_DATA | 0x8):
					message= "An error occurred when instancing an S4 object in R " + message + ".";
					break;
				default:
					message= message + ".";
					break;
				}
				throw new RjsException(code, message);
			}
		}
		return objP;
	}
	
	/**
	 * Put an {@link RObject RJ R object} into JRI, and get back the pointer to the object
	 * (Java to R).
	 * 
	 * @param obj an R object
	 * @return long protected R pointer
	 * @throws RjsException 
	 */ 
	public long assignDataObject(final RObject obj) throws RjsException {
		RStore<?> names;
		long objP;
		switch(obj.getRObjectType()) {
		case RObject.TYPE_NULL:
		case RObject.TYPE_MISSING:
			return this.NULL_P;
		case RObject.TYPE_VECTOR: {
			objP= assignDataStore(obj.getData());
			names= ((RVector<?>) obj).getNames();
			if (names != null) {
				this.rEngine.rniSetAttrBySym(objP, this.names_SymP, assignDataStore(names));
			}
			return objP; }
		case RObject.TYPE_ARRAY:
			objP= assignDataStore(obj.getData());
			this.rEngine.rniSetAttrBySym(objP, this.dim_SymP,
					this.rEngine.rniPutIntArray(((JRIArrayImpl<?>) obj).getJRIDimArray()));
			return objP;
		case RObject.TYPE_DATAFRAME: {
			final RDataFrame list= (RDataFrame) obj;
			final long length= list.getLength();
			if (length > Integer.MAX_VALUE) {
				throw new UnsupportedOperationException("long list");
			}
			final long[] itemPs= new long[(int) length];
			for (int i= 0; i < length; i++) {
				itemPs[i]= assignDataStore(list.getColumn(i));
			}
			objP= checkAndProtect(this.rEngine.rniPutVector(itemPs));
			names= list.getNames();
			if (names != null) {
				this.rEngine.rniSetAttrBySym(objP, this.names_SymP, assignDataStore(names));
			}
			names= list.getRowNames();
			this.rEngine.rniSetAttrBySym(objP, this.rowNames_SymP, (names != null) ?
					assignDataStore(names) : seqLength(list.getRowCount()) );
			this.rEngine.rniSetAttrBySym(objP, this.class_SymP, this.dataframeClassStringP);
			return objP; }
		case RObject.TYPE_LIST: {
			final RList list= (RList) obj;
			final long length= list.getLength();
			if (length > Integer.MAX_VALUE) {
				throw new UnsupportedOperationException("long list");
			}
			final long[] itemPs= new long[(int) length];
			final int savedProtected= saveProtected();
			for (int i= 0; i < length; i++) {
				itemPs[i]= assignDataObject(list.get(i));
			}
			looseProtected(savedProtected);
			objP= checkAndProtect(this.rEngine.rniPutVector(itemPs));
			names= list.getNames();
			if (names != null) {
				this.rEngine.rniSetAttrBySym(objP, this.names_SymP, assignDataStore(names));
			}
			return objP; }
		case RObject.TYPE_REFERENCE:
			return ((RReference) obj).getHandle();
		case RObject.TYPE_S4OBJECT: {
			final RS4Object s4obj= (RS4Object) obj;
			objP= this.NULL_P;
			for (int i= (int) s4obj.getLength()-1; i >= 0; i--) {
				final RObject slotObj= s4obj.get(i);
				if (slotObj != null && slotObj.getRObjectType() != RObject.TYPE_MISSING) {
					final long slotNameP= installSymbol(s4obj.getName(i));
					objP= checkAndProtect(this.rEngine.rniCons(
							assignDataObject(slotObj), objP,
							slotNameP, false ));
				}
			}
			return protect(evalExpr(this.rEngine.rniCons(
							this.new_SymP, this.rEngine.rniCons(
									this.rEngine.rniPutString(s4obj.getRClassName()), objP,
									this.newClass_SymP, false ),
							0, true ),
					this.rniSafeGlobalExecEnvP, (CODE_DATA_ASSIGN_DATA | 0x8) )); }
		case RObject.TYPE_LANGUAGE: {
			final RLanguage lang= (RLanguage) obj;
			if (lang.getLanguageType() == RLanguage.NAME) {
				return this.rEngine.rniInstallSymbol(lang.getSource());
			}
			objP= this.rEngine.rniParse(lang.getSource(), -1);
			if (objP == 0) {
				throw new RjsException(CODE_DATA_ASSIGN_DATA | 0x9, "The language data is invalid.");
			}
			switch (lang.getLanguageType()) {
			case RLanguage.EXPRESSION:
				return protect(objP);
			case RLanguage.CALL: {
				final long[] list= this.rEngine.rniGetVector(objP);
				if (list != null && list.length == 1
						&& this.rEngine.rniExpType(list[0]) == REXP.LANGSXP ) {
					return protect(list[0]);
				}
				break; }
			case 0: // auto
				final long[] list= this.rEngine.rniGetVector(objP);
				if (list != null && list.length == 1) {
					return protect(list[0]);
				}
				//$FALL-THROUGH$
			default:
				return protect(objP);
			}
			break; }
		default:
			break;
		}
		throw new RjsException((CODE_DATA_ASSIGN_DATA | 0x7),
				"The instantiation of R objects of type " + RDataUtils.getObjectTypeName(obj.getRObjectType()) + " in R is not yet supported." );
	}
	
	public long assignDataStore(final RStore<?> data) throws RNullPointerException {
		switch (data.getStoreType()) {
		case RStore.LOGICAL:
			return checkAndProtect(this.rEngine.rniPutBoolArrayI(
					((JRILogicalDataImpl)data).getJRIValueArray() ));
		case RStore.INTEGER:
			return checkAndProtect(this.rEngine.rniPutIntArray(
					((JRIIntegerDataImpl)data).getJRIValueArray() ));
		case RStore.NUMERIC:
			return checkAndProtect(this.rEngine.rniPutDoubleArray(
					((JRINumericDataImpl)data).getJRIValueArray() ));
		case RStore.COMPLEX: {
			final JRIComplexDataImpl complex= (JRIComplexDataImpl) data;
			final long realP= checkAndProtect(this.rEngine.rniPutDoubleArray(
					complex.getJRIRealValueArray() ));
			final long imaginaryP= checkAndProtect(this.rEngine.rniPutDoubleArray(
					complex.getJRIImaginaryValueArray() ));
			return checkAndProtect(this.rEngine.rniEval(this.rEngine.rniCons(
							this.complexFunP, this.rEngine.rniCons(
									realP, this.rEngine.rniCons(
											imaginaryP, this.NULL_P,
											this.imaginary_SymP, false ),
									this.real_SymP, false ),
							0, true ),
					this.rniSafeBaseExecEnvP )); }
		case RStore.CHARACTER:
			return checkAndProtect(this.rEngine.rniPutStringArray(
					((JRICharacterDataImpl)data).getJRIValueArray() ));
		case RStore.RAW:
			return checkAndProtect(this.rEngine.rniPutRawArray(
					((JRIRawDataImpl)data).getJRIValueArray() ));
		case RStore.FACTOR: {
			final JRIFactorDataImpl factor= (JRIFactorDataImpl) data;
			final long objP= checkAndProtect(this.rEngine.rniPutIntArray(
					factor.getJRIValueArray() ));
			this.rEngine.rniSetAttrBySym(objP, this.levels_SymP,
					this.rEngine.rniPutStringArray(factor.getJRILevelsArray()) );
			this.rEngine.rniSetAttrBySym(objP, this.class_SymP,
					factor.isOrdered() ? this.orderedClassStringP : this.factorClassStringP );
			return objP; }
		default:
			throw new UnsupportedOperationException("Data storage of type " + data.getStoreType() + " are not yet supported.");
		}
	}
	
	public RObject createDataObject(final long objP, final int flags) {
		if (objP == 0) {
			throw new IllegalArgumentException("objP: 0x0");
		}
		if (this.maxDepth > 0) {
			return createDataObject(objP, flags, EVAL_MODE_FORCE);
		}
		else {
			final RObject rObject= createDataObject(objP, (flags | F_ONLY_STRUCT), EVAL_MODE_FORCE);
			return new RReferenceImpl(objP, rObject.getRObjectType(), rObject.getRClassName());
		}
	}
	
	private @Nullable RObject createDataObjectRec(final long objP, final int flags) {
		if (this.currentDepth >= this.maxDepth) {
			return null;
		}
		return createDataObject(objP, flags, EVAL_MODE_DEFAULT);
	}
	
	/**
	 * Returns {@link RObject RJ/R object} for the given R pointer
	 * (R to Java).
	 * 
	 * @param objP a valid pointer to an object in R
	 * @param objTmp an optional R expression pointing to the same object in R
	 * @param flags to configure the data to create
	 * @param force forces the creation of the object (ignoring the depth etc.)
	 * @return new created R object
	 */ 
	private RObject createDataObject(long objP, final int flags, final byte mode) {
		this.currentDepth++;
		try {
			int rType= this.rEngine.rniExpType(objP);
			if (rType == REXP.PROMSXP) {
				final long resultP= this.rEngine.rniGetPromise(objP, 
						((flags & RObjectFactory.F_LOAD_PROMISE) != 0) ? 2 : 1);
				if (resultP == 0) {
					if ((flags & RObjectFactory.F_WITH_DBG) != 0) {
						final long[] detailP= this.rEngine.rniGetPromiseDetail(objP);
						if (detailP != null) {
							return new RPromiseImpl(
									createDataObject(detailP[0], flags, EVAL_MODE_FORCE),
									(detailP[1] != this.NULL_P) ?
											new RReferenceImpl(detailP[1], RObject.TYPE_ENVIRONMENT, RObject.CLASSNAME_ENVIRONMENT) :
											NULL_REF );
						}
					}
					return RPromiseImpl.INSTANCE;
				}
				objP= resultP;
				rType= this.rEngine.rniExpType(objP);
			}
			
			switch (rType) {
			case REXP.NILSXP:
				return RNullImpl.INSTANCE;
			
			case REXP.LGLSXP: { // logical vector / array
				final String className1;
				if (mode != EVAL_MODE_DATASLOT) {
					if (this.rEngine.rniIsS4(objP)) {
						return createS4Obj(objP, REXP.LGLSXP, flags);
					}
					className1= getClass1(objP);
				}
				else {
					className1= null;
				}
				
				final int[] dim= this.rEngine.rniGetArrayDim(objP);
				
				if (dim != null) {
					return ((flags & F_ONLY_STRUCT) != 0) ?
							new JRIArrayImpl<>(
									DefaultRObjectFactory.LOGI_STRUCT_DUMMY,
									className1, dim ) :
							new JRIArrayImpl<>(
									new JRILogicalDataImpl(this.rEngine.rniGetBoolArrayI(objP)),
									className1, dim, getDimNames(objP, dim.length) );
				}
				else {
					return ((flags & F_ONLY_STRUCT) != 0) ?
							new JRIVectorImpl<>(
									DefaultRObjectFactory.LOGI_STRUCT_DUMMY,
									this.rEngine.rniGetVectorLength(objP), className1, null ) :
							new JRIVectorImpl<>(
									new JRILogicalDataImpl(this.rEngine.rniGetBoolArrayI(objP)),
									className1, getNames(objP) );
				}
			}
			case REXP.INTSXP: { // integer vector / array
				final String className1;
				if (mode != EVAL_MODE_DATASLOT) {
					if (this.rEngine.rniIsS4(objP)) {
						return createS4Obj(objP, REXP.INTSXP, flags);
					}
					className1= getClass1(objP);
				}
				else {
					className1= null;
				}
				
				if (className1 != null
						&& (className1.equals("factor")
								|| this.rEngine.rniInheritsFactor(objP)) ) {
					final @Nullable String[] levels;
					{	final long levelsP= this.rEngine.rniGetAttrBySym(objP, this.levels_SymP);
						levels= (levelsP != 0) ? this.rEngine.rniGetStringArray(levelsP) : null;
					}
					if (levels != null) {
						final boolean isOrdered= className1.equals("ordered") || this.rEngine.rniInheritsOrdered(objP);
						final RFactorStore factorData= ((flags & F_ONLY_STRUCT) != 0) ?
								new RFactorStructStore(isOrdered, levels.length) :
								new RFactor32Store(this.rEngine.rniGetIntArray(objP), isOrdered, levels);
						return ((flags & F_ONLY_STRUCT) != 0) ?
								new JRIVectorImpl<>(
										factorData,
										this.rEngine.rniGetVectorLength(objP), className1, null ) :
								new JRIVectorImpl<>(
										factorData,
										className1, getNames(objP) );
					}
				}
				
				final int[] dim= this.rEngine.rniGetArrayDim(objP);
				
				if (dim != null) {
					return ((flags & F_ONLY_STRUCT) != 0) ?
							new JRIArrayImpl<>(
									DefaultRObjectFactory.INT_STRUCT_DUMMY,
									className1, dim ) :
							new JRIArrayImpl<>(
									new JRIIntegerDataImpl(this.rEngine.rniGetIntArray(objP)),
									className1, dim, getDimNames(objP, dim.length) );
				}
				else {
					return ((flags & F_ONLY_STRUCT) != 0) ?
							new JRIVectorImpl<>(
									DefaultRObjectFactory.INT_STRUCT_DUMMY,
									this.rEngine.rniGetVectorLength(objP), className1, null ) :
							new JRIVectorImpl<>(
									new JRIIntegerDataImpl(this.rEngine.rniGetIntArray(objP)),
									className1, getNames(objP) );
				}
			}
			case REXP.REALSXP: { // numeric vector / array
				final String className1;
				if (mode != EVAL_MODE_DATASLOT) {
					if (this.rEngine.rniIsS4(objP)) {
						return createS4Obj(objP, REXP.REALSXP, flags);
					}
					className1= getClass1(objP);
				}
				else {
					className1= null;
				}
				
				final int[] dim= this.rEngine.rniGetArrayDim(objP);
				
				if (dim != null) {
					return ((flags & F_ONLY_STRUCT) != 0) ?
							new JRIArrayImpl<>(
									DefaultRObjectFactory.NUM_STRUCT_DUMMY,
									className1, dim ) :
							new JRIArrayImpl<>(
									new JRINumericDataImpl(this.rEngine.rniGetDoubleArray(objP)),
									className1, dim, getDimNames(objP, dim.length) );
				}
				else {
					return ((flags & F_ONLY_STRUCT) != 0) ?
							new JRIVectorImpl<>(
									DefaultRObjectFactory.NUM_STRUCT_DUMMY,
									this.rEngine.rniGetVectorLength(objP), className1, null ) :
							new JRIVectorImpl<>(
									new JRINumericDataImpl(this.rEngine.rniGetDoubleArray(objP)),
									className1, getNames(objP) );
				}
			}
			case REXP.CPLXSXP: { // complex vector / array
				final String className1;
				if (mode != EVAL_MODE_DATASLOT) {
					if (this.rEngine.rniIsS4(objP)) {
						return createS4Obj(objP, REXP.CPLXSXP, flags);
					}
					className1= getClass1(objP);
				}
				else {
					className1= null;
				}
				
				final int[] dim= this.rEngine.rniGetArrayDim(objP);
				
				if (dim != null) {
					return ((flags & F_ONLY_STRUCT) != 0) ?
						new JRIArrayImpl<>(
								DefaultRObjectFactory.CPLX_STRUCT_DUMMY,
								className1, dim ) :
						new JRIArrayImpl<>(
								new JRIComplexDataImpl(getComplexRe(objP), getComplexIm(objP)),
								className1, dim, getDimNames(objP, dim.length) );
				}
				else {
					return ((flags & F_ONLY_STRUCT) != 0) ?
							new JRIVectorImpl<>(
									DefaultRObjectFactory.CPLX_STRUCT_DUMMY,
									this.rEngine.rniGetVectorLength(objP), className1, null ) :
							new JRIVectorImpl<>(
									new JRIComplexDataImpl(getComplexRe(objP), getComplexIm(objP)),
									className1, getNames(objP) );
				}
			}
			case REXP.STRSXP: { // character vector / array
				final String className1;
				if (mode != EVAL_MODE_DATASLOT) {
					if (this.rEngine.rniIsS4(objP)) {
						return createS4Obj(objP, REXP.STRSXP, flags);
					}
					className1= getClass1(objP);
				}
				else {
					className1= null;
				}
				
				final int[] dim= this.rEngine.rniGetArrayDim(objP);
				
				if (dim != null) {
					return ((flags & F_ONLY_STRUCT) != 0) ?
							new JRIArrayImpl<>(
									DefaultRObjectFactory.CHR_STRUCT_DUMMY,
									className1, dim ) :
							new JRIArrayImpl<>(
									new JRICharacterDataImpl(this.rEngine.rniGetStringArray(objP)),
									className1, dim, getDimNames(objP, dim.length) );
				}
				else {
					return ((flags & F_ONLY_STRUCT) != 0) ?
							new JRIVectorImpl<>(
									DefaultRObjectFactory.CHR_STRUCT_DUMMY,
									this.rEngine.rniGetVectorLength(objP), className1, null ) :
							new JRIVectorImpl<>(
									new JRICharacterDataImpl(this.rEngine.rniGetStringArray(objP)),
									className1, getNames(objP) );
				}
			}
			case REXP.RAWSXP: { // raw/byte vector
				final String className1;
				if (mode != EVAL_MODE_DATASLOT) {
					if (this.rEngine.rniIsS4(objP)) {
						return createS4Obj(objP, REXP.RAWSXP, flags);
					}
					className1= getClass1(objP);
				}
				else {
					className1= null;
				}
				
				final int[] dim= this.rEngine.rniGetArrayDim(objP);
				
				if (dim != null) {
					return ((flags & F_ONLY_STRUCT) != 0) ?
							new JRIArrayImpl<>(
									DefaultRObjectFactory.RAW_STRUCT_DUMMY,
									className1, dim ) :
							new JRIArrayImpl<>(
									new JRIRawDataImpl(this.rEngine.rniGetRawArray(objP)),
									className1, dim, getDimNames(objP, dim.length) );
				}
				else {
					return ((flags & F_ONLY_STRUCT) != 0) ?
							new JRIVectorImpl<>(
									DefaultRObjectFactory.RAW_STRUCT_DUMMY,
									this.rEngine.rniGetVectorLength(objP), className1, null ) :
							new JRIVectorImpl<>(
									new JRIRawDataImpl(this.rEngine.rniGetRawArray(objP)),
									className1, getNames(objP) );
				}
			}
			case REXP.VECSXP: { // generic vector / list
				final String className1;
				if (mode != EVAL_MODE_DATASLOT) {
					className1= getClass1(objP);
				}
				else {
					className1= null;
				}
				
				final long length= this.rEngine.rniGetVectorLength(objP);
				if (length > Integer.MAX_VALUE) {
					if ((flags & F_ONLY_STRUCT) != 0) {
						return new JRIListLongImpl(length, className1);
					}
					throw new UnsupportedOperationException("long list");
				}
				
				final long[] itemP= this.rEngine.rniGetVector(objP);
				DATA_FRAME: if (className1 != null &&
						(className1.equals("data.frame") || this.rEngine.rniInheritsDataFrame(objP)) ) {
					final RObject[] itemObjects= new RObject[itemP.length];
					long rowCount= -1;
					for (int i= 0; i < itemP.length; i++) {
						if (this.rniInterrupted) {
							throw new CancellationException();
						}
						itemObjects[i]= createDataObject(itemP[i], flags, EVAL_MODE_FORCE);
						if (itemObjects[i] == null || itemObjects[i].getRObjectType() != RObject.TYPE_VECTOR) {
							break DATA_FRAME;
						}
						else if (rowCount == -1) {
							rowCount= itemObjects[i].getLength();
						}
						else if (rowCount != itemObjects[i].getLength()){
							break DATA_FRAME;
						}
					}
					final String[] rowNames= ((flags & F_ONLY_STRUCT) != 0) ? null : getRowNames(objP);
					if (rowNames != null && rowCount != -1 && rowNames.length != rowCount) {
						break DATA_FRAME;
					}
					return new RDataFrame32Impl(itemObjects, className1, getNames(objP), rowNames);
				}
				if (((flags & F_ONLY_STRUCT) != 0 && length > this.maxListsLength)
						|| this.currentDepth >= this.maxDepth ) {
					return new JRIListLongImpl(length, className1);
				}
				{	final int savedProtected= saveProtected();
					final @Nullable RObject[] itemObjects= new @Nullable RObject[itemP.length];
					for (int i= 0; i < itemP.length; i++) {
						if (this.rniInterrupted) {
							throw new CancellationException();
						}
						itemObjects[i]= createDataObjectRec(itemP[i], flags);
						looseProtectedOpt(savedProtected);
						continue;
					}
					return new JRIListImpl(itemObjects, className1, getNames(objP));
				}
			}
			case REXP.LISTSXP:   // pairlist
			/*case REXP.LANGSXP: */{
				String className1;
				if (mode == EVAL_MODE_DATASLOT
						|| (className1= getClass1(objP)) == null ) {
					className1= RObject.CLASSNAME_PAIRLIST;
				}
				
				long cdr= objP;
				final int length= this.rEngine.rniGetLength(objP);
				final @Nullable String[] itemNames= new @Nullable String[length];
				final @Nullable RObject[] itemObjects= new @Nullable RObject[length];
				for (int i= 0; i < length && cdr != 0; i++) {
					if (this.rniInterrupted) {
						throw new CancellationException();
					}
					final long car= this.rEngine.rniCAR(cdr);
					final long tag= this.rEngine.rniTAG(cdr);
					itemNames[i]= (tag != 0) ? this.rEngine.rniGetSymbolName(tag) : null;
					itemObjects[i]= createDataObjectRec(car, flags);
					cdr= this.rEngine.rniCDR(cdr);
				}
				return new JRIListImpl(itemObjects, className1, itemNames);
			}
			case REXP.ENVSXP: {
				if (this.currentDepth > 1 && (flags & RObjectFactory.F_LOAD_ENVIRONMENT) == 0) {
					return new RReferenceImpl(objP, RObject.TYPE_ENVIRONMENT, RObject.CLASSNAME_ENVIRONMENT);
				}
				final long length= this.rEngine.rniGetLengthLong(objP);
				if (objP == this.Autoload_EnvP || objP == this.rjTmpEnvP
						|| length > this.maxEnvsLength) {
					return createEnvObject(objP, null, null, length,
							(mode != EVAL_MODE_DATASLOT) );
				}
				final long namesStrP= protect(this.rEngine.rniListEnv(objP, true));
				final @Nullable String[] names= this.rEngine.rniGetStringArray(namesStrP);
				if (names != null) {
					final int savedProtected= saveProtected();
					final @Nullable RObject[] itemObjects= new @Nullable RObject[names.length];
					for (int i= 0; i < names.length; i++) {
						if (this.rniInterrupted) {
							throw new CancellationException();
						}
						final long nameP= this.rEngine.rniInstallSymbolByStr(namesStrP, i);
						final long itemP= this.rEngine.rniGetVarBySym(nameP, objP, 0);
						if (itemP == 0) {
							itemObjects[i]= RMissingImpl.INSTANCE;
							continue;
						}
						protect(itemP);
						itemObjects[i]= createDataObjectRec(itemP, flags);
						looseProtectedOpt(savedProtected);
						continue;
					}
					return createEnvObject(objP, itemObjects, names, names.length,
							(mode != EVAL_MODE_DATASLOT) );
				}
				break;
			}
			case REXP.CLOSXP: {
				if (mode != EVAL_MODE_DATASLOT && this.rEngine.rniIsS4(objP)) {
					return createS4Obj(objP, REXP.CLOSXP, flags);
				}
				
				final String header= getFHeader(objP);
				return new RFunctionImpl(header);
			}
			case REXP.SPECIALSXP:
			case REXP.BUILTINSXP: {
				final String header= getFHeader(objP);
				return new RFunctionImpl(header);
			}
			case REXP.S4SXP: {
				if (mode != EVAL_MODE_DATASLOT) {
					return createS4Obj(objP, REXP.S4SXP, flags);
				}
				break; // invalid
			}
			case REXP.SYMSXP: {
				if (objP == this.MissingArg_P) {
					return RMissingImpl.INSTANCE;
				}
				return ((flags & F_ONLY_STRUCT) != 0) ? 
						new JRILanguageImpl(RLanguage.NAME, null) :
						new JRILanguageImpl(RLanguage.NAME, this.rEngine.rniGetSymbolName(objP), null);
			}
			case REXP.LANGSXP: {
				final String className1;
				if (mode != EVAL_MODE_DATASLOT) {
					className1= getClass1(objP);
				}
				else {
					className1= null;
				}
				return ((flags & F_ONLY_STRUCT) != 0) ? 
						new JRILanguageImpl(RLanguage.CALL, className1) :
						new JRILanguageImpl(RLanguage.CALL, getCallSourceString(objP), className1);
			}
			case REXP.EXPRSXP: {
				String className1;
				if (mode == EVAL_MODE_DATASLOT
						|| (className1= getClass1(objP)) == null ) {
					className1= null;
				}
				return ((flags & F_ONLY_STRUCT) != 0) ? 
						new JRILanguageImpl(RLanguage.EXPRESSION, className1) :
						new JRILanguageImpl(RLanguage.EXPRESSION, getExpressionSourceString(objP),
								className1 );
			}
			case REXP.EXTPTRSXP: {
				String className1;
				if (mode == EVAL_MODE_DATASLOT
						|| (className1= getClass1(objP)) == null ) {
					className1= "externalptr";
				}
				return new ROtherImpl(className1);
			}
			}
//				final long classP= this.rEngine.rniEval(this.rEngine.rniCons(this.classFunP,
//						this.rEngine.rniCons(objP, this._NULL_P, , false), 0, true),
//						this._Base_EnvP);
			{	// Other type and fallback
				final String className1= getClass1Safe(objP);
				return new ROtherImpl(className1);
			}
		}
		finally {
			this.currentDepth--;
		}
	}
	
	private RObject createS4Obj(final long objP, final int rType, final int flags) {
		final long classP= this.rEngine.rniGetAttrBySym(objP, this.class_SymP);
		String className= null;
		if (classP != 0 && classP != this.NULL_P) {
			className= this.rEngine.rniGetString(classP);
			final long slotNamesP= this.rEngine.rniEval(this.rEngine.rniCons(
							this.slotNamesFunP, this.rEngine.rniCons(
									classP, this.NULL_P,
									this.x_SymP, false ),
							0, true ),
					this.rniSafeGlobalExecEnvP );
			if (slotNamesP != 0) {
				protect(slotNamesP);
				final String[] slotNames= this.rEngine.rniGetStringArray(slotNamesP);
				if (slotNames != null && slotNames.length > 0) {
					if (isAnyNull(slotNames)) {
						if (className == null) {
							className= getClass1Safe(objP);
						}
						return new ROtherImpl(className);
					}
					final int savedProtected= saveProtected();
					final @NonNull RObject[] slotValues= new @NonNull RObject[slotNames.length];
					for (int i= 0; i < slotNames.length; i++) {
						if (this.rniInterrupted) {
							throw new CancellationException();
						}
						if (".Data".equals(slotNames[i])) {
							slotValues[i]= createDataObject(objP, flags, EVAL_MODE_DATASLOT);
							if (className == null && slotValues[i] != null) {
								className= slotValues[i].getRClassName();
							}
							looseProtectedOpt(savedProtected);
							continue;
						}
						else {
							final long slotNameP= this.rEngine.rniInstallSymbolByStr(slotNamesP, i);
							final long slotValueP;
							if ((slotValueP= this.rEngine.rniGetAttrBySym(objP, slotNameP)) == 0) {
								slotValues[i]= RMissingImpl.INSTANCE;
								continue;
							}
							if (slotValueP == this.slotNULL_SymP) {
								slotValues[i]= RNullImpl.INSTANCE;
								continue;
							}
							slotValues[i]= createDataObject(slotValueP, flags, EVAL_MODE_FORCE);
							looseProtectedOpt(savedProtected);
							continue;
						}
					}
					if (className == null) {
						className= getClass1Safe(objP);
					}
					return new RS4ObjectImpl(className, slotNames, slotValues);
				}
			}
		}
		if (rType != REXP.S4SXP) {
			final RObject dataSlot= createDataObject(objP, flags, EVAL_MODE_DATASLOT);
			if (dataSlot != null) {
				if (className == null) {
					className= dataSlot.getRClassName();
					if (className == null) {
						className= getClass1Safe(objP);
					}
				}
				return new RS4ObjectImpl(className, DATA_NAME_ARRAY, new @NonNull RObject[] { dataSlot });
			}
			if (className == null) {
				className= getClass1Safe(objP);
			}
		}
		else if (className == null) {
			className= "S4";
		}
		return new RS4ObjectImpl(className, EMPTY_STRING_ARRAY, EMPTY_ROBJECT_ARRAY);
	}
	
	private @Nullable String getClass1(final long objP) {
		final byte[] className1Utf8= this.rEngine.rniGetAttrStringUtf8BySym(objP, this.class_SymP);
		return (className1Utf8 != null) ? new String(className1Utf8, StandardCharsets.UTF_8) : null;
	}
	
	private String getClass1Safe(final long objP) {
		if (objP != 0) {
			final byte[] className1Utf8;
			final long classP;
			this.rniTempEvalAssigned= true;
			if (this.rEngine.rniAssignVarBySym(this.x_SymP, objP, this.rniTempEnvP)
					&& (classP= this.rEngine.rniEval(this.rniTempEvalClassExprP, this.rniTempEnvP)) != 0
					&& (className1Utf8= this.rEngine.rniGetStringUtf8(classP)) != null ) {
				return new String(className1Utf8, StandardCharsets.UTF_8);
			}
		}
		return "<unknown>";
	}
	
	private @Nullable String @Nullable [] getNames(final long objP) {
		final long namesP= this.rEngine.rniGetAttrBySym(objP, this.names_SymP);
		return (namesP != 0) ? this.rEngine.rniGetStringArray(namesP) : null;
	}
	
	private @Nullable SimpleRList<@Nullable RCharacterStore> getDimNames(final long objP, final int length) {
		final long namesP= this.rEngine.rniGetAttrBySym(objP, this.dimnames_SymP);
		final long[] names1P;
		if (this.rEngine.rniExpType(namesP) == REXP.VECSXP
				&& (names1P= this.rEngine.rniGetVector(namesP)) != null
				&& names1P.length == length) {
			@Nullable String[] s= getNames(namesP);
			final RCharacter32Store names0= (s != null) ? new RCharacter32Store(s) :
				new RCharacter32Store(names1P.length);
			final @Nullable RCharacterStore[] names1= new @Nullable RCharacterStore[names1P.length];
			for (int i= 0; i < names1P.length; i++) {
				s= this.rEngine.rniGetStringArray(names1P[i]);
				if (s != null) {
					names1[i]= new RCharacter32Store(s);
				}
			}
			return new SimpleRList<>(names1, names0);
		}
		return null;
	}
	
	private @Nullable String @Nullable [] getRowNames(final long objP) {
		final long namesP= this.rEngine.rniGetAttrBySym(objP, this.rowNames_SymP);
		return (namesP != 0) ? this.rEngine.rniGetStringArray(namesP) : null;
	}
	
	private double[] getComplexRe(final long objP) {
		final long numP= this.rEngine.rniEval(this.rEngine.rniCons(
						this.ReFunP, this.rEngine.rniCons(
								objP, this.NULL_P,
								this.z_SymP, false ),
						0, true ),
				this.rniSafeBaseExecEnvP );
		final double[] num;
		if (numP != 0
				&& ((num= this.rEngine.rniGetDoubleArray(numP)) != null) ) {
			return num;
		}
		if (this.rniInterrupted) {
			throw new CancellationException();
		}
		throw new RuntimeException("Failed to load values of real part of complex.");
	}
	
	private double[] getComplexIm(final long objP) {
		final long numP= this.rEngine.rniEval(this.rEngine.rniCons(
						this.ImFunP, this.rEngine.rniCons(
								objP, this.NULL_P,
								this.z_SymP, false ),
						0, true ),
				this.rniSafeBaseExecEnvP );
		final double[] num;
		if (numP != 0
				&& ((num= this.rEngine.rniGetDoubleArray(numP)) != null) ) {
			return num;
		}
		if (this.rniInterrupted) {
			throw new CancellationException();
		}
		throw new RuntimeException("Failed to load values of imaginary part of complex.");
	}
	
	private @Nullable String getFHeader(final long cloP) {
		final long argsP= this.rEngine.rniEval(this.rEngine.rniCons(
						this.getFHeaderFunP, this.rEngine.rniCons(
								cloP, this.NULL_P,
								this.x_SymP, false ),
						0, true ),
				this.rniSafeGlobalExecEnvP );
		final String args;
		if (argsP != 0
				&& (args= this.rEngine.rniGetString(argsP)) != null
				&& args.length() >= 11 ) { // "function ()".length
//			return args.substring(9,);
			return args;
		}
		return null;
	}
	
	
	public long createNewEnv(final long parentP) {
		if (parentP == 0) {
			throw new IllegalArgumentException("Missing parent for new environment.");
		}
		return this.rEngine.rniEval(this.rEngine.rniCons(
						this.newEnvFunP, this.rEngine.rniCons(
								parentP, this.NULL_P,
								this.parent_SymP, false ),
						0, true ),
				this.rniSafeBaseExecEnvP );
	}
	
	private long getNamespaceEnvP(final String nsName) throws RjsException {
		if (nsName == null) {
			throw new IllegalArgumentException("Missing name for namespace.");
		}
		final long p= this.rEngine.rniEval(this.rEngine.rniCons(
						this.getNamespaceFunP, this.rEngine.rniCons(
								installString(nsName), this.NULL_P,
								this.name_SymP, false ),
						0, true ),
				this.rniSafeBaseExecEnvP );
		if (p != 0) {
			return p;
		}
		throw new RjsException(0, "Namespace '" + nsName + "' not available.");
	}
	
	public RObject getNamespaceEnv(final String nsName, final int flags) throws RjsException {
		final long envP= getNamespaceEnvP(nsName);
		if (this.maxDepth == 0) {
			return new RReferenceImpl(envP, RObject.TYPE_ENVIRONMENT, RObject.CLASSNAME_ENVIRONMENT);
		}
		
		return createDataObject(envP, flags, EVAL_MODE_FORCE);
	}
	
	public RObject getNamespaceExportsEnv(final String nsName, final int flags) throws RjsException {
		final long envP= getNamespaceEnvP(nsName);
		if (this.maxDepth == 0) {
			return new RReferenceImpl(0, RObject.TYPE_ENVIRONMENT, RObject.CLASSNAME_ENVIRONMENT);
		}
		
		this.currentDepth++;
		try {
			final long namesStrP= this.rEngine.rniEval(this.rEngine.rniCons(
							this.getNamespaceExportedNamesFunP, this.rEngine.rniCons(
									envP, this.NULL_P,
									this.ns_SymP, false ),
							0, true ),
					this.rniSafeBaseExecEnvP );
			if (namesStrP != 0) {
				protect(namesStrP);
				final @Nullable String[] names= this.rEngine.rniGetStringArray(namesStrP);
				if (names != null) {
					final int savedProtected= saveProtected();
					final @Nullable RObject[] itemObjects= new @Nullable RObject[names.length];
					for (int i= 0; i < names.length; i++) {
						if (this.rniInterrupted) {
							throw new CancellationException();
						}
						final long itemP= this.rEngine.rniEval(this.rEngine.rniCons(
										this.getNamespaceExportedValueFunP, this.rEngine.rniCons(
												envP, this.rEngine.rniCons(
														this.rEngine.rniPutStringByStr(namesStrP, i), this.NULL_P,
														this.name_SymP, false ),
												this.ns_SymP, false ),
										0, true ),
								this.rniSafeBaseExecEnvP );
						if (itemP == 0) {
							itemObjects[i]= RMissingImpl.INSTANCE;
							continue;
						}
						protect(itemP);
						itemObjects[i]= createDataObjectRec(itemP, flags);
						looseProtectedOpt(savedProtected);
						continue;
					}
					return new JRIEnvironmentImpl(nsName, 0, names.length, itemObjects, names,
							REnvironment.ENVTYPE_NAMESPACE_EXPORTS, null );
				}
			}
			return new JRIEnvironmentImpl(nsName, 0, 0, EMPTY_ROBJECT_ARRAY, EMPTY_STRING_ARRAY,
					REnvironment.ENVTYPE_NAMESPACE_EXPORTS, null );
		}
		finally {
			this.currentDepth--;
		}
	}
	
	public boolean isInternEnv(final long envP) {
		return (envP == this.rjTmpEnvP
				|| envP == this.rniSafeBaseExecEnvP || envP == this.rniSafeGlobalExecEnvP );
	}
	
	
	private long installCall(final long funP, final Arg[] funArgs) {
		return JRISrvRni.this.rEngine.rniCons(funP, installArgs(funArgs), 0, true);
	}
	
	private long installArgs(final Arg[] funArgs) {
		long p= JRISrvRni.this.NULL_P;
		for (int i= funArgs.length - 1; i >= 0; i--) {
			final Arg arg= funArgs[i];
			p= JRISrvRni.this.rEngine.rniCons(arg.valueP, p, arg.symP, false);
		}
		return p;
	}
	
	public long seqLength(final double length) {
		return this.rEngine.rniEval(this.rEngine.rniCons(
						this.seqIntFunP, this.rEngine.rniCons(
								this.rEngine.rniPutDoubleArray(new double[] { length }), this.NULL_P,
								this.lengthOut_SymP, false ),
						0, true ),
				this.rniSafeBaseExecEnvP );
	}
	
	
	public String @Nullable [] getSourceLines(final long objP) {
		this.rniTempEvalAssigned= true;
		if (this.rEngine.rniAssignVarBySym(this.x_SymP, objP, this.rniTempEnvP)) {
			final long linesP= this.rEngine.rniEval(this.deparseLinesXCallP, this.rniTempEnvP);
			if (linesP != 0) {
				final @Nullable String[] lines= this.rEngine.rniGetStringArray(linesP);
				if (lines != null && !isAnyNull(lines)) {
					return lines;
				}
			}
		}
		return null;
	}
	
	
	public @Nullable String getCallSourceString(final long objP) {
		final String[] lines= getSourceLines(objP);
		if (lines != null) {
			switch (lines.length) {
			case 0:
				return "";
			case 1:
				return lines[0];
			default:
				this.sBuilder.setLength(0);
				this.sBuilder.append(lines[0]);
				for (int i= 1; i < lines.length; i++) {
					this.sBuilder.append('\n');
					this.sBuilder.append(lines[i]);
				}
				return this.sBuilder.toString();
			}
		}
		return null;
	}
	
	public @Nullable String getExpressionSourceString(final long objP) {
		final String[] lines= getSourceLines(objP);
		if (lines != null && lines.length > 0
					&& lines[0].startsWith("expression(") ) {
			this.sBuilder.setLength(0);
			this.sBuilder.append(lines[0]);
			for (int i= 1; i < lines.length; i++) {
				this.sBuilder.append('\n');
				this.sBuilder.append(lines[i]);
			}
			return this.sBuilder.substring(11, this.sBuilder.length() - 1);
		}
		return null;
	}
	
	public boolean setOption(final long symP, final long valP) {
		final long p= this.rEngine.rniEval(this.rEngine.rniCons(
						this.optionsSymP, this.rEngine.rniCons(
								valP, this.NULL_P,
								symP, false),
						0, true),
				this.rniSafeGlobalExecEnvP );
		return (p != 0);
	}
	
	
	public JRIEnvironmentImpl createEnvObject(final long objP,
			final @Nullable RObject @Nullable [] itemObjects, final String @Nullable [] names,
			final long length,
			final boolean loadClassName) {
		final byte type;
		String envName;
		if (objP == this.Base_EnvP) {
			type= REnvironment.ENVTYPE_BASE;
			envName= REnvironment.ENVNAME_BASE;
		}
		else if (objP == this.Global_EnvP) {
			type= REnvironment.ENVTYPE_GLOBAL;
			envName= REnvironment.ENVNAME_GLOBAL;
		}
		else if (objP == this.Empty_EnvP) {
			type= REnvironment.ENVTYPE_EMTPY;
			envName= REnvironment.ENVNAME_EMPTY;
		}
		else if (objP == this.BaseNamespace_EnvP) {
			type= REnvironment.ENVTYPE_NAMESPACE;
			envName= REnvironment.ENVNAME_BASE;
		}
		else if (objP == this.Autoload_EnvP) {
			type= REnvironment.ENVTYPE_AUTOLOADS;
			envName= REnvironment.ENVNAME_AUTOLOADS;
		}
		else {
			final byte[] envNameUtf8= this.rEngine.rniGetNamespaceEnvNameUtf8(objP);
			if (envNameUtf8 != null) {
				envName= new String(envNameUtf8, StandardCharsets.UTF_8);
				type= REnvironment.ENVTYPE_NAMESPACE;
			}
			else {
				envName= this.rEngine.rniGetAttrStringBySym(objP, this.name_SymP);
				if (envName == null) {
					envName= ""; // ?
				}
				if (envName != null && envName.startsWith("package:")) {
					type= REnvironment.ENVTYPE_PACKAGE;
				}
				else {
					type= 0;
				}
			}
		}
		
		return new JRIEnvironmentImpl(envName, objP, length, itemObjects, names,
				type, (loadClassName) ? getClass1(objP) : null );
	}
	
}
