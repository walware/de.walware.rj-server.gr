/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Apache License, Version 2.0 which is available at
 # https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package de.walware.rj.internal.server.gr.jri;

import static de.walware.rj.internal.server.gr.jri.JRISrvErrors.CODE_DBG_TRACE;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.regex.Pattern;

import org.rosuda.JRI.REXP;
import org.rosuda.JRI.Rengine;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.UnexpectedRDataException;
import org.eclipse.statet.rj.server.dbg.ElementTracepoints;
import org.eclipse.statet.rj.server.dbg.Srcref;
import org.eclipse.statet.rj.server.dbg.Tracepoint;
import org.eclipse.statet.rj.server.dbg.TracepointPosition;
import org.eclipse.statet.rj.server.dbg.TracepointState;

import de.walware.rj.internal.server.gr.jri.JRISrvRni.RNullPointerException;


final class TracepointManagerOperations {
	
	
	public static final class FunInfo {
		
		public static final int INVALID= -1;
		
		public static final int FILE_PATH= 1;
		public static final int FILE_NAME= 2;
		
		public static final int DONE_CLEAR= 1;
		public static final int DONE_SET= 2;
		
		public final long orgFunP;
		public final int[] subIndex;
		public long currentMainFunP;
		public final long orgMainFunP;
		public final String rawMethodSignature;
		
		// preCheck
		public long orgBodyP;
		public long srcfileP;
		public int fileType;
		public String file;
		public int[] startSrcref;
		public int[] wholeSrcref;
		
		public long currentMainBodyP;
		
		public final int[] done;
		
		
		public FunInfo(final long orgFunP, final long currentFunP, final String signature) {
			this.orgFunP= orgFunP;
			this.subIndex= null;
			this.orgMainFunP= orgFunP;
			this.currentMainFunP= currentFunP;
			this.rawMethodSignature= signature;
			this.done= new int[1];
		}
		
		public FunInfo(final long orgFunP, final int[] subIndex, final FunInfo main) {
			this.orgFunP= orgFunP;
			this.subIndex= subIndex;
			this.orgMainFunP= main.orgMainFunP;
			this.currentMainFunP= main.currentMainFunP;
			this.rawMethodSignature= main.rawMethodSignature;
			this.done= main.done;
		}
		
		
		public @NonNull String getLabel(final @NonNull ObjectInEnv ref) {
			if (this.rawMethodSignature != null) {
				return "method `" + ref.getName() + "`(" + this.rawMethodSignature + ") in " + ref.getEnv();
			}
			else {
				return "function `" + ref.getName() + "` in " + ref.getEnv();
			}
		}
		
		public int @Nullable [] getBodySrcref() {
			if (this.startSrcref != null) {
				if (this.wholeSrcref != null) {
					return Srcref.merge(this.startSrcref, this.wholeSrcref);
				}
				return this.startSrcref;
			}
			return null;
		}
		
	}
	
	
	private static final int[] NA_SRCREF= new int[] {
			Integer.MIN_VALUE, Integer.MIN_VALUE,
			Integer.MIN_VALUE, Integer.MIN_VALUE,
			Integer.MIN_VALUE, Integer.MIN_VALUE,
	};
	
	private static final Pattern SIGNATURE_PATTERN= Pattern.compile("\\#"); //$NON-NLS-1$
	
	
	private final JRISrvDbg dbg;
	private final Rengine rEngine;
	private final JRISrvRni rni;
	
	private final long traceSymP;
	private final long untraceSymP;
	
	private final long editFArgsP;
	private final long stepNextValueP;
	private final long stepSrcrefTmplP;
	
	private final long breakpointTmplP;
	private final long errorHandlerP;
	
	private final long browserExprP;
	
	
	public TracepointManagerOperations(final JRISrvDbg dbg, final JRISrvRni rni)
			throws RNullPointerException {
		this.dbg= dbg;
		this.rEngine= rni.getREngine();
		this.rni= rni;
		
		this.traceSymP= this.rEngine.rniInstallSymbol("trace"); //$NON-NLS-1$
		this.untraceSymP= this.rEngine.rniInstallSymbol("untrace"); //$NON-NLS-1$
		
		this.editFArgsP= this.rni.checkAndPreserve(this.rEngine.rniCons(
				this.rni.MissingArg_P, this.rEngine.rniCons(
						this.rni.MissingArg_P, this.rni.NULL_P,
						this.rni.Ellipsis_SymP, false ),
				rni.fdef_SymP, false ));
		this.stepNextValueP= this.rni.checkAndPreserve(this.rEngine.rniPutString("browser:n")); //$NON-NLS-1$
		this.stepSrcrefTmplP= this.rni.checkAndPreserve(this.rEngine.rniPutIntArray(NA_SRCREF));
		this.rEngine.rniSetAttrBySym(this.stepSrcrefTmplP, this.rni.what_SymP, this.stepNextValueP);
		
		this.breakpointTmplP= this.rni.checkAndPreserve(this.rEngine.rniGetVectorElt(
				this.rEngine.rniParse(
						"{ if (\"rj\" %in% loadedNamespaces()) rj:::dbg.checkBreakpoint() }", 1 ), //$NON-NLS-1$
				0 ));
		this.errorHandlerP= this.rni.checkAndPreserve(this.rEngine.rniParse(
				"rj:::dbg.checkEB()", 1 ));
		
		this.browserExprP= this.rni.checkAndPreserve(this.rEngine.rniGetVectorElt(this.rEngine.rniParse(
				"{ browser(skipCalls= 3L) }", 1 ), //$NON-NLS-1$
				0 ));
	}
	
	
//	public int trySetTracepoints(final ElementTracepoints elementTracepoints,
//			final FunInfo funInfo, final ObjectInEnv funRef) {
//		// file
//		final SrcfileData srcfile= elementTracepoints.getSrcfile();
//		boolean ok= false;
//		if (srcfile.getPath() == null) {
//			return TracepointInstallationReport.NOTFOUND;
//		}
//		if (funInfo.fileType == FunInfo.FILE_PATH) {
//			if (funInfo.file.equals(srcfile.getPath())) {
//				ok= true;
//			}
//			else {
//				return TracepointInstallationReport.NOTFOUND;
//			}
//		}
//		if (!ok && funInfo.fileType == FunInfo.FILE_NAME) {
//			if (funInfo.file.equals(srcfile.getName())) {
//				ok= true;
//			}
//			else {
//				return TracepointInstallationReport.NOTFOUND;
//			}
//		}
//		if (!ok) {
//			return TracepointInstallationReport.NOTFOUND;
//		}
//		
//		// element
//		ok= false;
//		if (elementTracepoints.getElementId() != null) {
//			final long p= this.rEngine.rniGetAttrBySym(funInfo.orgBodyP, this.rni.appElementId_SymP);
//			if (p != 0) {
//				if (elementTracepoints.getElementId().equals(this.rEngine.rniGetString(p))) {
//					ok= true;
//				}
//				else {
//					return TracepointInstallationReport.NOTFOUND;
//				}
//			}
//		}
//		if (!ok && funInfo.baseSrcref != null && elementTracepoints.getElementSrcref() != null) {
//			long funTimestamp;
//			if (srcfile.getTimestamp() != 0
//					&& funInfo.baseSrcref[0] == elementTracepoints.getElementSrcref()[0]
//					&& funInfo.baseSrcref[4] == elementTracepoints.getElementSrcref()[4]
//					&& ((funTimestamp= this.dbg.getFileTimestamp(funInfo.srcfileP)) == 0 // rpkg
//							|| equalsTimestamp(funTimestamp, srcfile.getTimestamp()) )) {
//				ok= true;
//			}
//		}
//		if (ok) {
//			funInfo.done[0]= FunInfo.DONE_SET;
//			return trySetTracepoints(funInfo, funRef, elementTracepoints.getPositions(),
//					elementTracepoints, null );
//		}
//		else {
//			long p;
//			if (funInfo.done[0] < FunInfo.DONE_CLEAR
//					&& funInfo.currentMainFunP != funInfo.orgMainFunP
//					&& (p= this.rEngine.rniGetAttrBySym(funInfo.currentMainFunP, this.dbg.dbgElementId_SymP)) != 0
//					&& elementTracepoints.getElementId().equals(this.rEngine.rniGetString(p)) ) {
//				funInfo.done[0]= FunInfo.DONE_CLEAR;
//				return trySetTracepoints(funInfo, funRef, ImCollections.newList(),
//						elementTracepoints, null );
//			}
//			return TracepointInstallationReport.NOTFOUND;
//		}
//	}
	
	public int trySetTracepoints(final FunInfo funInfo, final ObjectInEnv funRef,
			final List<? extends TracepointPosition> positions,
			final @Nullable ElementTracepoints elementTracepoints) {
		// element
		final int l= positions.size();
		if (l == 0 && funInfo.currentMainFunP == funInfo.orgMainFunP) {
			return TracepointManager.FOUND_UNINST;
		}
		
		int result= TracepointManager.NOT_FOUND; // error
		final int savedProtected= this.rni.saveProtected();
		try {
			long traceArgsP= this.rni.NULL_P;
			traceArgsP= this.rEngine.rniCons(
					funRef.getEnvP(), traceArgsP,
					this.rni.where_SymP, false );
			if (funInfo.rawMethodSignature != null) {
				final String[] signature= SIGNATURE_PATTERN.split(funInfo.rawMethodSignature);
				traceArgsP= this.rEngine.rniCons(
						this.rEngine.rniPutStringArray(signature), traceArgsP,
						this.rni.signature_SymP, false );
			}
			traceArgsP= this.rEngine.rniCons(
					funRef.getNameStrP(), traceArgsP,
					this.rni.what_SymP, false );
			
			long editFunP= 0;
			if (l > 0) {
				// prepare
				final long newMainFunP= this.rni.checkAndProtect(this.rEngine.rniDuplicate(funInfo.orgMainFunP));
				// if (subIndex != null) assert(this.rEngine.rniGetCloBodyExpr(newMainFunP) == bodyP);
				long newMainBodyP= this.rni.checkAndProtect(this.rEngine.rniDuplicate(
						this.rEngine.rniGetCloBodyExpr(newMainFunP) ));
				long newBodyP= newMainBodyP;
				long subListP= 0;
				long subCloP= 0;
				if (funInfo.subIndex != null && funInfo.subIndex.length > 0) {
					for (int i= 0; i < funInfo.subIndex.length; i++) {
						if (this.rEngine.rniExpType(newBodyP) != REXP.LANGSXP
								|| this.rEngine.rniGetLengthLong(newBodyP) < funInfo.subIndex[i]) {
							throw new IllegalStateException();
						}
						for (int idx= 1; idx < funInfo.subIndex[i]; idx++) {
							newBodyP= this.rEngine.rniCDR(newBodyP);
						}
						subListP= newBodyP;
						newBodyP= this.rEngine.rniCAR(newBodyP);
					}
					if (this.rEngine.rniExpType(newBodyP) == REXP.CLOSXP) {
						subListP= 0;
						subCloP= newBodyP;
						// assert(this.rEngine.rniGetCloBodyExpr(subCloP) == bodyP);
						newBodyP= this.rni.checkAndProtect(this.rEngine.rniDuplicate(
								this.rEngine.rniGetCloBodyExpr(newBodyP) ));
					}
				}
				if (this.rEngine.rniExpType(newBodyP) != REXP.LANGSXP) {
					throw new IllegalStateException("unsupported SXP-type: " + this.rEngine.rniExpType(newBodyP));
				}
				
				final long filePathP= this.rni.checkAndProtect(this.rEngine.rniPutString(
						elementTracepoints.getSrcfile().getPath() ));
				final long elementIdP= this.rni.checkAndProtect(this.rEngine.rniPutString(
						elementTracepoints.getElementId() ));
				if (elementTracepoints.getSrcfile().getTimestamp() != 0) {
					this.dbg.setTimestampAttr(elementIdP,
							elementTracepoints.getSrcfile().getTimestamp() );
				}
				
				final int i= 0;
				int j= 0;
				while (j < l) {
					if (positions.get(j).getIndex().length == 0) {
						j++;
					}
					else {
						break;
					}
				}
				
				if (j < l) {
					addTrace(newBodyP, positions.subList(j, l), 0,
							funInfo, filePathP, elementIdP, funInfo.startSrcref, null );
				}
				if (i < j) {
					newBodyP= createTraceLang(newBodyP, positions.subList(i, j),
							funInfo, filePathP, elementIdP, funInfo.startSrcref);
				}
				
				if (subCloP != 0) {
					this.rEngine.rniSetAttrBySym(subCloP, this.rni.original_SymP,
							this.rEngine.rniDuplicate(subCloP));
					this.rEngine.rniSetCloBody(subCloP, newBodyP);
				}
				else if (subListP != 0) {
					this.rEngine.rniSetCAR(subListP, newBodyP);
				}
				else {
					newMainBodyP= newBodyP;
				}
				
				final long tracepointIdsP= this.rni.checkAndProtect(this.rEngine.rniPutRawArray(
						TracepointManager.encodeTracepointIds(positions) ));
				this.rEngine.rniSetAttrBySym(newBodyP, this.dbg.dbgElementId_SymP, elementIdP);
				this.rEngine.rniSetAttrBySym(newBodyP, this.dbg.dbgTracepointIds_SymP, tracepointIdsP);
				if (newBodyP != newMainBodyP) {
					this.rEngine.rniSetAttrBySym(newMainBodyP, this.dbg.dbgElementId_SymP, elementIdP);
					this.rEngine.rniSetAttrBySym(newBodyP, this.dbg.dbgTracepointIds_SymP, tracepointIdsP);
				}
				this.rEngine.rniSetCloBody(newMainFunP, newMainBodyP);
				editFunP= createEditFun(newMainFunP);
			}
			
			if (funInfo.currentMainFunP != funInfo.orgMainFunP) {
				// unset
				this.rni.evalExpr(this.rEngine.rniCons(
								this.untraceSymP, traceArgsP,
								0, true ),
						this.rni.rniSafeGlobalExecEnvP, CODE_DBG_TRACE );
				funInfo.currentMainFunP= funInfo.orgMainFunP;
				result= TracepointManager.FOUND_UNINST;
			}
			if (l > 0) { // set
				this.rni.evalExpr(this.rEngine.rniCons(
								this.traceSymP, this.rEngine.rniCons(
										editFunP, traceArgsP,
										this.rni.edit_SymP, false ),
								0, true ), 
						this.rni.rniSafeGlobalExecEnvP, CODE_DBG_TRACE );
				result= TracepointManager.FOUND_INST_NEW;
			}
		}
		catch (final Exception e) {
			final LogRecord record= new LogRecord(Level.SEVERE,
					"Updating element tracepoints failed for {0}." );
			record.setParameters(new Object[] { funInfo.getLabel(funRef) });
			record.setThrown(e);
			JRISrvErrors.LOGGER.log(record);
		}
		finally {
			this.rni.looseProtected(savedProtected);
		}
		return result;
	}
	
	private long createEditFun(final long newFDefP) {
		final long fBodyP= this.rEngine.rniCons(
				this.rni.Block_SymP, this.rEngine.rniCons(
						newFDefP, this.rni.NULL_P,
						0, false ),
				0, true );
		return this.rni.protect(this.rEngine.rniEval(this.rni.protect(this.rEngine.rniCons(
						this.rni.function_SymP, this.rEngine.rniCons(
								this.editFArgsP, this.rEngine.rniCons(
										fBodyP, this.rni.NULL_P,
										0, false ),
								0, false ),
						0, true )),
				this.rni.rniSafeBaseExecEnvP ));
	}
	
	private void addTrace(final long newP, final List<? extends TracepointPosition> list, final int depth,
			final FunInfo funInfo, final long filePathP, final long elementIdP,
			final int[] baseSrcref, final int[] lastSrcref)
			throws UnexpectedRDataException, RNullPointerException {
		final int length= this.rEngine.rniGetLength(newP);
		final long newSrcrefP= this.rEngine.rniGetAttrBySym(newP, this.dbg.srcref_SymP);
		int currentIdx= 1;
		long currentP= newP;
		for (int i= 0; i < list.size(); ) {
			final int[] breakpointIndex= list.get(i).getIndex();
			final int breakpointIdx= breakpointIndex[depth];
			if (currentIdx > breakpointIdx || breakpointIdx > length) {
				throw new IllegalStateException();
			}
			while (currentIdx < breakpointIdx) {
				currentP= this.rEngine.rniCDR(currentP);
				currentIdx++;
			}
			// collect nested breakpoints
			int j= (depth + 1 == breakpointIndex.length) ? i + 1 : i; // exclusive
			int k= i + 1; // exclusive
			while (k < list.size()) {
				final int[] nextIndex= list.get(k).getIndex();
				if (breakpointIdx == nextIndex[depth]) {
					if (j == k && depth+1 == nextIndex.length) {
						j++;
					}
					k++;
					continue;
				}
				else {
					break;
				}
			}
			
			long currentValueP= this.rEngine.rniCAR(currentP);
			final long currentSrcrefP= (newSrcrefP != 0) ?
					this.rEngine.rniGetVectorElt(newSrcrefP, breakpointIdx-1) : 0;
			int[] currentSrcref= null;
			if (i < j) {
				currentSrcref= bestSrcref(baseSrcref, list.get(i).getSrcref(), currentSrcrefP,
						lastSrcref );
			}
			else {
				if (currentSrcrefP != 0) {
					currentSrcref= this.rEngine.rniGetIntArray(currentSrcrefP);
				}
				if (currentSrcref == null) {
					currentSrcref= lastSrcref;
				}
			}
			if (j < k) {
				if (this.rEngine.rniExpType(currentValueP) != REXP.LANGSXP) {
					throw new IllegalStateException("unsupported SXP-type: " + this.rEngine.rniExpType(currentValueP));
				}
				long orgP;
				if (this.rEngine.rniGetLength(currentValueP) == 3
						&& this.rEngine.rniCAR(currentValueP) == this.rni.function_SymP) {
					orgP= this.rni.checkAndProtect(this.rEngine.rniDuplicate(currentValueP));
				}
				else {
					orgP= 0;
				}
				addTrace(currentValueP, list.subList(j, k), depth+1,
						funInfo, filePathP, elementIdP, baseSrcref, currentSrcref );
				if (orgP != 0) {
					orgP= this.rEngine.rniEval(orgP,
							this.rni.rniSafeBaseExecEnvP );
					if (orgP != 0) {
						currentValueP= this.rEngine.rniEval(currentValueP, 
								this.rni.rniSafeBaseExecEnvP );
						if (currentValueP == 0) {
							throw new UnexpectedRDataException("closure");
						}
						this.rEngine.rniSetAttrBySym(currentValueP, this.rni.original_SymP, orgP);
						this.rEngine.rniSetCAR(currentP, currentValueP);
					}
				}
			}
			if (i < j) {
				this.rEngine.rniSetCAR(currentP, createTraceLang(currentValueP, list.subList(i, j),
						funInfo, filePathP, elementIdP, currentSrcref ));
			}
			i= k;
		}
	}
	
	private int[] bestSrcref(final int[] baseSrcref, final int[] positionSrcref, 
			final long currentSrcrefP, final int[] lastSrcref)
			throws UnexpectedRDataException {
		int[] currentSrcref= null;
		if (currentSrcrefP != 0) {
			currentSrcref= this.rEngine.rniGetIntArray(currentSrcrefP);
			this.rEngine.rniSetAttrBySym(currentSrcrefP,
					this.rni.what_SymP, this.stepNextValueP );
		}
		else if (baseSrcref != null && positionSrcref != null) {
			currentSrcref= Srcref.add(positionSrcref, baseSrcref);
			if (currentSrcref != null && lastSrcref != null) {
				if (currentSrcref[0] < lastSrcref[0]
						|| currentSrcref[2] > lastSrcref[2] ) {
					currentSrcref= null;
				}
				else {
					if (currentSrcref[4] != Integer.MIN_VALUE
							&& lastSrcref[4] != Integer.MIN_VALUE
							&& currentSrcref[0] == lastSrcref[0]
							&& currentSrcref[4] < lastSrcref[4] ) {
						currentSrcref[4]= Integer.MIN_VALUE;
					}
					if (currentSrcref[5] != Integer.MIN_VALUE
							&& lastSrcref[5] != Integer.MIN_VALUE
							&& currentSrcref[2] == lastSrcref[2]
							&& currentSrcref[5] > lastSrcref[5] ) {
						currentSrcref[5]= Integer.MIN_VALUE;
					}
				}
			}
		}
		if (currentSrcref == null && lastSrcref != null) {
			currentSrcref= lastSrcref;
		}
		return currentSrcref;
	}
	
	private long createTraceLang(long currentP, final List<? extends TracepointPosition> list,
			final FunInfo funInfo, final long filePathP, final long elementIdP,
			final int[] currentSrcref) throws RNullPointerException {
		for (int i= list.size()-1; i >= 0; i--) {
			int n;
			final TracepointPosition position= list.get(i);
			if (position.getType() == Tracepoint.TYPE_LB) {
				final long breakpointP= createBreakpointCall(position, currentSrcref,
						funInfo, filePathP, elementIdP, 0 );
				currentP= this.rEngine.rniCons(
						breakpointP, this.rEngine.rniCons(
								currentP, this.rni.NULL_P,
								0, false ),
						0, false );
				n= 1;
			}
			else if (position.getType() == Tracepoint.TYPE_FB) {
				final long entryP= createBreakpointCall(position, currentSrcref,
						funInfo, filePathP, elementIdP, TracepointState.FLAG_MB_ENTRY );
				long exitP= createBreakpointCall(position, currentSrcref,
						funInfo, filePathP, elementIdP, TracepointState.FLAG_MB_EXIT );
				exitP= this.rni.protect(this.rEngine.rniCons(
						this.rni.onExit_SymP, this.rEngine.rniCons(
								exitP, this.rni.NULL_P,
								0, false ),
						0, true ));
				currentP= this.rEngine.rniCons(
						exitP, this.rEngine.rniCons(
							entryP, this.rEngine.rniCons(
									currentP, this.rni.NULL_P,
									0, false ),
							0, false ),
						0, false );
				n= 2;
			}
			else {
				continue;
			}
			currentP= this.rni.checkAndProtect(this.rEngine.rniCons(
					this.rni.Block_SymP, currentP,
					0, true ));
			this.rEngine.rniSetAttrBySym(currentP, this.dbg.srcref_SymP, createTraceSrcref(n,
					(currentSrcref != null) ? currentSrcref : NA_SRCREF, funInfo.srcfileP, elementIdP ));
		}
		return currentP;
	}
	
	private long createBreakpointCall(final TracepointPosition position, final int[] srcref,
			final FunInfo funInfo, final long filePathP, final long elementIdP, final int flags)
			throws RNullPointerException {
		final long exprP= this.rni.checkAndProtect(this.rEngine.rniDuplicate(this.breakpointTmplP));
		{	final long[] srcrefList= new long[2];
			srcrefList[0]= this.rni.checkAndProtect(this.rEngine.rniDuplicate(
					this.stepSrcrefTmplP ));
			srcrefList[1]= this.rni.checkAndProtect(this.rEngine.rniPutIntArray(
					(srcref != null) ? srcref : NA_SRCREF ));
			this.rEngine.rniSetAttrBySym(srcrefList[1], this.dbg.srcfile_SymP, funInfo.srcfileP);
			if (filePathP != 0) {
				this.rEngine.rniSetAttrBySym(srcrefList[1], this.rni.appFilePath_SymP, filePathP);
			}
			this.rEngine.rniSetAttrBySym(srcrefList[1], this.dbg.dbgElementId_SymP, elementIdP);
			this.rEngine.rniSetAttrBySym(srcrefList[1], this.rni.at_SymP,
					this.rEngine.rniPutIntArray(position.getIndex()) );
			this.rEngine.rniSetAttrBySym(srcrefList[1], this.rni.id_SymP,
					this.rEngine.rniPutRawArray(RDataUtils.encodeLongToRaw(position.getId())) );
			if (flags != 0) {
				this.rEngine.rniSetAttrBySym(srcrefList[1], this.rni.flags_SymP,
						this.rEngine.rniPutIntArray(new int[] { flags }) );
			}
			this.rEngine.rniSetAttrBySym(srcrefList[1], this.rni.what_SymP, this.stepNextValueP);
			this.rEngine.rniSetAttrBySym(exprP, this.dbg.srcref_SymP, this.rEngine.rniPutVector(srcrefList));
		}
		return exprP;
	}
	
	private long createTraceSrcref(final int n, final int[] srcref,
			final long srcfileP, final long elementIdP)
			throws RNullPointerException {
		final long[] list= new long[n+2];
		list[0]= this.rni.checkAndProtect(this.rEngine.rniPutIntArray(srcref));
		this.rEngine.rniSetAttrBySym(list[0], this.dbg.srcfile_SymP, srcfileP);
		this.rEngine.rniSetAttrBySym(list[0], this.rni.what_SymP, this.stepNextValueP);
		this.rEngine.rniSetAttrBySym(list[0], this.dbg.dbgElementId_SymP, elementIdP);
		for (int i= 1; i <= n; i++) {
			list[i]= list[0];
		}
		list[n+1]= this.rni.checkAndProtect(this.rEngine.rniPutIntArray(srcref));
		this.rEngine.rniSetAttrBySym(list[n+1], this.dbg.srcfile_SymP, srcfileP);
		this.rEngine.rniSetAttrBySym(list[n+1], this.dbg.dbgElementId_SymP, elementIdP);
		return this.rEngine.rniPutVector(list);
	}
	
	public long createBreakpointSuspend(final int codeFlags, final int[] srcref)
			throws RNullPointerException{
		final long browserExprP= this.rni.checkAndProtect(this.rEngine.rniDuplicate(
				this.browserExprP ));
		
		final long[] srcrefList= new long[2];
		srcrefList[0]= this.rni.checkAndProtect(this.rEngine.rniDuplicate(this.stepSrcrefTmplP));
		srcrefList[1]= this.rni.checkAndProtect(this.rEngine.rniPutIntArray(srcref));
		if ((codeFlags & TracepointState.FLAG_MB_EXIT) == 0) {
			this.rEngine.rniSetAttrBySym(srcrefList[1], this.rni.what_SymP, this.stepNextValueP);
		}
		this.rEngine.rniSetAttrBySym(browserExprP, this.dbg.srcref_SymP,
				this.rEngine.rniPutVector(srcrefList) );
		
		return browserExprP;
	}
	
	
	public long createExceptionHandler() throws RNullPointerException {
		return this.errorHandlerP;
	}
	
	public long createExceptionSuspend()
			throws RNullPointerException {
		return this.rni.checkAndProtect(this.rEngine.rniDuplicate(
				this.browserExprP ));
	}
	
	
	public long createFallbackSuspend() {
		return this.browserExprP;
	}
	
}
