/*=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Apache License, Version 2.0 which is available at
 # https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package de.walware.rj.internal.server.gr.jri;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import org.rosuda.JRI.REXP;
import org.rosuda.JRI.Rengine;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.collections.ImList;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.server.RjsException;
import org.eclipse.statet.rj.server.rh.Handle;
import org.eclipse.statet.rj.server.rh.RhEngine;
import org.eclipse.statet.rj.server.rh.RhRefListener;
import org.eclipse.statet.rj.server.rh.RhWeakRef;


@NonNullByDefault
public final class GRhEngine implements RhEngine {
	
	
	public static final class R0PointerException extends RjsException {
		
		private static final long serialVersionUID= 1L;
		
		public R0PointerException() {
			super(0, "R engine returned unexpected null pointer (out of memory?).");
		}
		
	}
	
	private static final class Handles {
		
		private Handle[] array;
		private int size;
		
		public Handles() {
			this.array= new Handle[512];
		}
		
		public Handles reset() {
			this.size= 0;
			return this;
		}
		
		public void add(final Handle handle) {
			Object[] a= this.array;
			if (a.length == this.size) {
				this.array= new Handle[a.length + 512];
				System.arraycopy(a, 0, this.array, 0, a.length);
				a= this.array;
			}
			a[this.size++]= handle;
		}
		
		public int size() {
			return this.size;
		}
		
		public ImList<Handle> toImList() {
			return ImCollections.toList(this.array, 0, this.size);
		}
		
	}
	
	private static class WeakRef extends RhWeakRef implements Runnable {
		
		
		public WeakRef(final Handle handle, final @Nullable RhRefListener listener,
				final GRhEngine engine) {
			super(handle, listener);
			
			engine.jri.rniAddObjFinalizer(this.handle.p, this);
		}
		
		
		@Override
		public void run() {
			onFinalized();
		}
		
		@Override
		public void dispose(final RhEngine engine) {
			super.dispose(engine);
			
			((GRhEngine) engine).jri.rniRemoveObjFinalizer(this.handle.p, this);
		}
		
	}
	
	
	private static final Arg[] NO_ARGS= new Arg[0];
	
	private static void assertNon0(final long p) throws RjsException {
		if (p == 0) {
			throw new R0PointerException();
		}
	}
	
	private static void assertExpType(final int type, final int expectedType) throws RjsException {
		if (type != expectedType) {
			throw new RjsException(0, "type= " + type);
		}
	}
	
	private static boolean equals(final Handle h1, final Handle h2) {
		return (h1 == h2 || h1.p == h2.p);
	}
	
	private static long pElse0(final @Nullable Handle handle) {
		return (handle != null) ? handle.p : 0;
	}
	
	
	private final Rengine jri;
	
	private final Handle rNull;
	
	private final Handle rEmptyEnv;
	private final Handle rBaseEnv;
	private final Handle rGlobalEnv;
	
	private final Handle rNewEnvFun;
	private final Arg[] rNewEnvArgs;
	
	private final Handle rSysFramesFun;
	private final Handle rSysFramesCall;
	
	private final Handles tmpHandles= new Handles();
	
	
	public GRhEngine(final Rengine jri) throws RjsException {
		this.jri= jri;
		
		this.rNull= createHandle(this.jri.rniSpecialObject(Rengine.SO_NilValue));
		
		this.rEmptyEnv= createHandle(this.jri.rniSpecialObject(Rengine.SO_EmptyEnv));
		this.rBaseEnv= createHandle(this.jri.rniSpecialObject(Rengine.SO_BaseEnv));
		this.rGlobalEnv= createHandle(this.jri.rniSpecialObject(Rengine.SO_GlobalEnv));
		
		this.rNewEnvFun= preserve(eval(asName("new.env"), this.rBaseEnv)); //$NON-NLS-1$
		this.rNewEnvArgs= new Arg[] {
				new Arg(asName("parent"), null) //$NON-NLS-1$
		};
		
		this.rSysFramesFun= preserve(eval(asName("sys.frames"), this.rBaseEnv)); //$NON-NLS-1$
		this.rSysFramesCall= preserve(newCall(this.rSysFramesFun, NO_ARGS));
	}
	
	
	private Handle createHandle(final long p) throws RjsException {
		assertNon0(p);
		return new Handle(p);
	}
	
	
	@Override
	public Handle preserve(final Handle obj) {
		this.jri.rniPreserve(obj.p);
		return obj;
	}
	
	@Override
	public void releasePreserved(final Handle obj) {
		this.jri.rniRelease(obj.p);
	}
	
	@Override
	public RhWeakRef newWeakRef(final Handle obj, final @Nullable RhRefListener listener) {
		return new WeakRef(obj, listener, this);
	}
	
	
	@Override
	public Handle getNull() {
		return this.rNull;
	}
	
	@Override
	public Handle getEmptyEnv() {
		return this.rEmptyEnv;
	}
	
	@Override
	public Handle getBaseEnv() {
		return this.rBaseEnv;
	}
	
	@Override
	public Handle getGlobalEnv() {
		return this.rGlobalEnv;
	}
	
	@Override
	public Handle newEnv(final Handle parentEnv) throws RjsException {
		this.rNewEnvArgs[0].setValue(parentEnv);
		return eval(newCall(this.rNewEnvFun, this.rNewEnvArgs), this.rBaseEnv);
	}
	
	@Override
	public @Nullable Handle getParentEnv(final Handle env) throws RjsException {
		if (equals(env, this.rEmptyEnv)) {
			return null;
		}
		return createHandle(this.jri.rniParentEnv(env.p));
	}
	
	
	@Override
	public Handle asName(final String name) throws RjsException {
		return createHandle(this.jri.rniInstallSymbol(name));
	}
	
	private long installArgs(final Arg[] funArgs) throws RjsException {
		long p= this.rNull.p;
		for (int i= funArgs.length - 1; i >= 0; i--) {
			final Arg arg= funArgs[i];
			p= this.jri.rniCons(arg.getValue().p, p, pElse0(arg.getName()), false);
			assertNon0(p);
		}
		return p;
	}
	
	@Override
	public Handle newCall(final Handle fun, final Arg... funArgs) throws RjsException {
		return createHandle(this.jri.rniCons(fun.p, installArgs(funArgs), 0, true));
	}
	
	@Override
	public Handle eval(final Handle expr, final Handle env) throws RjsException {
		return createHandle(this.jri.rniEval(expr.p, pElse0(env)));
	}
	
	
	@Override
	public @Nullable Handle getAttribute(final Handle obj, final Handle name) throws RjsException {
		final long p= this.jri.rniGetAttrBySym(obj.p, name.p);
		return (p != 0) ? new Handle(p) : null;
	}
	
	
	@Override
	public ImList<Handle> getSearchPath() throws RjsException {
		final Handles handles= this.tmpHandles.reset();
		final Handle baseEnv= getBaseEnv();
		for (Handle env= getGlobalEnv(); !equals(env, baseEnv);
				env= nonNullAssert(getParentEnv(env)) ) {
			handles.add(env);
		}
		handles.add(baseEnv);
		return handles.toImList();
	}
	
	@Override
	public ImList<Handle> getStackFrames() throws RjsException {
		final Handles handles= this.tmpHandles.reset();
		final Handle list= eval(this.rSysFramesCall, this.rBaseEnv);
		if (this.jri.rniExpType(list.p) == REXP.LISTSXP) {
			for (long cdr= list.p; cdr != 0; cdr= this.jri.rniCDR(cdr)) {
				final long car= this.jri.rniCAR(cdr);
				assertExpType(this.jri.rniExpType(car), REXP.ENVSXP);
				handles.add(new Handle(car));
			}
		}
		return handles.toImList();
	}
	
}
