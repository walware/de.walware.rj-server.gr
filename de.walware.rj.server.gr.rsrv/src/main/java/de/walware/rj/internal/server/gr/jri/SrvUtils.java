/*=============================================================================#
 # Copyright (c) 2008, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Apache License, Version 2.0 which is available at
 # https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package de.walware.rj.internal.server.gr.jri;

import static de.walware.rj.internal.server.gr.jri.JRISrvErrors.LOGGER;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.function.IntFunction;
import java.util.logging.Level;

import org.rosuda.JRI.Rengine;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RCharacterStore;


@NonNullByDefault
public final class SrvUtils {
	
	
	public static final String R_NATIVE_ENCODING_NAME= "native.enc"; //$NON-NLS-1$
	
	
	private final StringBuilder rTempStringBuilder= new StringBuilder(0x2000);
	
	private final Map<String, Object> platformData;
	private int @Nullable [] rVersion;
	
	private final JRIObjectFactory rObjectFactory;
	
	private @Nullable Charset sysCharset;
	
	
	public SrvUtils(final Map<String, Object> platformData, final JRIObjectFactory rObjectFactory) {
		this.platformData= platformData;
		this.rObjectFactory= rObjectFactory;
	}
	
	
	public String concat(final String[] array, final char sep) {
		this.rTempStringBuilder.setLength(0);
		for (int i= 0; i < array.length; i++) {
			this.rTempStringBuilder.append(array[i]);
			this.rTempStringBuilder.append(sep);
		}
		return this.rTempStringBuilder.toString();
	}
	
	
	public Charset getSysCharset() {
		Charset charset= this.sysCharset;
		if (charset == null) {
			String charsetName= null;
			try {
				charsetName= Rengine.getSysEncoding();
				charset= Charset.forName(charsetName);
			}
			catch (final IllegalArgumentException e) {
				LOGGER.log(Level.SEVERE, String.format("Failed to detect system charset (%1$s).", charsetName),
						e );
				charset= StandardCharsets.UTF_8;
			}
			this.sysCharset= charset;
		}
		return charset;
	}
	
	public Charset getCharset(final @Nullable String charsetName) {
		if (charsetName != null) {
			try {
				if (charsetName.equals(R_NATIVE_ENCODING_NAME)) {
					return getSysCharset();
				}
				return Charset.forName(charsetName);
			}
			catch (final IllegalArgumentException e) {
				LOGGER.log(Level.SEVERE, String.format("Failed to get specified charset (%1$s).", charsetName),
						e );
			}
		}
		return StandardCharsets.UTF_8;
	}
	
	public String readFileToString(final Path path, final Charset charset) throws IOException {
		final byte[] bytes= Files.readAllBytes(path);
		return new String(bytes, charset);
	}
	
	public RCharacterStore readFilesToStore(final int length, final IntFunction<Path> pathSupplier,
			final Charset charset, final boolean deleteFiles) {
		if (charset.equals(StandardCharsets.UTF_8)) {
			final byte [] @Nullable [] texts= new byte [length] @Nullable [];
			for (int i= 0; i < length; i++) {
				Path path= null;
				try {
					path= pathSupplier.apply(i);
					texts[i]= Files.readAllBytes(path);
				}
				catch (final Exception e) {
					texts[i]= ("<error: " + e.getMessage() + ">").getBytes(StandardCharsets.UTF_8);
				}
				if (deleteFiles && path != null) {
					try {
						Files.delete(path);
					}
					catch (final IOException e) {}
				}
			}
			return this.rObjectFactory.createCharDataUtf8(texts);
		}
		else {
			final @Nullable String[] texts= new @Nullable String[length];
			for (int i= 0; i < length; i++) {
				Path path= null;
				try {
					path= pathSupplier.apply(i);
					texts[i]= readFileToString(path, charset);
				}
				catch (final Exception e) {
					texts[i]= "<error: " + e.getMessage() + ">";
				}
				if (deleteFiles && path != null) {
					try {
						Files.delete(path);
					}
					catch (final IOException e) {}
				}
			}
			return this.rObjectFactory.createCharData(texts);
		}
	}
	
	
	public String checkFilename(final String filename) {
		if (filename.charAt(0) == '\\') {
			return (filename.indexOf('/', 1) > 0) ? filename.replace('/', '\\') : filename;
		}
		else {
			return (filename.indexOf('\\', 1) > 0) ? filename.replace('\\', '/') : filename;
		}
	}
	
	public @Nullable Object getPlatformDataValue(final String key) {
		return this.platformData.get(key);
	}
	
	public int @Nullable [] getRVersion() {
		int[] rVersion= this.rVersion;
		if (rVersion == null) {
			final Object value= this.platformData.get("version.string"); //$NON-NLS-1$
			if (value instanceof String) {
				final @NonNull String[] segments= ((String)value).split("\\."); //$NON-NLS-1$
				if (segments.length >= 3) {
					try {
						final int[] version= new int[3];
						for (int i= 0; i < 3; i++) {
							version[i]= Integer.parseInt(segments[i]);
						}
						this.rVersion= rVersion= version;
					}
					catch (final NumberFormatException e) {}
				}
			}
		}
		return rVersion;
	}
	
	public boolean isRVersionEqualGreater(final int major, final int minor) {
		final int[] rVersion= getRVersion();
		return (rVersion != null && (rVersion[0] > major
				|| (rVersion[0] == major && rVersion[1] >= minor) ));
	}
	
	public boolean isRVersionLess(final int major, final int minor) {
		final int[] rVersion= getRVersion();
		return (rVersion != null && (rVersion[0] < major
				|| (rVersion[0] == major && rVersion[1] < minor) ));
	}
	
}
