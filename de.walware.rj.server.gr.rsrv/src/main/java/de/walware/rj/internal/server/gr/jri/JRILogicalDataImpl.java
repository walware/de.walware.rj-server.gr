/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Apache License, Version 2.0 which is available at
 # https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package de.walware.rj.internal.server.gr.jri;

import java.io.IOException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.impl.RLogicalInt32Store;


@NonNullByDefault
public class JRILogicalDataImpl extends RLogicalInt32Store {
	
	
	public JRILogicalDataImpl(final boolean[] values) {
		super(values, (int[])null);
	}
	
	public JRILogicalDataImpl(final int[] value) {
		super(value);
	}
	
	public JRILogicalDataImpl(final RJIO io, final int length) throws IOException {
		super(io, length);
	}
	
	
	public int[] getJRIValueArray() {
		final int l= length();
		if (this.boolValues.length == l) {
			return this.boolValues;
		}
		final int[] array= new int[l];
		System.arraycopy(this.boolValues, 0, array, 0, l);
		return array;
	}
	
}
