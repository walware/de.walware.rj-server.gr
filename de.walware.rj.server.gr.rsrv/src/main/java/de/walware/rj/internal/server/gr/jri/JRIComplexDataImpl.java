/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Apache License, Version 2.0 which is available at
 # https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package de.walware.rj.internal.server.gr.jri;

import java.io.IOException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;

import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.impl.RComplexB32Store;


@NonNullByDefault
public class JRIComplexDataImpl extends RComplexB32Store {
	
	
	public JRIComplexDataImpl(final double[] realValues, final double[] imaginaryValues) {
		super(realValues, imaginaryValues);
		for (int i= 0; i < imaginaryValues.length; i++) {
			if (Double.isNaN(imaginaryValues[i])) {
				if ((int) Double.doubleToRawLongBits(imaginaryValues[i]) == NA_numeric_INT_MATCH) {
					realValues[i]= NA_numeric_DOUBLE;
					imaginaryValues[i]= NA_numeric_DOUBLE;
				}
				else {
					realValues[i]= NaN_numeric_DOUBLE;
					imaginaryValues[i]= NaN_numeric_DOUBLE;
				}
			}
		}
	}
	
	public JRIComplexDataImpl(final RJIO io, final int length) throws IOException {
		super(io, length);
	}
	
	
	public double[] getJRIRealValueArray() {
		final int l= length();
		if (this.realValues.length == l) {
			return this.realValues;
		}
		final double[] array= new double[l];
		System.arraycopy(this.realValues, 0, array, 0, l);
		return array;
	}
	
	public double[] getJRIImaginaryValueArray() {
		final int l= length();
		if (this.imaginaryValues.length == l) {
			return this.imaginaryValues;
		}
		final double[] array= new double[l];
		System.arraycopy(this.imaginaryValues, 0, array, 0, l);
		return array;
	}
	
}
