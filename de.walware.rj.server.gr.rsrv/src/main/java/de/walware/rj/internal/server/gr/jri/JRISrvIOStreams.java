/*=============================================================================#
 # Copyright (c) 2013, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Apache License, Version 2.0 which is available at
 # https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package de.walware.rj.internal.server.gr.jri;

import static de.walware.rj.internal.server.gr.jri.JRISrvErrors.LOGGER;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CoderResult;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.concurrent.locks.Condition;
import java.util.logging.ConsoleHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import org.rosuda.JRI.Rengine;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.RjInitFailedException;
import org.eclipse.statet.rj.server.ConsoleWriteCmdItem;


@NonNullByDefault
class JRISrvIOStreams {
	
	
	private static final int LOCAL_CHAR_BUFFER_SIZE= 0x2000;
	private static final int LOCAL_BYTE_BUFFER_SIZE= LOCAL_CHAR_BUFFER_SIZE * 3;
	
	
	static interface StreamHandler {
		
		byte getStreamId();
		
		void domexFlush();
		
	}
	
	
	abstract class AbstractOutPipe extends Thread implements StreamHandler {
		
		
		private final byte id;
		
		protected final ByteBuffer bbNative;
		protected final ByteBuffer bbNoInput= ByteBuffer.allocate(0);
		
		private final CharsetDecoder decoder;
		
		private boolean working;
		
		
		public AbstractOutPipe(final byte id, final Charset charset) {
			super("OutPipe-" + id); //$NON-NLS-1$
			setDaemon(true);
			
			this.id= id;
			this.bbNative= ByteBuffer.allocateDirect(LOCAL_CHAR_BUFFER_SIZE * 2);
			
			this.decoder= charset.newDecoder()
					.onMalformedInput(CodingErrorAction.REPLACE)
					.onUnmappableCharacter(CodingErrorAction.REPLACE);
		}
		
		
		@Override
		public final byte getStreamId() {
			return this.id;
		}
		
		@Override
		public final void run() {
			while (true) {
				int read;
				try {
					read= doRead();
				}
				catch (final IOException e) {
					read= -1;
					LOGGER.log(Level.SEVERE, "An error occurred when reading output stream.", e);
				}
				
				if (read > 0) {
					this.bbNative.position(this.bbNative.position() + read);
				}
				this.bbNative.flip();
				
				JRISrvIOStreams.this.server.mainExchangeLock.lock();
				try {
					domexBeginStream(this);
					
					DECODE: while (true) {
						this.working= true;
						
						final CoderResult result;
						if (read > 0) {
							result= this.decoder.decode(this.bbNative,
									JRISrvIOStreams.this.outputCharBuffer, false );
						}
						else if (read < 0) {
							result= this.decoder.decode(this.bbNative,
									JRISrvIOStreams.this.outputCharBuffer, true );
						}
						else {
							result= null;
						}
						if (result == CoderResult.OVERFLOW) {
							domexSendOut();
							continue DECODE;
						}
						else if (read < 0) {
							this.bbNative.clear();
							return;
						}
						else { // result == CoderResult.UNDERFLOW
							break DECODE;
						}
					}
				}
				finally {
					JRISrvIOStreams.this.server.mainExchangeLock.unlock();
				}
				
				this.bbNative.compact();
			}
		}
		
		@Override
		public final void domexFlush() {
			try {
				do {
					this.working= false;
					try {
						JRISrvIOStreams.this.mexCondition.awaitNanos(50000000L);
					}
					catch (final InterruptedException e) {
					}
				} while (this.working);
				
				CoderResult result= this.decoder.decode(this.bbNoInput,
						JRISrvIOStreams.this.outputCharBuffer, true );
				while (result == CoderResult.OVERFLOW) {
					domexSendOut();
					result= this.decoder.flush(
							JRISrvIOStreams.this.outputCharBuffer );
				}
			}
			finally {
				this.decoder.reset();
			}
		}
		
		protected abstract int doRead() throws IOException;
		
	}
	
	private class SysOutPipe extends AbstractOutPipe {
		
		public SysOutPipe(final Charset charset) throws Exception {
			super(ConsoleWriteCmdItem.SYS_OUTPUT, charset);
			
			final ConsoleHandler[] consoleHandlers= getConsoleHandlers();
			
			System.out.flush();
			System.err.flush();
			
			final int code= Rengine.rniInitSysPipes(this.bbNative, consoleHandlers);
			if (code != 0) {
				throw new RjInitFailedException("Error code of InitSysPipes: " + code); //$NON-NLS-1$
			}
		}
		
		
		@Override
		protected int doRead() throws IOException {
			return Rengine.rniReadSysOut(this.bbNative.position());
		}
		
	}
	
	
	private final GRSrvEngine server;
	
	
	//-- mex --
	
	private final Condition mexCondition;
	
	private byte currentStreamId;
	
	private @Nullable StreamHandler currentHandler;
	
	private final ByteBuffer localByteBuffer= ByteBuffer.allocate(LOCAL_BYTE_BUFFER_SIZE);
	private final CharBuffer outputCharBuffer= CharBuffer.allocate(LOCAL_CHAR_BUFFER_SIZE);
	private final CharsetDecoder utf8Decoder;
	
	private volatile boolean hasLastROutput;
	private @Nullable String lastROutput1;
	private @Nullable String lastROutput2;
	
	
	public JRISrvIOStreams(final GRSrvEngine server) {
		this.server= server;
		this.mexCondition= this.server.mainExchangeLock.newCondition();
		
		this.utf8Decoder= StandardCharsets.UTF_8.newDecoder()
				.onMalformedInput(CodingErrorAction.REPLACE)
				.onUnmappableCharacter(CodingErrorAction.REPLACE);
	}
	
	
	void init() {
		// Sys pipes
		if (!Boolean.getBoolean("org.eclipse.statet.rj.server.sysout.disable")) { //$NON-NLS-1$
			try {
				Charset charset= null;
				{	final String enc= System.getProperty("org.eclipse.statet.rj.server.sysout.Encoding"); //$NON-NLS-1$
					if (enc != null) {
						try {
							charset= Charset.forName(enc);
						}
						catch (final IllegalArgumentException e) {
							LOGGER.log(Level.WARNING, "Failed to setup specified encoding for system output.", e);
						}
					}
				}
				if (charset == null) {
					final String enc= Rengine.getSysOutEncoding();
					if (enc != null) {
						try {
							charset= Charset.forName(enc);
						}
						catch (final IllegalArgumentException e) {}
					}
				}
				if (charset == null) {
					charset= Charset.defaultCharset();
				}
				
				final AbstractOutPipe abstractOutPipe= new SysOutPipe(charset);
				abstractOutPipe.start();
			}
			catch (final Exception e) {
				LOGGER.log(Level.WARNING, "Failed to setup redirect of system pipes.", e);
			}
		}
	}
	
	void domexAppendROutUtf8(final byte streamId, final ByteBuffer text) {
		if (this.currentStreamId != streamId) {
			domexFlushOut();
			this.currentStreamId= streamId;
			this.currentHandler= null;
		}
		
		this.hasLastROutput= true;
		final ByteBuffer bb= this.localByteBuffer;
		try {
			bb.clear();
			READ: while (true) {
				final boolean end;
				{	// read
					final int bbWritePos= bb.position();
					final int n= Math.min(text.remaining(), bb.capacity() - bbWritePos);
//					bb.put(bb.position(), text, text.position(), n);
					text.get(bb.array(), bbWritePos, n);
					bb.position(bbWritePos + n);
					bb.flip();
					end= !text.hasRemaining();
				}
				DECODE: while (true) {
					final CoderResult result;
					result= this.utf8Decoder.decode(bb, this.outputCharBuffer, end);
					if (result == CoderResult.OVERFLOW) {
						domexSendOut();
						if (end || bb.remaining() >= 0x400) {
							continue DECODE;
						}
					}
					else {
						if (end) {
							break READ;
						}
					}
					bb.compact();
					continue READ;
				}
			}
		}
		finally {
			this.utf8Decoder.reset();
		}
	}
	
	void domexBeginStream(final StreamHandler handler) {
		if (this.currentStreamId != handler.getStreamId()) {
			domexFlushOut();
			this.currentStreamId= handler.getStreamId();
			this.currentHandler= handler;
		}
	}
	
	boolean domexHasOut() {
		return (this.outputCharBuffer.position() > 0);
	}
	
	void domexSendOut() {
		if (this.outputCharBuffer.position() > 0) {
			this.outputCharBuffer.flip();
			final String text= this.outputCharBuffer.toString();
			this.outputCharBuffer.clear();
			this.server.domexAppend2C(new ConsoleWriteCmdItem(this.currentStreamId, text));
			
			if (this.currentHandler == null && this.hasLastROutput) {
				this.lastROutput2= this.lastROutput1;
				this.lastROutput1= text;
			}
		}
	}
	
	void domexFlushOut() {
		final StreamHandler handler= this.currentHandler;
		if (handler != null) {
			try {
				handler.domexFlush();
			}
			catch (final Exception e) {
				LOGGER.log(Level.SEVERE, "An error occurred when flushing output stream.", e);
			}
			finally {
				this.currentHandler= null;
			}
		}
		domexSendOut();
	}
	
	
	public void resetLastROut() {
		this.hasLastROutput= false;
		this.lastROutput1= null;
	}
	
	public @Nullable String getLastROut() {
		if (this.hasLastROutput) {
			this.server.mainExchangeLock.lock();
			try {
				if (this.currentHandler == null) {
					domexSendOut();
				}
				final String text= this.lastROutput1;
				if (text != null && this.lastROutput2 != null && text.length() < 0x1000) {
					return this.lastROutput2 + text;
				}
				return text;
			}
			finally {
				this.server.mainExchangeLock.unlock();
			}
		}
		return null;
	}
	
	
	protected static ConsoleHandler[] getConsoleHandlers() {
		final List<ConsoleHandler> consoleHandlers= new ArrayList<>();
		final LogManager logManager= LogManager.getLogManager();
		for (final Enumeration<String> loggerNames= logManager.getLoggerNames(); loggerNames.hasMoreElements(); ) {
			final Logger logger= logManager.getLogger(loggerNames.nextElement());
			if (logger != null) {
				for (final Handler handler : logger.getHandlers()) {
					if (handler instanceof ConsoleHandler
							&& !consoleHandlers.contains(handler) ) {
						handler.flush();
						consoleHandlers.add((ConsoleHandler) handler);
					}
				}
			}
		}
		return consoleHandlers.toArray(new ConsoleHandler[consoleHandlers.size()]);
	}
	
}
