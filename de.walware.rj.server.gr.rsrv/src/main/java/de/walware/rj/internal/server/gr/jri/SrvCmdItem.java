/*=============================================================================#
 # Copyright (c) 2012, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Apache License, Version 2.0 which is available at
 # https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package de.walware.rj.internal.server.gr.jri;

import java.io.IOException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.server.MainCmdItem;
import org.eclipse.statet.rj.server.RjsStatus;


@NonNullByDefault
public final class SrvCmdItem extends MainCmdItem {
	
	
	public static final byte OP_CLEAR_SESSION= 1;
	
	
	private final byte op;
	
	
	public SrvCmdItem(final byte op) {
		this.op= op;
	}
	
	@Override
	public void writeExternal(final RJIO io) throws IOException {
	}
	
	
	@Override
	public byte getCmdType() {
		return T_SRV_ITEM;
	}
	
	@Override
	public byte getOp() {
		return this.op;
	}
	
	@Override
	public void setAnswer(final RjsStatus status) {
	}
	
	@Override
	public boolean isOK() {
		return true;
	}
	
	@Override
	public RjsStatus getStatus() {
		return RjsStatus.OK_STATUS;
	}
	
	@Override
	public @Nullable String getDataText() {
		return null;
	}
	
	
	@Override
	public boolean testEquals(final MainCmdItem other) {
		if (!(other instanceof SrvCmdItem)) {
			return false;
		}
		final SrvCmdItem otherItem= (SrvCmdItem) other;
		if (getOp() != otherItem.getOp()) {
			return false;
		}
		if (this.options != otherItem.options) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		final StringBuffer sb= new StringBuffer(100);
		sb.append("SrvCmdItem ");
		switch (this.op) {
		case OP_CLEAR_SESSION:
			sb.append("CLEAR_SESSION");
			break;
		default:
			sb.append(this.op);
			break;
		}
		sb.append("\n\t").append("options= 0x").append(Integer.toHexString(this.options));
		return sb.toString();
	}
	
}
