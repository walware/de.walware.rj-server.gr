/*=============================================================================#
 # Copyright (c) 2010, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Apache License, Version 2.0 which is available at
 # https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package de.walware.rj.internal.server.gr.jri;

import static org.eclipse.statet.rj.data.impl.RLanguageImpl.getBaseClassname;

import java.io.IOException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RLanguage;
import org.eclipse.statet.rj.data.RObjectFactory;
import org.eclipse.statet.rj.data.RStore;
import org.eclipse.statet.rj.data.impl.AbstractRObject;
import org.eclipse.statet.rj.data.impl.ExternalizableRObject;


@NonNullByDefault
public class JRILanguageImpl extends AbstractRObject
		implements RLanguage, ExternalizableRObject {
	
	
	private final byte type;
	
	private final @Nullable String className1;
	
	private final @Nullable String source;
	
	
	public JRILanguageImpl(final byte type, final String source, final @Nullable String className1) {
		this.type= type;
		this.className1= className1;
		this.source= source;
	}
	
	public JRILanguageImpl(final byte type, final @Nullable String className1) {
		this.type= type;
		this.className1= className1;
		this.source= null;
	}
	
	public JRILanguageImpl(final RJIO io, final RObjectFactory factory) throws IOException {
		final int options= io.readInt();
		this.type= io.readByte();
		//-- special attributes
		this.className1= ((options & RObjectFactory.O_CLASS_NAME) != 0) ?
				io.readString() :
				getBaseClassname(this.type);
		//-- data
		if ((io.flags & RObjectFactory.F_ONLY_STRUCT) == 0) {
			this.source= io.readString();
		}
		else {
			this.source= null;
		}
	}
	
	@Override
	public void writeExternal(final RJIO io, final RObjectFactory factory) throws IOException {
		int options= 0;
		if (this.className1 != null
				&& !this.className1.equals(getBaseClassname(this.type)) ) {
			options |= RObjectFactory.O_CLASS_NAME;
		}
		io.writeInt(options);
		io.writeByte(this.type);
		//-- special attributes
		if ((options & RObjectFactory.O_CLASS_NAME) != 0) {
			io.writeString(this.className1);
		}
		//-- data
		if ((io.flags & RObjectFactory.F_ONLY_STRUCT) == 0) {
			io.writeString(this.source);
		}
	}
	
	
	@Override
	public byte getRObjectType() {
		return TYPE_LANGUAGE;
	}
	
	@Override
	public byte getLanguageType() {
		return this.type;
	}
	
	@Override
	public String getRClassName() {
		final String className1= this.className1;
		return (className1 != null) ? className1 : getBaseClassname(this.type);
	}
	
	
	@Override
	public long getLength() {
		return 0;
	}
	
	@Override
	public @Nullable String getSource() {
		return this.source;
	}
	
	@Override
	public @Nullable RStore<?> getData() {
		return null;
	}
	
}
