/*=============================================================================#
 # Copyright (c) 2014, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Apache License, Version 2.0 which is available at
 # https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package de.walware.rj.internal.server.gr.jri;

import static org.eclipse.statet.rj.server.dbg.Srcfiles.equalsTimestamp;
import static org.eclipse.statet.rj.server.dbg.Tracepoints.LOGGER;
import static org.eclipse.statet.rj.server.dbg.Tracepoints.filterPositionsById;
import static org.eclipse.statet.rj.server.dbg.Tracepoints.toLogString;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.LogRecord;

import org.rosuda.JRI.REXP;
import org.rosuda.JRI.Rengine;

import org.eclipse.statet.jcommons.collections.ImCollections;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.RjException;
import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.server.RjsException;
import org.eclipse.statet.rj.server.dbg.DbgListener;
import org.eclipse.statet.rj.server.dbg.ElementTracepoints;
import org.eclipse.statet.rj.server.dbg.Srcref;
import org.eclipse.statet.rj.server.dbg.Tracepoint;
import org.eclipse.statet.rj.server.dbg.TracepointEvent;
import org.eclipse.statet.rj.server.dbg.TracepointPosition;
import org.eclipse.statet.rj.server.dbg.TracepointState;
import org.eclipse.statet.rj.server.rh.AbstractTracepointManager;
import org.eclipse.statet.rj.server.rh.Handle;
import org.eclipse.statet.rj.server.rh.ObjectManager;
import org.eclipse.statet.rj.server.rh.ParsedExpr;
import org.eclipse.statet.rj.server.rh.RhEngine;
import org.eclipse.statet.rj.server.rh.RhEnv;

import de.walware.rj.internal.server.gr.jri.JRISrvRni.RNullPointerException;
import de.walware.rj.internal.server.gr.jri.TracepointManagerOperations.FunInfo;


@NonNullByDefault
public final class TracepointManager extends AbstractTracepointManager {
	
	
	static final int NOT_FOUND= 1;
	static final int FOUND_UNINST= 2;
	static final int FOUND_INST= 3;
	static final int FOUND_INST_UNCHANGED= 3;
	static final int FOUND_INST_NEW= 4;
	
	private static final int[] LOCAL_METHOD_INDEX= new int[] { 2, 3 };
	
	
	private static boolean isLBorFB(final int type) {
		return (type == Tracepoint.TYPE_LB || type == Tracepoint.TYPE_FB);
	}
	
	private static @Nullable TracepointPosition getTBPosition(final ElementTracepoints positions,
			final int[] srcref) {
		final int[] baseSrcref= positions.getElementSrcref();
		for (final TracepointPosition position : positions.getPositions()) {
			final int[] positionSrcref;
			if (position.getType() == Tracepoint.TYPE_TB
					&& (positionSrcref= position.getSrcref()) != null
					&& Srcref.addLine(baseSrcref[Srcref.BEGIN_LINE], positionSrcref[Srcref.BEGIN_LINE]) == srcref[Srcref.BEGIN_LINE]) {
				return position;
			}
		}
		return null;
	}
	
	static final byte[] encodeTracepointIds(final List<? extends TracepointPosition> positions) {
		final int l= positions.size();
		final byte[] raw= new byte[l * 8];
		for (int i= 0; i < l; i++) {
			RDataUtils.encodeLongToRaw(positions.get(i).getId(), raw, i * 8);
		}
		return raw;
	}
	
	static final long @Nullable [] decodeTracepointIds(final byte @Nullable [] raw) {
		if (raw == null) {
			return null;
		}
		final int l= raw.length / 8;
		final long[] ids= new long[l];
		for (int i= 0; i < l; i++) {
			ids[i]= RDataUtils.decodeLongFromRaw(raw, i * 8);
		}
		return ids;
	}
	
	private static final int containsPositionsById(final long[] ids,
			final List<? extends TracepointPosition> requiredIds) {
		final int l= requiredIds.size();
		if (ids.length < l) {
			return 0;
		}
		ITER_REQUIRED: for (int i= 0, j= 0; i < l; i++) {
			final long id= requiredIds.get(i).getId();
			while (j < ids.length) {
				if (ids[j++] == id) {
					continue ITER_REQUIRED;
				}
			}
			return 0;
		}
		return (ids.length == l) ? 2 : 1;
	}
	
	
	final JRISrvDbg dbg;
	final Rengine rEngine;
	private final JRISrvRni rni;
	private final RhEngine rhEngine;
	private final TracepointManagerOperations op;
	
	private @Nullable TracepointState hitBreakpointState;
	private int hitBreakpointFlags;
	private int @Nullable [] hitBreakpointSrcref;
	private long hitBreakpointSrcfile;
	private long hitBreakpointFrameP;
	
	private final long AllMTableSymP;
	
	private final long getTraceExprEnvCallP;
	private final long getRootEvalCallCallP;
	
	
	public TracepointManager(final ObjectManager objManager, final DbgListener listener,
			final JRISrvDbg dbg, final JRISrvRni rni) throws RjsException {
		super(objManager, listener);
		this.dbg= dbg;
		this.rEngine= rni.getREngine();
		this.rni= rni;
		this.rhEngine= rni.getRhEngine();
		this.op= new TracepointManagerOperations(dbg, rni);
		
		this.AllMTableSymP= this.rEngine.rniInstallSymbol(".AllMTable"); //$NON-NLS-1$
		
		this.getRootEvalCallCallP= this.rni.checkAndPreserve(this.dbg.createSysCallCall(-3));
		this.getTraceExprEnvCallP= this.rni.checkAndPreserve(this.dbg.createSysFrameCall(-1));
	}
	
	
	void registerEnv(final long argsP) {
		final long[] argValuesP;
		if (argsP == 0
				|| (argValuesP= this.rEngine.rniGetVector(argsP)) == null || argValuesP.length < 3) {
			return;
		}
		
		final String key= this.rEngine.rniGetString(argValuesP[0]);
		final long envP= argValuesP[1];
		if (key != null && this.rEngine.rniExpType(envP) == REXP.ENVSXP) {
			final String[] objectNames= this.rEngine.rniGetStringArray(argValuesP[2]);
			
			registerEnv(key, new Handle(envP),
					(objectNames != null) ? ImCollections.newList(objectNames) : null );
		}
	}
	
	void unregisterEnvs(final long argsP) {
		final long[] argValuesP;
		if (argsP == 0
				|| (argValuesP= this.rEngine.rniGetVector(argsP)) == null || argValuesP.length < 1) {
			return;
		}
		
		final String key= this.rEngine.rniGetString(argValuesP[0]);
		if (key != null) {
			unregisterEnvs(key);
		}
	}
	
	void updateInst(final long argsP) {
		final long[] argValuesP;
		if (argsP == 0
				|| (argValuesP= this.rEngine.rniGetVector(argsP)) == null || argValuesP.length < 2) {
			return;
		}
		
		final long envP= argValuesP[0];
		if (this.rEngine.rniExpType(envP) == REXP.ENVSXP) {
			final String[] objectNames= this.rEngine.rniGetStringArray(argValuesP[1]);
			
			updateInst(this.objManager.getEnv(new Handle(envP)),
					(objectNames != null) ? ImCollections.newList(objectNames) : null );
		}
	}
	
	
	@Override
	protected void performUpdatingInstEB(final int flags) {
		try {
			if ((flags & TracepointState.FLAG_ENABLED) != 0) {
				final long handlerP= this.op.createExceptionHandler();
				if (this.rni.setOption(this.rni.error_SymP, handlerP)) {
					notifyListeners(new TracepointEvent(TracepointEvent.KIND_INSTALLED,
							Tracepoint.TYPE_EB, TracepointState.EB_FILEPATH, 0, null, null, 0 ));
				}
			}
			else {
				if (this.rni.setOption(this.rni.error_SymP, this.rni.NULL_P)) {
					notifyListeners(new TracepointEvent(TracepointEvent.KIND_UNINSTALLED,
							Tracepoint.TYPE_EB, TracepointState.EB_FILEPATH, 0, null, null, 0 ));
				}
			}
		}
		catch (final Exception e) {
			final LogRecord record= new LogRecord(Level.SEVERE,
					"An error occurred when setting dbg error handler.");
			record.setThrown(e);
			JRISrvErrors.LOGGER.log(record);
		}
	}
	
//	/**
//	 * Installs the specified tracepoints
//	 * 
//	 * @param request with elements and their tracepoints
//	 * @return a report
//	 * @throws RjsException
//	 */
//	public TracepointInstallationReport installTracepoints(final ElementTracepointInstallationRequest request)
//			throws RjsException {
//		final List<? extends ElementTracepoints> elementList= request.getRequests();
//		final int[] resultCodes= new int[elementList.size()];
//		Arrays.fill(resultCodes, TracepointInstallationReport.NOTFOUND);
//		
//		addTracepoints(elementList);
//		{	boolean installRequired= false;
//			for (int elementIdx= 0; elementIdx < elementList.size(); elementIdx++) {
//				final ElementTracepoints tracepoints= elementList.get(elementIdx);
//				if (!tracepoints.getElementId().startsWith(TOPLEVEL_ELEMENT_ID)) {
//					installRequired= true;
//					break;
//				}
//			}
//			if (!installRequired) {
//				return new TracepointInstallationReport(resultCodes);
//			}
//		}
//		
//		this.objManager.update();
//		final List<RhEnv> envs= new ArrayList<>(64);
//		envs.addAll(this.objManager.getSearchPath());
//		for (final RhEnv env : this.objManager.getStackFrames()) {
//			addEnvWithParents(envs, env);
//		}
//		addRegisteredEnvs(envs);
//		
//		while (!envs.isEmpty()) {
//			final RhEnv env= envs.remove(0);
//			final long envP= env.handle.p;
//			
//			if (this.rni.isInternEnv(envP)) {
//				continue;
//			}
//			
//			final int savedProtected= this.rni.saveProtected();
//			final long namesStrP= this.rni.protect(this.rEngine.rniListEnv(envP, true));
//			final String[] names= this.rEngine.rniGetStringArray(namesStrP);
//			for (int namesIdx= 0; namesIdx < names.length; namesIdx++) {
//				final String name= names[namesIdx];
//				if (name == null
//						|| ((envP == this.rni.Base_EnvP || envP == this.rni.BaseNamespace_EnvP)
//								&& name.equals(".Last.value") )) { //$NON-NLS-1$
//					continue;
//				}
//				final long funP;
//				final long nameSymP= this.rEngine.rniInstallSymbolByStr(namesStrP, namesIdx);
//				{	long p= this.rEngine.rniGetVarBySym(nameSymP, envP, 0);
//					if (p == 0) {
//						continue;
//					}
//					int type= this.rEngine.rniExpType(p);
//					if (type == REXP.PROMSXP) {
//						p= this.rEngine.rniGetPromise(p, 1);
//						type= this.rEngine.rniExpType(p);
//					}
//					if (type != REXP.CLOSXP) {
//						continue;
//					}
//					funP= p;
//				}
//				final long orgFunP= this.dbg.getOrgFun(funP);
//				if (orgFunP == 0) {
//					continue;
//				}
//				
//				FunInfo commonInfo= new FunInfo(orgFunP, funP, null);
//				if (!preCheck(commonInfo)) {
//					commonInfo= null;
//				}
//				
//				final ObjectInEnv funRef= new ObjectInEnv(env, name,
//						this.rni.protect(this.rEngine.rniPutString(name) ));
//				
//				// method
//				List<FunInfo> methodInfos= null;
//				try {
//					final long nameEnvP= checkGeneric(orgFunP, funRef);
//					
//					if (nameEnvP != 0) {
//						final String[] signatures= this.rEngine.rniGetStringArray(
//								this.rEngine.rniListEnv(nameEnvP, true) );
//						methodInfos= new ArrayList<>(signatures.length);
//						for (int signarturesIdx= 0; signarturesIdx < signatures.length; signarturesIdx++) {
//							try {
//								final long methodP;
//								{	long p= this.rEngine.rniGetVar(signatures[signarturesIdx], nameEnvP);
//									int type= this.rEngine.rniExpType(p);
//									if (type == REXP.PROMSXP) {
//										p= this.rEngine.rniGetPromise(p, 1);
//										type= this.rEngine.rniExpType(p);
//									}
//									if (type != REXP.CLOSXP) {
//										continue;
//									}
//									methodP= p;
//								}
//								final long mainOrgMethodP= this.dbg.getOrgFun(methodP);
//								if (mainOrgMethodP == 0) {
//									continue;
//								}
//								final FunInfo methodInfo= new FunInfo(mainOrgMethodP,
//										methodP, signatures[signarturesIdx] );
//								if (preCheck(methodInfo)) {
//									methodInfos.add(methodInfo);
//								}
//								
//								long localMethodP= this.rEngine.rniGetCloBodyExpr(mainOrgMethodP);
//								if (this.rEngine.rniExpType(localMethodP) != REXP.LANGSXP
//										|| this.rEngine.rniGetLength(localMethodP) < 3
//										|| this.rEngine.rniCAR(localMethodP) != this.rni.Block_SymP) {
//									continue;
//								}
//								localMethodP= this.rEngine.rniCAR(this.rEngine.rniCDR(localMethodP));
//								if (this.rEngine.rniExpType(localMethodP) != REXP.LANGSXP
//										|| this.rEngine.rniGetLength(localMethodP) < 3
//										|| this.rEngine.rniCAR(localMethodP) != this.rni.Assign_SymP) {
//									continue;
//								}
//								localMethodP= this.rEngine.rniCAR(this.rEngine.rniCDR(this.rEngine.rniCDR(localMethodP)));
//								if (this.rEngine.rniExpType(localMethodP) != REXP.CLOSXP) {
//									continue;
//								}
//								{	final FunInfo subInfo= new FunInfo(localMethodP, LOCAL_METHOD_INDEX,
//											methodInfo );
//									if (preCheck(subInfo)) {
//										methodInfos.add(subInfo);
//									}
//								}
//							}
//							catch (final Exception e) {
//								final LogRecord record= new LogRecord(Level.SEVERE,
//										"Method check failed for function ''{0}({1})'' in ''0x{2}''.");
//								record.setParameters(new Object[] { name, signatures[signarturesIdx], Long.toHexString(envP) });
//								record.setThrown(e);
//								JRISrvErrors.LOGGER.log(record);
//							}
//						}
//					}
//				}
//				catch (final Exception e) {
//					final LogRecord record= new LogRecord(Level.SEVERE,
//							"Generic check failed for function ''{0}'' in ''0x{1}''.");
//					record.setParameters(new Object[] { name, Long.toHexString(envP) });
//					record.setThrown(e);
//					JRISrvErrors.LOGGER.log(record);
//				}
//				
//				for (int elementIdx= 0; elementIdx < elementList.size(); elementIdx++) {
//					final ElementTracepoints elementTracepoints= elementList.get(elementIdx);
//					if (elementTracepoints.getElementId().startsWith(TOPLEVEL_ELEMENT_ID)) {
//						continue;
//					}
//					if (commonInfo != null && commonInfo.done[0] < FunInfo.DONE_SET) {
//						final int funSet= this.op.trySetTracepoints(elementList.get(elementIdx),
//								commonInfo, funRef );
//						if (funSet > resultCodes[elementIdx]) {
//							resultCodes[elementIdx]= funSet;
//							continue;
//						}
//					}
//					if (methodInfos != null) {
//						for (int methodIdx= 0; methodIdx < methodInfos.size(); methodIdx++) {
//							final FunInfo methodInfo= methodInfos.get(methodIdx);
//							if (methodInfo.done[0] < FunInfo.DONE_SET) {
//								final int methodSet= this.op.trySetTracepoints(elementTracepoints,
//										methodInfo, funRef );
//								if (methodSet > resultCodes[elementIdx]) {
//									resultCodes[elementIdx]= methodSet;
//								}
//							}
//						}
//					}
//				}
//			}
//			this.rni.looseProtected(savedProtected);
//		}
//		
//		if (LOGGER.isLoggable(Level.FINER)) {
//			final StringBuilder sb= new StringBuilder("Dbg: installTracepoints"); //$NON-NLS-1$
//			for (int i= 0; i < resultCodes.length; i++) {
//				sb.append('\n').append(i).append("."); //$NON-NLS-1$
//				sb.append(" --> ").append(resultCodes[i]); //$NON-NLS-1$
//				sb.append('\n').append(elementList.get(i));
//			}
//			LOGGER.log(Level.FINER, sb.toString());
//		}
//		
//		return new TracepointInstallationReport(resultCodes);
//	}
	
	@Override
	protected void performUpdatingInst(final RhEnv env, final @Nullable List<String> objectNames) {
		final long envP= env.handle.p;
		
		if (env.isDisposed() || this.rni.isInternEnv(envP)
				|| (objectNames != null && objectNames.isEmpty()) ) {
			return;
		}
		
		final int savedProtected= this.rni.saveProtected();
		this.rni.protect(envP);
		
		beginUpdatingInstEnv(env, objectNames);
		
		final long envNamesStrP= this.rni.protect(this.rEngine.rniListEnv(envP, true));
		final String[] envNames= this.rEngine.rniGetStringArray(envNamesStrP);
		for (int envNamesIdx= 0; envNamesIdx < envNames.length; envNamesIdx++) {
			final String name= envNames[envNamesIdx];
			if (name == null
					|| ((envP == this.rni.Base_EnvP || envP == this.rni.BaseNamespace_EnvP)
							&& name.equals(".Last.value") )
					|| (objectNames != null && !objectNames.contains(name)) ) {
				continue;
			}
			final long funP;
			{	long p= this.rEngine.rniGetVarBySym(
						this.rEngine.rniInstallSymbolByStr(envNamesStrP, envNamesIdx), envP,
						0 );
				if (p == 0) {
					continue;
				}
				int type= this.rEngine.rniExpType(p);
				if (type == REXP.PROMSXP) {
					p= this.rEngine.rniGetPromise(p, 1);
					type= this.rEngine.rniExpType(p);
				}
				if (type != REXP.CLOSXP) {
					continue;
				}
				funP= p;
			}
			final long orgFunP= this.dbg.getOrgFun(funP);
			if (orgFunP == 0) {
				continue;
			}
			
			FunInfo commonInfo= new FunInfo(orgFunP, funP, null);
			if (!preCheck(commonInfo)) {
				commonInfo= null;
			}
			
			final ObjectInEnv funRef= new ObjectInEnv(env, name,
					this.rni.protect(this.rEngine.rniPutString(name) ));
			
			// method
			List<FunInfo> methodInfos= null;
			try {
				final long nameEnvP= checkGeneric(orgFunP, funRef);
				
				if (nameEnvP != 0) {
					final @Nullable String [] signatures= this.rEngine.rniGetStringArray(
							this.rEngine.rniListEnv(nameEnvP, true) );
					methodInfos= new ArrayList<>(signatures.length);
					for (int signarturesIdx= 0; signarturesIdx < signatures.length; signarturesIdx++) {
						try {
							final long methodP;
							{	long p= this.rEngine.rniGetVar(signatures[signarturesIdx], nameEnvP);
								int type= this.rEngine.rniExpType(p);
								if (type == REXP.PROMSXP) {
									p= this.rEngine.rniGetPromise(p, 1);
									type= this.rEngine.rniExpType(p);
								}
								if (type != REXP.CLOSXP) {
									continue;
								}
								methodP= p;
							}
							final long mainOrgMethodP= this.dbg.getOrgFun(methodP);
							if (mainOrgMethodP == 0) {
								continue;
							}
							final FunInfo methodInfo= new FunInfo(mainOrgMethodP,
									methodP, signatures[signarturesIdx] );
							if (preCheck(methodInfo)) {
								methodInfos.add(methodInfo);
							}
							
							long localMethodP= this.rEngine.rniGetCloBodyExpr(mainOrgMethodP);
							if (this.rEngine.rniExpType(localMethodP) != REXP.LANGSXP
									|| this.rEngine.rniGetLengthLong(localMethodP) < 3
									|| this.rEngine.rniCAR(localMethodP) != this.rni.Block_SymP) {
								continue;
							}
							localMethodP= this.rEngine.rniCAR(this.rEngine.rniCDR(localMethodP));
							if (this.rEngine.rniExpType(localMethodP) != REXP.LANGSXP
									|| this.rEngine.rniGetLengthLong(localMethodP) < 3
									|| this.rEngine.rniCAR(localMethodP) != this.rni.Assign_SymP) {
								continue;
							}
							localMethodP= this.rEngine.rniCAR(this.rEngine.rniCDR(this.rEngine.rniCDR(localMethodP)));
							if (this.rEngine.rniExpType(localMethodP) != REXP.CLOSXP) {
								continue;
							}
							{	final FunInfo subInfo= new FunInfo(localMethodP, LOCAL_METHOD_INDEX,
										methodInfo );
								if (preCheck(subInfo)) {
									methodInfos.add(subInfo);
								}
							}
						}
						catch (final Exception e) {
							final LogRecord record= new LogRecord(Level.SEVERE,
									"Method check failed for function ''{0}({1})'' in ''0x{2}''.");
							record.setParameters(new Object[] {
									name,
									signatures[signarturesIdx],
									Long.toHexString(envP) });
							record.setThrown(e);
							JRISrvErrors.LOGGER.log(record);
						}
					}
				}
			}
			catch (final Exception e) {
				final LogRecord record= new LogRecord(Level.SEVERE,
						"Generic check failed for function ''{0}'' in ''0x{1}''.");
				record.setParameters(new Object[] {
						name,
						Long.toHexString(envP) });
				record.setThrown(e);
				JRISrvErrors.LOGGER.log(record);
			}
			
			if (commonInfo != null && commonInfo.done[0] < FunInfo.DONE_SET) {
				doUpdateTracepoints(funRef, commonInfo);
			}
			if (methodInfos != null) {
				for (int methodIdx= 0; methodIdx < methodInfos.size(); methodIdx++) {
					final FunInfo methodInfo= methodInfos.get(methodIdx);
					doUpdateTracepoints(funRef, methodInfo);
				}
			}
		}
		
		finishUpdatingInstEnv();
		
		this.rni.looseProtected(savedProtected);
	}
	
	private void doUpdateTracepoints(final ObjectInEnv funRef, final FunInfo funInfo) {
		ElementTracepointsCollection collection= null;
		final PathEntry pathEntry= getPathEntry(funInfo);
		if (pathEntry != null) {
			collection= findTracepoints(pathEntry, funInfo);
		}
		
		int result= NOT_FOUND;
		if (collection == null
				|| collection.isActualPositionsEmpty()
				|| collection.getList().isEmpty() ) {
			if (funInfo.currentMainFunP == funInfo.orgMainFunP
					|| collection != null && collection.getCurrent() == NO_CHANGE_ELEMENT_TRACEPOINTS) {
				return; // nothing to do
			}
			if (funInfo.currentMainBodyP != 0
					&& this.rEngine.rniGetAttrBySym(funInfo.currentMainBodyP, this.dbg.dbgElementId_SymP) == 0) {
				if (LOGGER.isLoggable(Level.FINER)) {
					LOGGER.log(Level.FINER,
							"[TP] Updating element tracepoints for {0}: SKIP (not RJ).",
							funInfo.getLabel(funRef) );
				}
				return; // not set by us
			}
			
			final List<? extends TracepointPosition> positions= NO_POSITIONS;
			
			if (LOGGER.isLoggable(Level.FINER)) {
				LOGGER.log(Level.FINER,
						"[TP] Updating element tracepoints for {0}: UNINSTALL.",
						funInfo.getLabel(funRef) );
			}
			
			result= this.op.trySetTracepoints(funInfo, funRef, positions, null);
		}
		else {
			int currInstTracepointMatch= 0; // 0= no, 1= contains, 2= exact
			long currInstTimestamp= 0;
			long[] currInstTracepointIds= null;
			if (funInfo.currentMainBodyP != 0 && funInfo.currentMainFunP != funInfo.orgMainFunP) {
				long p;
				if ((p= this.rEngine.rniGetAttrBySym(funInfo.currentMainBodyP, this.dbg.dbgElementId_SymP)) != 0) {
					currInstTimestamp= this.dbg.getTimestampAttr(p);
				}
				if ((p= this.rEngine.rniGetAttrBySym(funInfo.currentMainBodyP, this.dbg.dbgTracepointIds_SymP)) != 0
						&& (currInstTracepointIds= decodeTracepointIds(this.rEngine.rniGetRawArray(p))) != null ) {
					currInstTracepointMatch= containsPositionsById(currInstTracepointIds,
							collection.getCurrent().getPositions() );
				}
			}
			
			for (final Iterator<ElementTracepoints> iter= collection.getList().iterator();
					result < FOUND_UNINST && iter.hasNext();) {
				final ElementTracepoints cand= iter.next();
				if (currInstTracepointMatch > 0 && currInstTimestamp != 0) {
					final long candTimestamp= cand.getSrcfile().getTimestamp();
					if (equalsTimestamp(currInstTimestamp, candTimestamp)
							&& currInstTracepointMatch == 2) { // currInst == cand
						result= FOUND_INST_UNCHANGED;
					}
					else if (collection.getRequestedTimestamp() != 0
							&& equalsTimestamp(currInstTimestamp, collection.getRequestedTimestamp())
							&& (currInstTracepointMatch == 2 // currInst already perfect
									|| !equalsTimestamp(candTimestamp, collection.getRequestedTimestamp()) // new is worse
									)) {
						result= FOUND_INST_UNCHANGED;
					}
				}
				if (result < FOUND_INST) {
					final List<? extends TracepointPosition> positions= collection.getActualPositions(
							cand.getPositions() );
					
					if (LOGGER.isLoggable(Level.FINER)) {
						LOGGER.log(Level.FINER,
								"[TP] Updating element tracepoints for {0}: INSTALL." +
										"\n\tpath= {1}" +
										"\n\telementId= {2}" +
										"\n\ttracepoints= {3}",
								new Object[] {
										funInfo.getLabel(funRef),
										cand.getSrcfile().getPath(),
										cand.getElementId(),
										toLogString(positions) } );
					}
					
					result= this.op.trySetTracepoints(funInfo, funRef, positions, cand);
				}
				else { // changed to FOUND_INST_UNCHANGED
					if (LOGGER.isLoggable(Level.FINER)) {
						LOGGER.log(Level.FINER,
								"[TP] Updating element tracepoints for {0}: SKIP (already installed)." +
										"\n\tpath= {1}" +
										"\n\telementId= {2}",
								new Object[] {
										funInfo.getLabel(funRef),
										cand.getSrcfile().getPath(),
										cand.getElementId() } );
					}
				}
			}
			
			if (result == NOT_FOUND && getClearUpdatingInst()) {
				if (currInstTracepointMatch > 0) {
					result= FOUND_INST_UNCHANGED;
				}
				else if (currInstTracepointIds != null) { // old subset
					final List<TracepointPosition> positions= filterPositionsById(
							collection.getCurrent().getPositions(), currInstTracepointIds );
					if (!positions.isEmpty()) {
						final ElementTracepoints cand= new ElementTracepoints(
								collection.getCurrent().getSrcfile(),
								collection.getCurrent().getElementId(), null,
								positions );
						result= FOUND_INST_UNCHANGED;
						collection= new ElementTracepointsCollection(Long.MIN_VALUE, cand, cand, 0);
					}
				}
			}
			if (result == FOUND_INST_NEW
					|| (result >= FOUND_INST && getClearUpdatingInst()) ) {
				pathEntry.addInstalled(collection);
			}
		}
	}
	
	private @Nullable PathEntry getPathEntry(final FunInfo funInfo) {
		switch (funInfo.fileType) {
		case FunInfo.FILE_PATH:
			return getPathEntry(funInfo.file);
		case FunInfo.FILE_NAME:
			return findPathEntryByFileName(funInfo.file);
		default:
			return null;
		}
	}
	
	private @Nullable ElementTracepointsCollection findTracepoints(final PathEntry pathEntry,
			final FunInfo funInfo) {
		final String elementId= this.rEngine.rniGetAttrStringBySym(funInfo.orgBodyP, this.rni.appElementId_SymP);
		final long funTimestamp= this.dbg.getFileTimestamp(funInfo.srcfileP);
		
		final ElementTracepointsCollection collection= pathEntry.findCurrentElementTracepoints(elementId,
				funTimestamp, funInfo.getBodySrcref() );
//		if (tracepoints == null) {
//			if (funInfo.currentMainFunP != funInfo.orgMainFunP
//					&& elementId == null
//					&& (elementId= this.rEngine.rniGetAttrStringBySym(funInfo.currentMainFunP, this.rni.dbgElementId_SymP)) != null) {
//				tracepoints= pathEntry.findCurrentElementPositions(elementId,
//						funTimestamp, funInfo.baseSrcref );
		return collection;
	}
	
	private long checkGeneric(final long funP, final ObjectInEnv funRef) throws Exception {
		long nameEnvP= this.rEngine.rniGetCloEnv(funP);
		if (nameEnvP == 0 || this.rEngine.rniExpType(nameEnvP) != REXP.ENVSXP) {
			return 0;
		}
		nameEnvP= this.rEngine.rniGetVarBySym(this.AllMTableSymP, nameEnvP, 0);
		if (nameEnvP == 0 || this.rEngine.rniExpType(nameEnvP) != REXP.ENVSXP) {
			return 0;
		}
		
		final long p= this.rEngine.rniEval(this.rEngine.rniCons(
						this.rni.isGeneric_SymP, this.rEngine.rniCons(
								funRef.getNameStrP(), this.rEngine.rniCons(
										funRef.getEnvP(), this.rEngine.rniCons(
												funP, this.rni.NULL_P,
												this.rni.fdef_SymP, false ),
										this.rni.where_SymP, false ),
								0, false ),
						0, true ),
				this.rni.rniSafeGlobalExecEnvP );
		return (p != 0 && this.rEngine.rniIsTrue(p)) ? nameEnvP : 0;
	}
	
	private boolean preCheck(final FunInfo funInfo) {
		if ((funInfo.orgBodyP= this.rEngine.rniGetCloBodyExpr(funInfo.orgFunP)) == 0) {
			return false;
		}
		if ((funInfo.srcfileP= this.dbg.getSrcfileEnvP(funInfo.orgBodyP)) == 0) {
			return false;
		}
		if ((funInfo.file= this.dbg.getFilePath(funInfo.srcfileP, 0)) != null) {
			funInfo.fileType= FunInfo.FILE_PATH;
		}
		else if ((funInfo.file= this.dbg.getFileName(funInfo.srcfileP)) != null) {
			funInfo.fileType= FunInfo.FILE_NAME;
		}
		else {
			funInfo.fileType= FunInfo.INVALID;
			return false;
		}
		
		{	final long p= this.rEngine.rniGetAttrBySym(funInfo.orgBodyP, this.dbg.srcref_SymP);
			if (p != 0) {
				funInfo.startSrcref= this.dbg.getSrcrefObject(this.rEngine.rniGetVectorElt(p, 0));
			}
		}
		{	final long p= this.rEngine.rniGetAttrBySym(funInfo.orgBodyP, this.dbg.wholeSrcref_SymP);
			if (p != 0) {
				funInfo.wholeSrcref= this.dbg.getSrcrefObject(p);
			}
		}
		if (funInfo.currentMainFunP != 0 && funInfo.currentMainFunP != funInfo.orgMainFunP) {
			funInfo.currentMainBodyP= this.rEngine.rniGetCloBodyExpr(funInfo.currentMainFunP);
		}
		
		return true;
	}
	
	
	long checkLBandFB(final long callP) throws RjException {
		if (!isAnyTracepointEnabled() || !isBreakpointsEnabled()) {
			return 0;
		}
		final long srcrefP= this.rEngine.rniGetAttrBySym(callP, this.dbg.srcref_SymP);
		if (srcrefP == 0) {
			throw new RjException("Missing data: srcref.");
		}
		final long srcfileP= this.dbg.getSrcfileEnvP(srcrefP);
		if (srcfileP == 0) {
			throw new RjException("Missing data: srcfile env of srcref.");
		}
		final String filePath= this.dbg.getFilePath(srcfileP, srcrefP);
		if (filePath == null) {
			throw new RjException("Missing data: path of srcref.");
		}
		final long idP= this.rEngine.rniGetAttrBySym(srcrefP, this.rni.id_SymP);
		final long atP= this.rEngine.rniGetAttrBySym(srcrefP, this.rni.at_SymP);
		if (idP == 0 || atP == 0 ) {
			throw new RjException("Missing data: id/position.");
		}
		final long id= RDataUtils.decodeLongFromRaw(this.rEngine.rniGetRawArray(idP));
		StateEntry stateEntry= null;
		final PathEntry pathEntry= getPathEntry(filePath);
		if (pathEntry != null) {
			synchronized (pathEntry.getStates()) {
				stateEntry= getBreakpointState(pathEntry.getStates(), id);
				if (stateEntry == null) {
					stateEntry= getBreakpointState(pathEntry.getStates(),
							this.rEngine.rniGetAttrStringBySym(srcrefP, this.dbg.dbgElementId_SymP),
							this.rEngine.rniGetIntArray(atP) );
				}
			}
		}
		final TracepointState state;
		if (stateEntry == null
				|| (state= stateEntry.getState()) == null
				|| !isLBorFB(state.getType()) ) {
			LOGGER.log(Level.FINE, "Skipping breakpoint because of missing state.");
			return 0;
		}
		if (!state.isEnabled()) {
//			LOGGER.log(Level.FINER, "Skipping breakpoint because it is disabled.");
			return 0;
		}
		
		this.hitBreakpointState= null;
		this.hitBreakpointSrcref= getSrcrefObject(srcrefP);
		if (this.hitBreakpointSrcref == null) {
			throw new RjException("Missing data: srcref values.");
		}
		if (!isBreakpointsEnabled()) {
			return 0;
		}
		
		int codeFlags= 0;
		{	// load flags
			final long p= this.rEngine.rniGetAttrBySym(srcrefP, this.rni.flags_SymP);
			if (p != 0) {
				codeFlags= this.rEngine.rniGetIntArray(p)[0];
			}
		}
		int flags= 0;
		{	// check flags
			final int stateFlags= state.getFlags();
			if (state.getType() == Tracepoint.TYPE_FB) {
				if ((codeFlags & stateFlags & (TracepointState.FLAG_MB_ENTRY | TracepointState.FLAG_MB_EXIT)) == 0) {
//					LOGGER.log(Level.FINER, "Skipping breakpoint because current position is disabled.");
					return 0;
				}
			}
			flags |= (codeFlags & 0x00FF0000);
		}
		if (state.getExpr() != null) { // check expr
			final long exprP= getTracepointEvalExpr(state.getExpr(), stateEntry);
			if (exprP == 0) {
//				LOGGER.log(Level.WARNING, "Creating expression for breakpoint condition failed.");
				return 0;
			}
			
			this.dbg.beginSafeMode();
			try {
				final long envP= this.rni.createNewEnv(this.getTraceExprEnvCallP);
				if (envP == 0) {
					LOGGER.log(Level.SEVERE, "Creating environment for breakpoint condition failed.");
					return 0;
				}
				this.rni.protect(envP);
				final long p= this.rni.evalExpr(exprP, envP, 1);
				if (!this.rEngine.rniIsTrue(p)) {
					return 0;
				}
			}
			catch (final RjsException e) {
//				LOGGER.log(Level.FINER, "Skipping breakpoint because evaluating the condition failed.", e);
				// evaluation failed (expression invalid...)
//				TODO notify ?
				return 0;
			}
			finally {
				this.dbg.endSafeMode();
			}
		}
		
		try {
			return this.op.createBreakpointSuspend(codeFlags, this.hitBreakpointSrcref);
		}
		catch (final RNullPointerException e) {
			LOGGER.log(Level.WARNING, "Failed to create browser expression.", e);
			return this.op.createFallbackSuspend();
		}
		finally {
			this.hitBreakpointState= state;
			this.hitBreakpointFlags= flags;
			this.hitBreakpointSrcfile= srcfileP;
			this.hitBreakpointFrameP= 0;
		}
	}
	
	long checkTB(final long argsP) {
		final long[] argValuesP;
		if (!isAnyTracepointEnabled()
				|| argsP == 0
				|| (argValuesP= this.rEngine.rniGetVector(argsP)) == null || argValuesP.length < 2) {
			return 0;
		}
		
		final long srcrefP= this.dbg.getExpr0SrcrefP(argValuesP[0]);
		if (srcrefP == 0) {
			return 0;
		}
		final long srcfileP= this.dbg.getSrcfileEnvP(srcrefP);
		if (srcfileP == 0) {
			return 0;
		}
		
		PathEntry pathEntry= null;
		{	String s;
			if ((s= this.dbg.getFilePath(srcfileP, srcrefP)) != null) {
				pathEntry= getPathEntry(s);
			}
			else if ((s= this.dbg.getFileName(srcfileP)) != null) {
				pathEntry= findPathEntryByFileName(s);
			}
		}
		if (pathEntry == null) {
			return 0;
		}
		final ElementTracepoints tracepoints= pathEntry.findElementTracepoints(
				TOPLEVEL_ELEMENT_ID, this.dbg.getFileTimestamp(srcfileP) );
		if (tracepoints == null
				|| tracepoints.getPositions().isEmpty() ) {
			return 0;
		}
		
		final long frameP= argValuesP[1];
		if (this.rEngine.rniExpType(frameP) != REXP.ENVSXP) {
			return 0;
		}
		
		this.hitBreakpointState= null;
		this.hitBreakpointSrcref= getSrcrefObject(srcrefP);
		if (this.hitBreakpointSrcref == null) {
			return 0;
		}
		
		final TracepointPosition position= getTBPosition(tracepoints, this.hitBreakpointSrcref);
		if (position == null) {
			return 0;
		}
		
		final StateEntry stateEntry= pathEntry.getBreakpointState(position.getId());
		final TracepointState state;
		if (stateEntry == null
				|| (state= stateEntry.getState()) == null
				|| state.getType() != Tracepoint.TYPE_TB ) {
			LOGGER.log(Level.FINE, "Skipping breakpoint because of missing state.");
			return 0;
		}
		if (!state.isEnabled()) {
			LOGGER.log(Level.FINER, "Skipping breakpoint because it is disabled.");
			return 0;
		}
		
		this.rEngine.rniSetDebug(frameP, 1);
		
		this.hitBreakpointState= state;
		this.hitBreakpointFlags= 0;
		this.hitBreakpointSrcfile= srcfileP;
		this.hitBreakpointFrameP= frameP;
		
		return 0;
	}
	
	long checkEB(final long argsP) {
		this.dbg.beginSafeMode();
		try {
			final int nFrame= this.dbg.getNFrame();
			if (nFrame <= 1) {
				return 0;
			}
			final int suspendedNFrame= this.dbg.getSuspendedNFrame();
			final long suspendedFrame= this.dbg.getSuspendedFrame();
			final long frameP= this.rEngine.rniEval(this.getTraceExprEnvCallP,
					this.rni.rniSafeBaseExecEnvP );
			if (frameP != 0) {
				if (nFrame - 1 == suspendedNFrame && frameP == suspendedFrame) {
					return 0;
				}
				if ((nFrame == 4 && frameP == this.rni.Global_EnvP)
						|| (nFrame - 4 == suspendedFrame && frameP == suspendedFrame) ) {
					final long callP= this.rEngine.rniEval(this.getRootEvalCallCallP,
							this.rni.rniSafeGlobalExecEnvP );
					if (callP != 0) {
						final String[] call= this.rni.getSourceLines(callP);
						if (call != null && call.length > 0 && call[0].startsWith("rj:::")) {
							return 0;
						}
					}
				}
			}
			
			TracepointState state= null;
			final int flags= 0;
			final PathEntry pathEntry= getPathEntry(TracepointState.EB_FILEPATH);
			if (pathEntry != null) {
				synchronized (pathEntry.getStates()) {
					for (final StateEntry entry : pathEntry.getStates()) {
						final TracepointState current= entry.getState();
						if (current != null
								&& current.getElementId().equals("*")) {
							state= current;
							break;
						}
					}
				}
			}
			if (state == null) {
				LOGGER.log(Level.FINE, "Skipping exception because of missing state.");
				return 0;
			}
			if (!state.isEnabled()) {
				return 0;
			}
			
			try {
				return this.op.createExceptionSuspend();
			}
			catch (final RNullPointerException e) {
				LOGGER.log(Level.WARNING, "Failed to create browser expression.", e);
				return this.op.createFallbackSuspend();
			}
			finally {
				this.hitBreakpointState= state;
				this.hitBreakpointFlags= flags;
				this.hitBreakpointSrcfile= 0;
				this.hitBreakpointSrcref= null;
				this.hitBreakpointFrameP= frameP;
			}
		}
		finally {
			this.dbg.endSafeMode();
		}
	}
	
	private int @Nullable [] getSrcrefObject(final long srcrefP) {
		final int[] array= this.rEngine.rniGetIntArray(srcrefP);
		if (array != null && array.length >= 6) {
			return array;
		}
		return null;
	}
	
	private long getTracepointEvalExpr(final @Nullable String expr, final StateEntry entry) {
		if (expr == null) {
			return 0;
		}
		final ParsedExpr parsedExpr= entry.getParsedExpr(expr);
		Handle handle= parsedExpr.getParsedExpr();
		if (handle == null
				&& (parsedExpr.getFlags() & TracepointState.FLAG_EXPR_INVALID) == 0) {
			long p= this.rEngine.rniParse("{\n" + expr + "\n}", 1); //$NON-NLS-1$ //$NON-NLS-2$
			if (p != 0) {
				p= this.rEngine.rniGetVectorElt(p, 0);
				if (p != 0) {
					handle= new Handle(p);
				}
			}
			parsedExpr.setParsedExpr(handle, this.rhEngine);
			if (handle == null) {
//				sendNotification(notification);
			}
		}
		if (handle != null) {
			this.rni.protect(handle.p);
			return handle.p;
		}
		else {
			return 0;
		}
	}
	
	
	void handleSuspended(final long srcrefP) {
		final TracepointState state= this.hitBreakpointState;
		if (state != null) {
			this.hitBreakpointState= null;
			if (state.getType() == Tracepoint.TYPE_EB) {
				notifyListeners(new TracepointEvent(TracepointEvent.KIND_ABOUT_TO_HIT,
						state.getType(), state.getFilePath(), state.getId(),
						state.getElementId(), null,
						this.hitBreakpointFlags ));
			}
			else if (srcrefP != 0 && Arrays.equals(this.hitBreakpointSrcref, this.rEngine.rniGetIntArray(srcrefP))) { // compare srcfile?
				notifyListeners(new TracepointEvent(TracepointEvent.KIND_ABOUT_TO_HIT,
						state.getType(), state.getFilePath(), state.getId(),
						state.getElementId(), state.getElementLabel(),
						this.hitBreakpointFlags ));
			}
		}
	}
	
	void handleCancelled() {
		this.hitBreakpointState= null;
	}
	
}
