/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Apache License, Version 2.0 which is available at
 # https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package de.walware.rj.internal.server.gr.jri;

import java.io.IOException;
import java.util.Arrays;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RArray;
import org.eclipse.statet.rj.data.RCharacterStore;
import org.eclipse.statet.rj.data.RDataUtils;
import org.eclipse.statet.rj.data.RIntegerStore;
import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RList;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RObjectFactory;
import org.eclipse.statet.rj.data.RStore;
import org.eclipse.statet.rj.data.impl.AbstractRObject;
import org.eclipse.statet.rj.data.impl.ExternalizableRObject;
import org.eclipse.statet.rj.data.impl.ExternalizableRStore;
import org.eclipse.statet.rj.data.impl.RCharacter32Store;
import org.eclipse.statet.rj.data.impl.SimpleRList;


@NonNullByDefault
public class JRIArrayImpl<TData extends RStore<?>> extends AbstractRObject
		implements RArray<TData>, ExternalizableRObject {
	
	
	private long length;
	private TData data;
	
	private @Nullable String className1;
	private int[] dimAttribute;
	private @Nullable SimpleRList<? extends @Nullable RStore<?>> dimnamesAttribute;
	
	
	public JRIArrayImpl(final TData data, final @Nullable String className1, final int[] dim,
			final @Nullable SimpleRList<@Nullable RCharacterStore> dimnames) {
		this.length= (data.getLength() >= 0) ? data.getLength() : RDataUtils.computeLengthFromDim(dim);
		this.data= data;
		this.className1= className1;
		this.dimAttribute= dim;
		this.dimnamesAttribute= dimnames;
	}
	
	public JRIArrayImpl(final TData data, final @Nullable String className1, final int[] dim) {
		this.length= (data.getLength() >= 0) ? data.getLength() : RDataUtils.computeLengthFromDim(dim);
		this.data= data;
		this.className1= className1;
		this.dimAttribute= dim;
	}
	
	@SuppressWarnings("null")
	public JRIArrayImpl(final RJIO io, final RObjectFactory factory) throws IOException {
		readExternal(io, factory);
	}
	
	public void readExternal(final RJIO io, final RObjectFactory factory) throws IOException {
		//-- options
		final int options= io.readInt();
		final boolean customClass= ((options & RObjectFactory.O_CLASS_NAME) != 0);
		//-- special attributes
		if (customClass) {
			this.className1= io.readString();
		}
		this.length= io.readVULong((byte) (options & RObjectFactory.O_LENGTHGRADE_MASK));
		final int[] dim= io.readIntArray();
		this.dimAttribute= dim;
		if ((options & RObjectFactory.O_WITH_NAMES) != 0) {
			final RCharacter32Store names0= new RCharacter32Store(io, dim.length);
			final RStore<?>[] names1= new RStore[dim.length];
			for (int i= 0; i < dim.length; i++) {
				names1[i]= factory.readNames(io, dim[i]);
			}
			this.dimnamesAttribute= new SimpleRList<>(names1, names0);
		}
		//-- data
		this.data= (TData)factory.readStore(io, this.length);
		
		if (!customClass) {
			this.className1= (dim.length == 2) ? RObject.CLASSNAME_MATRIX : RObject.CLASSNAME_ARRAY;
		}
		//-- attributes
		if ((options & RObjectFactory.O_WITH_ATTR) != 0) {
			setAttributes(factory.readAttributeList(io));
		}
	}
	
	@Override
	public void writeExternal(final RJIO io, final RObjectFactory factory) throws IOException {
		//-- options
		int options= io.getVULongGrade(this.length);
		if (this.className1 != null
				&& !this.className1.equals((this.dimAttribute.length == 2) ?
						RObject.CLASSNAME_MATRIX : RObject.CLASSNAME_ARRAY )) {
			options |= RObjectFactory.O_CLASS_NAME;
		}
		if ((io.flags & RObjectFactory.F_ONLY_STRUCT) == 0 && this.dimnamesAttribute != null) {
			options |= RObjectFactory.O_WITH_NAMES;
		}
		final RList attributes= ((io.flags & RObjectFactory.F_WITH_ATTR) != 0) ? getAttributes() : null;
		if (attributes != null) {
			options |= RObjectFactory.O_WITH_ATTR;
		}
		io.writeInt(options);
		//-- special attributes
		if ((options & RObjectFactory.O_CLASS_NAME) != 0) {
			io.writeString(this.className1);
		}
		io.writeVULong((byte) (options & RObjectFactory.O_LENGTHGRADE_MASK), this.length);
		io.writeIntArray(this.dimAttribute, this.dimAttribute.length);
		if ((options & RObjectFactory.O_WITH_NAMES) != 0) {
			((ExternalizableRStore) this.dimnamesAttribute.getNames()).writeExternal(io);
			for (int i= 0; i < this.dimAttribute.length; i++) {
				factory.writeNames(this.dimnamesAttribute.get(i), io);
			}
		}
		//-- data
		factory.writeStore(this.data, io);
		//-- attributes
		if ((options & RObjectFactory.O_WITH_ATTR) != 0) {
			factory.writeAttributeList(attributes, io);
		}
	}
	
	
	@Override
	public final byte getRObjectType() {
		return TYPE_ARRAY;
	}
	
	@Override
	public String getRClassName() {
		final String className1= this.className1;
		return (className1 != null) ? className1 :
				((this.dimAttribute.length == 2) ? RObject.CLASSNAME_MATRIX : RObject.CLASSNAME_ARRAY);
	}
	
	@Override
	public long getLength() {
		return this.length;
	}
	
	@Override
	public RIntegerStore getDim() {
		return new JRIIntegerDataImpl(this.dimAttribute);
	}
	
	@Override
	public @Nullable RCharacterStore getDimNames() {
		if (this.dimnamesAttribute != null) {
			return this.dimnamesAttribute.getNames();
		}
		return null;
	}
	
	@Override
	public @Nullable RStore<?> getNames(final int dim) {
		if (this.dimnamesAttribute != null) {
			return this.dimnamesAttribute.get(dim);
		}
		return null;
	}
	
	
	@Override
	public TData getData() {
		return this.data;
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder();
		sb.append("RObject type=array, class=").append(getRClassName());
		sb.append("\n\tlength=").append(getLength());
		sb.append("\n\tdim=");
		sb.append(Arrays.toString(this.dimAttribute));
		sb.append("\n\tdata: ");
		sb.append(this.data.toString());
		return sb.toString();
	}
	
	
	public int[] getJRIDimArray() {
		return this.dimAttribute;
	}
	
}
