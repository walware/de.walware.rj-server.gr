/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Apache License, Version 2.0 which is available at
 # https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package de.walware.rj.internal.server.gr.jri;

import static org.eclipse.statet.jcommons.lang.ObjectUtils.nonNullAssert;

import java.io.IOException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RCharacterStore;
import org.eclipse.statet.rj.data.REnvironment;
import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RList;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RObjectFactory;
import org.eclipse.statet.rj.data.RStore;
import org.eclipse.statet.rj.data.UnexpectedRDataException;
import org.eclipse.statet.rj.data.impl.AbstractRObject;
import org.eclipse.statet.rj.data.impl.ExternalizableRObject;
import org.eclipse.statet.rj.data.impl.RCharacter32Store;


@NonNullByDefault
public class JRIEnvironmentImpl extends AbstractRObject
		implements REnvironment, ExternalizableRObject {
	
	
	private byte specialType;
	
	private @Nullable String className1;
	
	private String environmentName;
	private long handle;
	
	private long length;
	private @Nullable RObject [] components;
	private RCharacter32Store namesAttribute;
	
	
	public JRIEnvironmentImpl(final String envName, final long handle, final long length,
			final @Nullable RObject @Nullable [] initialComponents, final String @Nullable [] initialNames,
			final byte specialType, final @Nullable String className1) {
		this.environmentName= envName;
		this.handle= handle;
		this.length= length;
		this.components= initialComponents;
		this.namesAttribute= (initialComponents != null) ?
				new RCharacter32Store(nonNullAssert(initialNames), (int)length) : null;
		this.specialType= specialType;
		this.className1= className1;
	}
	
	@SuppressWarnings("null")
	public JRIEnvironmentImpl(final RJIO io, final RObjectFactory factory) throws IOException, ClassNotFoundException {
		readExternal(io, factory);
	}
	
	public void readExternal(final RJIO io, final RObjectFactory factory) throws IOException {
		//-- options
		final int options= io.readInt();
		//-- special attributes
		this.specialType= (byte) ((options >>> 24) & 0xff);
		this.className1= ((options & RObjectFactory.O_CLASS_NAME) != 0) ?
				io.readString() : RObject.CLASSNAME_ENVIRONMENT;
		//-- data
		this.handle= io.readLong();
		this.environmentName= io.readString();
		final long length= this.length= io.readVULong((byte) (options & RObjectFactory.O_LENGTHGRADE_MASK));
		
		if ((options & RObjectFactory.O_NO_CHILDREN) != 0) {
			this.namesAttribute= null;
			this.components= null;
		}
		else if (length > Integer.MAX_VALUE) {
			throw new UnsupportedOperationException();
		}
		else {
			final int l= (int)length;
			this.namesAttribute= new RCharacter32Store(io, l);
			final @Nullable RObject[] components= new @Nullable RObject[l];
			for (int i= 0; i < l; i++) {
				components[i]= factory.readObject(io);
			}
			this.components= components;
		}
		//-- attributes
		if ((options & RObjectFactory.O_WITH_ATTR) != 0) {
			setAttributes(factory.readAttributeList(io));
		}
	}
	
	@Override
	public void writeExternal(final RJIO io, final RObjectFactory factory) throws IOException {
		final long length= this.length;
		//-- options
		int options= io.getVULongGrade(length);
		options|= (this.specialType << 24);
		final boolean customClass= this.className1 != null
				&& !this.className1.equals(RObject.CLASSNAME_ENVIRONMENT);
		if (customClass) {
			options |= RObjectFactory.O_CLASS_NAME;
		}
		final RList attributes= ((io.flags & RObjectFactory.F_WITH_ATTR) != 0) ? getAttributes() : null;
		if (attributes != null) {
			options |= RObjectFactory.O_WITH_ATTR;
		}
		if (this.components == null) {
			options |= RObjectFactory.O_NO_CHILDREN;
		}
		io.writeInt(options);
		//-- special attributes
		if (customClass) {
			io.writeString(this.className1);
		}
		
		io.writeLong(this.handle);
		io.writeString(this.environmentName);
		io.writeVULong((byte) (options & RObjectFactory.O_LENGTHGRADE_MASK), length);
		
		if (this.components != null) {
			final int l= (int)length;
			this.namesAttribute.writeExternal(io);
			//-- data
			for (int i= 0; i < l; i++) {
				factory.writeObject(this.components[i], io);
			}
		}
		//-- attributes
		if (attributes != null) {
			factory.writeAttributeList(attributes, io);
		}
	}
	
	
	@Override
	public final byte getRObjectType() {
		return TYPE_ENVIRONMENT;
	}
	
	@Override
	public String getRClassName() {
		final String className1= this.className1;
		return (className1 != null) ? className1 : RObject.CLASSNAME_ENVIRONMENT;
	}
	
	
	@Override
	public int getSpecialType() {
		return this.specialType;
	}
	
	@Override
	public String getEnvironmentName() {
		return this.environmentName;
	}
	
	@Override
	public long getHandle() {
		return this.handle;
	}
	
	
	@Override
	public long getLength() {
		return this.length;
	}
	
	@Override
	public RCharacterStore getNames() {
		return this.namesAttribute;
	}
	
	@Override
	public String getName(final int idx) {
		return this.namesAttribute.getChar(idx);
	}
	
	@Override
	public String getName(final long idx) {
		return this.namesAttribute.getChar(idx);
	}
	
	@Override
	public @Nullable RObject get(final int idx) {
		return this.components[idx];
	}
	
	@Override
	public @Nullable RObject get(final long idx) {
		if (idx < 0 || idx >= Integer.MAX_VALUE) {
			throw new IndexOutOfBoundsException(Long.toString(idx));
		}
		return this.components[(int) idx];
	}
	
	public @Nullable RObject[] toArray() throws UnexpectedRDataException {
		final int l= (int)this.length;
		final @Nullable RObject[] array= new @Nullable RObject[l];
		System.arraycopy(this.components, 0, array, 0, l);
		return array;
	}
	
	@Override
	public @Nullable RStore<?> getData() {
		return null;
	}
	
	
	@Override
	public @Nullable RObject get(final String name) {
		final int idx= this.namesAttribute.indexOf(name, 0);
		if (idx >= 0) {
			return this.components[idx];
		}
		return null;
	}
	
	public boolean containsName(final String name) {
		return (this.namesAttribute.indexOf(name) >= 0);
	}
	
	
	@Override
	public String toString() {
		final StringBuilder sb= new StringBuilder();
		sb.append("RObject type=environment, class=").append(getRClassName());
		sb.append("\n\tlength=").append(this.length);
		if (this.components != null) {
			sb.append("\n\tdata: ");
			for (int i= 0; i < this.length; i++) {
				sb.append("\n$").append(this.namesAttribute.getChar(i)).append("\n");
				sb.append(this.components[i]);
			}
		}
		else {
			sb.append("\n<NODATA/>");
		}
		return sb.toString();
	}
	
}
