/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Apache License, Version 2.0 which is available at
 # https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package de.walware.rj.internal.server.gr.jri;

import java.io.IOException;

import org.eclipse.statet.jcommons.lang.NonNull;
import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RObjectFactory;
import org.eclipse.statet.rj.data.impl.RCharacter32Store;
import org.eclipse.statet.rj.data.impl.RDataFrame32Impl;


@NonNullByDefault
public class JRIDataFrameImpl extends RDataFrame32Impl {
	
	
	public JRIDataFrameImpl(final @NonNull RObject[] columns, final String className1,
			final @Nullable String [] initialNames,
			final @NonNull String @Nullable [] initialRownames) {
		super(columns, className1, initialNames, initialRownames, false);
	}
	
	public JRIDataFrameImpl(final RJIO io, final RObjectFactory factory) throws IOException {
		super(io, factory, io.readInt());
	}
	
	
	@Override
	protected RCharacter32Store createNamesStore(final @Nullable String [] names) {
		return new JRICharacterDataImpl(names);
	}
	
}
