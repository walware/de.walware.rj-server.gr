/*=============================================================================#
 # Copyright (c) 2009, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Apache License, Version 2.0 which is available at
 # https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package de.walware.rj.internal.server.gr.jri;

import java.io.IOException;

import org.eclipse.statet.jcommons.lang.NonNullByDefault;
import org.eclipse.statet.jcommons.lang.Nullable;

import org.eclipse.statet.rj.data.RJIO;
import org.eclipse.statet.rj.data.RObject;
import org.eclipse.statet.rj.data.RObjectFactory;
import org.eclipse.statet.rj.data.impl.RCharacter32Store;
import org.eclipse.statet.rj.data.impl.RList32Impl;


@NonNullByDefault
public class JRIListImpl extends RList32Impl {
	
	
	public JRIListImpl(final RObject[] initialComponents, final String className1,
			final @Nullable String[] initialNames) {
		super(initialComponents, (className1 != null) ? className1 : CLASSNAME_LIST,
				initialNames, initialComponents.length );
	}
	
	@Override
	protected RCharacter32Store createNamesStore(final @Nullable String [] names) {
		return new JRICharacterDataImpl(names);
	}
	
	public JRIListImpl(final RJIO io, final RObjectFactory factory, final int options) throws IOException {
		super(io, factory, options);
	}
	
	
}
