AC_INIT(rj, 4.0, stephan.wahlbrink@walware.de)
AC_CONFIG_AUX_DIR([tools])


AC_MSG_CHECKING(R config)
: ${R_HOME=`R RHOME`}
if test -z "${R_HOME}"; then
    AC_MSG_ERROR(cannot determine R_HOME)
fi
RBIN="${R_HOME}/bin/R"

AC_MSG_RESULT([R config set
    R_HOME   : ${R_HOME}])

AC_SUBST(R_HOME)
export R_HOME


# Set CC/CFLAGS
CC=`"${RBIN}" CMD config CC`;
CFLAGS=`"${RBIN}" CMD config CFLAGS`

AC_PROG_CC
AC_LANG(C)


AC_MSG_CHECKING(Java support in R)
: ${JAVA_HOME=`"${RBIN}" CMD config JAVA_HOME|sed 's/ERROR:.*//'`}

if test -z "${JAVA_HOME}"; then
    AC_MSG_ERROR([cannot determine JAVA_HOME
R is not configured with Java support. Please run
    R CMD javareconf
as root to add Java support to R.

If you don't have root privileges, run
    R CMD javareconf -e
to set all Java-related variables and then install the package.])
else
    AC_MSG_RESULT([...])
fi

: ${JAVA=`"${RBIN}" CMD config JAVA|sed 's/ERROR:.*//'`}
: ${JAVAC=`"${RBIN}" CMD config JAVAC|sed 's/ERROR:.*//'`}
JAVAC_FLAGS=' -h .'
: ${JAVA_CPPFLAGS=`"${RBIN}" CMD config JAVA_CPPFLAGS|sed 's/ERROR:.*//'`}
: ${JAVA_LIBS=`"${RBIN}" CMD config JAVA_LIBS|sed 's/ERROR:.*//'`}
: ${JAR=`"${RBIN}" CMD config JAR|sed 's/ERROR:.*//'`}

JAVA_VERSION=`"${JAVA}" -version 2>&1 | sed -n 's:^.* version "::p' | sed 's:".*::'`
if test -z "${JAVA_VERSION}"; then
    AC_MSG_WARN([**** Cannot detect Java version - the java -version output is unknown! ****])
else
    AC_MSG_RESULT([${JAVA_VERSION}])
fi

have_all_java=yes
if test -z "${JAVA_HOME}" \
        || test -z "${JAVA}" \
        || test -z "${JAVAC}" \
        || test -z "${JAVA_CPPFLAGS}" \
        || test -z "${JAVA_LIBS}" \
        || test -z "${JAR}"; then
    have_all_java=no
fi

if test ${have_all_java} = no; then
    AC_MSG_ERROR([cannot determine complete java config
    JAVA_HOME: ${JAVA_HOME}
    JAVA     : ${JAVA}
    JAVAC    : ${JAVAC}
    JAVAC_FLAGS: ${JAVAC_FLAGS}
    JAVA_CPPFLAGS: ${JAVA_CPPFLAGS}
    JAVA_LIBS: ${JAVA_LIBS}
    JAR      : ${JAR}
R is not configured with full Java support. Please make sure 
an JDK is installed and run
    R CMD javareconf
as root to add Java support to R.

If you don't have root privileges, run
    R CMD javareconf -e
to set all Java-related variables and then install the package.])
fi

AC_MSG_RESULT([Java config set
    JAVA_HOME: ${JAVA_HOME}
    JAVA     : ${JAVA}
    JAVAC    : ${JAVAC}
    JAVAC_FLAGS: ${JAVAC_FLAGS}
    JAVA_CPPFLAGS: ${JAVA_CPPFLAGS}
    JAVA_LIBS: ${JAVA_LIBS}
    JAR      : ${JAR}
])

AC_SUBST(JAVA_HOME)
AC_SUBST(JAVA)
AC_SUBST(JAVAC)
AC_SUBST(JAVAC_FLAGS)
AC_SUBST(JAVA_CPPFLAGS)
AC_SUBST(JAVA_LIBS)
AC_SUBST(JAR)
export JAVA_HOME JAVA JAVAC JAVAC_FLAGS JAVA_CPPFLAGS JAVA_LIBS JAVA_LD_LIBRARY_PATH JAR


# Configure and build JRI
CONFIGURED=1
export CONFIGURED
AC_CONFIG_SUBDIRS(jri)

AC_CONFIG_FILES([src/Makevars])
AC_OUTPUT
