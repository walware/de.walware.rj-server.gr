 #=============================================================================#
 # Copyright (c) 2017, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Eclipse Public License 2.0 which is available at
 # https://www.eclipse.org/legal/epl-2.0, or the Apache License, Version 2.0
 # which is available at https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: EPL-2.0 OR Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================#


## Extensions for compatibility of R apps with StatET


apps.initExt <- function() {
	setHook(hookName= "shiny.onAppStart", value= function(appUrl, ...) {
		rj::.client.execCommand(
				commandId= 'org.eclipse.statet.r.apps.onAppStarted',
				args= list(url= appUrl, typeId= 'org.eclipse.statet.r.apps.appTypes.RShiny'),
				wait= FALSE );
	}, action= "append" );
	setHook(hookName= "shiny.onAppStop", value= function(appUrl, ...) {
		rj::.client.execCommand(
				commandId= 'org.eclipse.statet.r.apps.onAppStopped',
				args= list(url= appUrl, typeId= 'org.eclipse.statet.r.apps.appTypes.RShiny'),
				wait= FALSE );
				
		.rj.tmp$appDomains <- NULL;
	}, action= "append" );
	
	return (invisible());
}

apps.addDomain <- function(domain) {
	if (!is.null(domain) && !any(sapply(.rj.tmp$appDomains, identical, domain))) {
		.rj.tmp$appDomains <- c(.rj.tmp$appDomains, domain);
	}
}


### Dbg

apps.initDbgExt <- function() {
	setHook(hookName= "shiny.onAppStop", value= function(...) {
		dbg.unregisterEnvs(key= "#s:dbg.ShinyApp");
	}, action= "append" )
	
	return (invisible());
}


#' Debug Hook for Shiny
#' 
#' @param params see documentation of hook.
#' @param ... for compatibility.
#' @details The hook is called by the shiny package, it is not intended that the function is
#'     called by the users.
#' 
#' @export
#' @keywords internal
registerShinyDebugHook <- function(params, ...) {
	if (rj.state$isAppDbgExtInitialized) {
		fun <- get(params$name, envir= params$where)
		if (is.function(fun)
				&& !is.null(sfile <- attr(body(fun), "srcfile"))
				&& is.null(sfile$timestampFixed) ) {
			sfile$timestampFixed <- TRUE
			file.timestamp <- file.mtime(sfile$filename)
			if (!is.na(file.timestamp)
					&& (is.na(sfile$timestamp) || file.timestamp < sfile$timestamp) ) {
				sfile$timestamp <- file.timestamp
			}
		}
		
		dbg.registerEnv(key= "#s:dbg.ShinyApp", env= params$where, objectNames= params$name)
	}
	
	apps.addDomain(shiny::getDefaultReactiveDomain());
	
	return (invisible());
}
