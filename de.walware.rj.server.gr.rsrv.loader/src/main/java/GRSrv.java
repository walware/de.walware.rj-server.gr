/*=============================================================================#
 # Copyright (c) 2018, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Apache License, Version 2.0 which is available at
 # https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/


import java.net.URI;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import javax.swing.UIManager;

import org.eclipse.statet.rj.RjException;
import org.eclipse.statet.rj.RjInitFailedException;
import org.eclipse.statet.rj.server.Server;
import org.eclipse.statet.rj.server.srv.CliUtil;
import org.eclipse.statet.rj.server.srv.RMIServerControl;
import org.eclipse.statet.rj.server.srv.ServerControl;
import org.eclipse.statet.rj.server.srv.engine.SrvEngine;
import org.eclipse.statet.rj.server.srv.engine.SrvEnginePluginExtension;
import org.eclipse.statet.rj.server.srv.engine.SrvEngineServer;
import org.eclipse.statet.rj.server.srvext.ServerRuntimePlugin;
import org.eclipse.statet.rj.server.util.LocalREnv;
import org.eclipse.statet.rj.server.util.ServerUtils;


/**
 * Main exec class for RJServ
 */
public class GRSrv {
	
	
	private static Logger getLogger() {
		return Logger.getLogger("de.walware.rj.server.gr.jri");
	}
	
	
	private static class RJavaMainClassLoader extends RJavaClassLoader {
		
		
		private final LocalREnv rEnv;
		
		private final String r_arch;
		
		
		public RJavaMainClassLoader(final LocalREnv rEnv, final ClassLoader parent) throws RjInitFailedException {
			super(parent);
			this.rEnv= rEnv;
			
			this.r_arch= getNonEmpty(System.getenv("R_ARCH"), System.getProperty("r.arch"));
			
			Path rjPath= this.rEnv.checkPath(System.getProperty("de.walware.rj.server.RPkg.path"));
			if (rjPath == null) {
				rjPath= this.rEnv.searchRPkg("rj");
			}
			if (rjPath == null) {
				final String message = "Path to rj package not found. Use R_LIBS or java property 'de.walware.rj.server.RPkg.path' to specify the location.";
				LOGGER.log(Level.SEVERE, "GRSrv/RJ-Init: " + message);
				throw new RuntimeException(message);
			}
			
			// jri library
			String jriLibPath= System.getProperty("rjava.jrilibs");
			if (jriLibPath == null) {
				jriLibPath= rjPath + "/jri";
			}
			final String jriDynlibName;
			switch (this.rEnv.getOSType()) {
			case LocalREnv.OS_WIN:
				jriDynlibName= "jri.dll";
				break;
			case LocalREnv.OS_MAC:
				jriDynlibName= "libjri.jnilib";
				break;
			default:
				jriDynlibName= "libjri.so";
				break;
			}
			final UnixFile jriDynlibFile= searchExistingFile(getRArchNames(jriLibPath, jriDynlibName));
			if (jriDynlibFile != null) {
				addRLibrary("jri", jriDynlibFile);
				if (LOGGER.isLoggable(Level.CONFIG)) {
					LOGGER.log(Level.CONFIG, "GRSrv/RJ-Init: JRI library ''{0}'' registered.", jriDynlibFile);
				}
			}
			else {
				LOGGER.log(Level.WARNING, "GRSrv/RJ-Init: JRI library ''{0}'' not found.", jriDynlibName);
			}
			
			final UnixFile jriJarFile= searchExistingFile(new String[] {
					jriLibPath + "/JRI.jar",
					rjPath + "/jri/JRI.jar",
			});
			if (jriJarFile != null) {
				addClassPath(jriJarFile);
			}
			else {
				logEntries();
				final String message= "JRI.jar not found.";
				LOGGER.log(Level.SEVERE, "GRSrv/RJ-Init: " + message);
				throw new RuntimeException(message);
			}
			
			{	initRJavaLib();
				
				final String rJavaClassPath= System.getProperty("rjava.class.path");
				if (rJavaClassPath != null) {
					final List<Path> pathList= this.rEnv.checkPathList(rJavaClassPath);
					if (pathList != null) {
						for (final Path path : pathList) {
							addClassPath(path.toString());
						}
					}
				}
			}
			
			if (verbose) {
				logEntries();
			}
		}
		
		
		private String[] getRArchNames(final String prefix, final String postfix) {
			final String[] names;
			int i= 0;
			if (this.r_arch != null) {
				names= new String[2];
				names[i++]= prefix + this.r_arch + '/' + postfix;
			}
			else {
				names= new String[1];
			}
			names[i++]= prefix + '/' + postfix;
			return names;
		}
		
		/** For compatibility */
		private void initRJavaLib() {
			// rJava library
			Path rJavaPath= this.rEnv.checkPath(System.getProperty("rjava.path"));
			if (rJavaPath == null) {
				rJavaPath= this.rEnv.searchRPkg("rJava");
			}
			if (rJavaPath == null) {
				return;
			}
			
			String rJavaLibPath= System.getProperty("rjava.rjavalibs");
			if (rJavaLibPath == null) {
				rJavaLibPath= rJavaPath + "/java";
			}
			addClassPath(rJavaPath + "/java");
			final String rJavaDynlibName;
			switch (this.rEnv.getOSType()) {
			case LocalREnv.OS_WIN:
				rJavaDynlibName= "rJava.dll";
				break;
			default:
				rJavaDynlibName= "rJava.so";
				break;
			}
			final UnixFile rJavaDynlibFile= new UnixFile(rJavaLibPath + '/' + rJavaDynlibName);
			if (rJavaDynlibFile.exists()) {
				addRLibrary("rJava", rJavaDynlibFile);
			}
		}
		
	}
	
	
	public static void main(final String[] args) throws RjException {
		control(args, ClassLoader.getSystemClassLoader());
	}
	
	public static void control(final String[] args, final ClassLoader classLoader) {
		final CliUtil cli= new CliUtil(args);
		final RMIServerControl serverControl;
		switch (cli.getCommand()) {
		
		case "start":
			serverControl= new RMIServerControl(cli.getName(), cli.getOptions());
			final SrvEngineServer srvEngineServer= serverControl.initServer();
			final SrvEngine engine= loadEngine(serverControl, srvEngineServer, classLoader);
			srvEngineServer.setEngine(engine);
			serverControl.start(srvEngineServer);
			return;
		
		case "clean":
			serverControl= new RMIServerControl(cli.getName(), cli.getOptions());
			serverControl.clean();
			return;
		}
	}
	
	
	private static SrvEngine loadEngine(final RMIServerControl control, final Server publicServer,
			final ClassLoader parentClassLoader) {
		final ClassLoader oldLoader= Thread.currentThread().getContextClassLoader();
		try {
			final LocalREnv rEnv= new LocalREnv();
			
			if (control.getOptions().containsKey("verbose")) {
				RJavaClassLoader.setDebug(1000);
			}
			final RJavaClassLoader loader= new RJavaMainClassLoader(rEnv, parentClassLoader);
			if (Boolean.parseBoolean(System.getProperty("org.eclipse.statet.rj.debug"))) {
				loader.setDefaultAssertionStatus(true);
			}
			
			// Add rj-engine to RJavaClassLoader
			// If this does not work, you can add in your command line to the rjava.class.path property
			final URI srvClasspath= RJSrv.getPkgLib("de.walware.rj.server.gr.rsrv"); //$NON-NLS-1$
			loader.addClassPath(srvClasspath);
			
			final SrvEngine engine;
			try {
				Thread.currentThread().setContextClassLoader(loader);
				
				final Class<? extends SrvEngine> serverClazz= (Class<? extends SrvEngine>)loader
						.loadRJavaClass("de.walware.rj.internal.server.gr.jri.GRSrvEngine");
				
				engine= serverClazz.getDeclaredConstructor().newInstance();
			}
			catch (final ClassNotFoundException e) {
				getLogger().log(Level.INFO,
						"Perhaps autodetection of 'rj-engine' classpath entry failed: " + 
						((srvClasspath != null) ? srvClasspath : "<not found>") );
				throw e;
			}
			
			final int[] rjVersion= ServerUtils.RJ_VERSION;
			final int[] implVersion= engine.getVersion();
			if ((implVersion.length < 2)
					|| (implVersion[0] != rjVersion[0]) || (implVersion[1] != rjVersion[1])) {
				final StringBuilder sb= new StringBuilder();
				sb.append("The version of the loaded RJ server engine ");
				ServerUtils.prettyPrintVersion(implVersion, sb);
				sb.append(" is not compatible to RJ version ");
				ServerUtils.prettyPrintVersion(rjVersion, sb);
				sb.append(".");
				sb.append(" Make sure that the correct R package 'rj' is installed and in the R library path.");
				throw new RjInitFailedException(sb.toString());
			}
			
			final Map<String, Object> varargs= new HashMap<>();
			varargs.put("REnv", rEnv);
			varargs.put("ClassLoader", loader);
			engine.init(control, publicServer, varargs);
			
			
			final SrvEnginePluginExtension localServer= (SrvEnginePluginExtension) engine;
			
			// plugins
			final List<String> plugins= ServerUtils.getArgValueList(
					control.getOptions().get("plugins") );
			if (plugins.contains("awt")) {
				UIManager.put("ClassLoader", loader);
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				}
				catch (final Throwable e) {
				}
			}
			if (plugins.contains("swt")) {
				localServer.addPlugin((ServerRuntimePlugin)Class
						.forName("org.eclipse.statet.rj.server.e.srvext.SWTPlugin")
						.getDeclaredConstructor().newInstance() );
			}
			return engine;
		}
		catch (final Throwable e) {
			final LogRecord record= new LogRecord(Level.SEVERE,
					"Initializing JRI/Rengine failed.");
			record.setThrown(e);
			getLogger().log(record);
			
			ServerControl.exit(ServerControl.EXIT_INIT_RENGINE_ERROR | 1);
			throw new RuntimeException();
		}
		finally {
			try {
				Thread.currentThread().setContextClassLoader(oldLoader);
			}
			catch (final Throwable e) { }
		}
	}
	
	
	private static String getNonEmpty(final String... strings) {
		for (final String string : strings) {
			if (string != null && string.length() > 0) {
				return string;
			}
		}
		return null;
	}
	
}
