/*=============================================================================#
 # Copyright (c) 2005, 2025 Simon Urbanek and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the GNU Lesser General Public License v2.1 or newer which
 # accompanies this distribution, and is available at
 # http://www.gnu.org/licenses/lgpl.html
 # 
 # Contributors:
 #     Simon Urbanek <simon.urbanek@r-project.org> - initial API and implementation
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - adjustments to RJ
 #=============================================================================*/

/* package (default) */

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectStreamClass;
import java.net.URI;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.eclipse.statet.rj.server.util.ServerUtils;


public class RJavaClassLoader extends URLClassLoader {
	
	
	protected static final Logger LOGGER= Logger.getLogger("de.walware.rj.server.gr.jri");
	
	public static boolean verbose= false;
	
	public static void setDebug(final int level) {
		verbose= (level > 0);
		if (verbose) {
			LOGGER.setLevel(Level.ALL);
		}
	}
	
	
	private static RJavaClassLoader primaryLoader;
	
	/**
	 * Returns the primary instance of RJavaClassLoader.
	 */ 
	public static RJavaClassLoader getPrimaryLoader() {
		return RJavaClassLoader.primaryLoader;
	}
	
	private static final AtomicInteger COUNTER= new AtomicInteger();
	
	protected static final String getId(final ClassLoader classLoader) {
		if (classLoader == null) {
			return "";
		}
		if (classLoader instanceof RJavaClassLoader) {
			return ((RJavaClassLoader)classLoader).getId();
		}
		return classLoader.toString();
	}
	
	
/*[ UnixFile ]=================================================================*/
	
	/**
	 * Light extension of File that handles file separators and updates
	 */
	protected static class UnixFile extends java.io.File {
		
		private static final long serialVersionUID= 6120112960030467453L;
		
		/**
		 * cached "last time modified" stamp
		 */ 
		private long lastModStamp;
		public Object cache;
		
		/**
		 * Constructor. Modifies the path so that the proper path separator is used (most useful on windows)
		 */
		public UnixFile(final String fn) {
			super((separatorChar != '/') ? fn.replace('/', separatorChar) : fn);
			this.lastModStamp= 0;
		}
		
		public UnixFile(final URI uri) {
			super(uri);
			this.lastModStamp= 0;
		}
		
		/**
		 * @return whether the file modified since last time the update method was called
		 */
		public boolean hasChanged() {
			final long curMod= lastModified();
			return (curMod != this.lastModStamp);
		}
		
		/**
		 * Cache the result of the lastModified stamp
		 */
		public void update() {
			this.lastModStamp= lastModified();
		}
		
	}
	
	/**
	 * Specialization of UnixFile that deals with jar files
	 */
	private static class UnixJarFile extends UnixFile {
		
		private static final long serialVersionUID= -956832260008070610L;
		
		/**
		 * The cached jar file
		 */
		private ZipFile zfile ;
		
		/**
		 * common prefix for all URLs within this jar file
		 */
		private String urlPrefix ;
		
		public UnixJarFile(final String filename) {
			super(filename);
		}
		
		@Override
		public void update() {
			try {
				if (this.zfile != null) {
					this.zfile.close();
				}
				this.zfile= new ZipFile(this) ;
			}
			catch (final Exception tryCloseX) {}
			/* time stamp */
			super.update() ; 
		}
		
		/**
		 * Get an input stream for a resource contained in the jar file
		 * 
		 * @param name file name of the resource within the jar file
		 * @return an input stream representing the resouce if it exists or null
		 */ 
		public InputStream getResourceAsStream(final String name) {
			if (this.zfile==null || hasChanged()) {
				update(); 
			}
			try {
				if (this.zfile == null) {
					return null;
				}
				final ZipEntry e= this.zfile.getEntry(name);
				if (e != null) {
					return this.zfile.getInputStream(e);
				}
			}
			catch (final Exception e) {
				LOGGER.log(Level.WARNING, "Failed to create resource stream for JAR file.", e);
			}
			return null;
		}
		
		public URL getResource(final String name) {
			if(this.zfile == null  || this.zfile.getEntry(name) == null) {
				return null;
			}
			
			URL url= null;
			if (this.urlPrefix == null) {
				try{
					this.urlPrefix= "jar:" + toURL().toString() + "!";
				}
				catch(final java.net.MalformedURLException ex) {
				}
			}
			
			try{
				url= new URL(this.urlPrefix + name) ;
			}
			catch (final java.net.MalformedURLException ex) {
			}
			return url ;
		}
		
	}
	
	/**
	 * Specialization of UnixFile representing a directory
	 */ 
	protected static class UnixDirectory extends UnixFile {
		
		private static final long serialVersionUID= -5697105404215366546L;
		
		public UnixDirectory(final String dirname) {
			super(dirname);
		}
		
	}
	
	protected static UnixFile searchExistingFile(final String[] search) {
		for (final String path : search) {
			final UnixFile file= new UnixFile(path);
			if (file.exists()) {
				return file;
			}
		}
		return null;
	}
	
/*=============================================================================*/
	
	
	private final String id;
	
	/**
	 * Map of native libraries (name, path)
	 */
	private final HashMap<String, UnixFile> libMap= new HashMap<>();
	
	/**
	 * The class path list for alternative class loading
	 */ 
	private final Vector<UnixFile> classPath= new Vector<>();
	
	/**
	 * Should the system class loader be used to resolve classes as well as this class loader
	 */
	private final boolean useSystem= !"false".equalsIgnoreCase(System.getProperty("rjava.classloader.system")); // default true
	
	private final boolean useSecond= "true".equalsIgnoreCase(System.getProperty("rjava.classloader.alternative")); // default false
	
	
	protected RJavaClassLoader(final ClassLoader parent) {
		super(new URL[0], parent);
		
		this.id= String.format("RJavaClassLoader#%1$08X", COUNTER.getAndIncrement());
		
		if (RJavaClassLoader.primaryLoader == null) {
			RJavaClassLoader.primaryLoader= this;
		}
		
		if (LOGGER.isLoggable(Level.INFO)) {
			final ClassLoader actualParent= getParent();
			LOGGER.log(Level.INFO, "{0}: Initializing class loader... ({1})" +
							"\n    primary= {2}" +
							"\n    parent= {3}" +
							"\n    useSystemMode= {4}" +
							"\n    useAlternateMode= {5}",
					new Object[] {
							this.id,
							getClass().getName(),
							(RJavaClassLoader.primaryLoader == this),
							(actualParent != null) ?
									getId(actualParent) +
											" (" + actualParent.getClass().getName() +
											", explicite= " + (parent != null) + ")" :
									"<none>",
							(this.useSystem) ? "enabled" : "disabled",
							(this.useSecond) ? "enabled" : "disabled",
					});
		}
	}
	
	public RJavaClassLoader(final String path, final String libpath, final RJavaClassLoader parent) {
		this((parent != null) ? parent : getPrimaryLoader());
	}
	
	
	protected String getId() {
		return this.id;
	}
	
	protected void logEntries() {
		final StringBuilder sb= new StringBuilder();
		sb.append(this.id).append(": ")
				.append("Registered native libraries:");
		ServerUtils.prettyPrint(this.libMap, sb);
		LOGGER.log(Level.FINE, sb.toString());
		
		sb.setLength(0);
		sb.append(this.id).append(": ")
				.append("Registered class paths:");
		if (this.useSystem) {
			ServerUtils.prettyPrint(Arrays.asList(getURLs()), sb);
		}
		else {
			ServerUtils.prettyPrint(this.classPath, sb);
		}
		LOGGER.log(Level.FINE, sb.toString());
	}
	
	
	protected void addRLibrary(final String name, final UnixFile file) {
		this.libMap.put(name, file);
	}
	
	/** add a library to path mapping for a native library */
	public void addRLibrary(final String name, final String path) {
		addRLibrary(name, new UnixFile(path));
	}
	
	@Override
	protected String findLibrary(final String name) {
		if (verbose) {
			LOGGER.entering("RJavaClassLoader", "findLibrary", name);
		}
		
		final UnixFile u= this.libMap.get(name);
		String s= null;
		if (u != null && u.exists()) {
			s= u.getPath();
		}
		if (LOGGER.isLoggable(Level.FINE)) {
			LOGGER.log(Level.FINE, "{0}: Mapping library ''{1}'' to ''{2}''.",
					new Object[] {
							this.id,
							name,
							(s == null) ? "<none>" : s,
					});
		}
		
		return s;
	}
	
	
	public void addClassPath(final String cp) {
		final UnixFile f= new UnixFile(cp);
		addClassPath(f);
	}
	
	public void addClassPathUtf8(final byte[] cp) {
		final UnixFile f= new UnixFile(new String(cp, StandardCharsets.UTF_8));
		addClassPath(f);
	}
	
	public void addClassPath(final URI uri) {
		final UnixFile f= new UnixFile(uri);
		addClassPath(f);
	}
	
	public void addClassPath(final UnixFile f) {
		if (this.useSystem) {
			try {
				addURL(f.toURI().toURL());
				if (LOGGER.isLoggable(Level.FINE)) {
					LOGGER.log(Level.FINE, "{0}: Added ''{1}'' to classpath of URL loader.",
							new Object[] {
									this.id,
									f.getPath(),
							});
				}
				// return; // we need to add it anyway
			}
			catch (final Exception e) {
			}
		}
		if (this.useSecond) {
			UnixFile g= null;
			if (f.isFile() && (f.getName().endsWith(".jar") || f.getName().endsWith(".JAR"))) {
				g= new UnixJarFile(f.getPath());
				if (verbose) {
					System.out.println("RJavaClassLoader: adding Java archive file '"+g+"' to the internal class path");
				}
			}
			else if (f.isDirectory()) {
				g= new UnixDirectory(f.getPath());
				if (verbose) {
					System.out.println("RJavaClassLoader: adding class directory '"+g+"' to the internal class path");
				}
			}
			else if (verbose) {
				System.err.println(f.exists() ?
						("WARNING: the path '"+f+"' is neither a directory nor a JAR file, it will NOT be added to the internal class path!") :
						("WARNING: the path '"+f+"' does NOT exist, it will NOT be added to the internal class path!") );
			}
			
			if (g != null && !this.classPath.contains(g)) {
				this.classPath.add(g);
				System.setProperty("java.class.path",
						System.getProperty("java.class.path")+File.pathSeparator+g.getPath());
			}
		}
	}
	
	/**
	 * Adds multiple entries to the class path
	 */
	public void addClassPath(final String[] cp) {
		int i= 0;
		while (i < cp.length) {
			addClassPath(cp[i++]);
		}
	}
	
	public void addClassPath(final List<String> cpList) {
		for (final String path : cpList) {
			addClassPath(path);
		}
	}
	
	/**
	 * @return the array of class paths used by this class loader
	 */
	public String[] getClassPath() {
		final List<String> list= new ArrayList<>();
		if (this.useSystem) {
			final URL[] urls= getURLs();
			for (final URL url : urls) {
				list.add(url.toString());
			}
		}
		if (this.useSecond) {
			final UnixFile[] files= this.classPath.toArray(new UnixFile[list.size()]);
			for (final UnixFile file : files) {
				list.add(file.getPath());
			}
		}
		return list.toArray(new String[list.size()]);
	}
	
	@Override
	protected Class<?> findClass(final String name) throws ClassNotFoundException {
		if (verbose) {
			LOGGER.entering("RJavaClassLoader", "findClass", name);
		}
		Class cl= null;
		if ("RJavaClassLoader".equals(name)) {
			return RJavaClassLoader.class;
		}
		if (this.useSystem) {
			try {
				cl= super.findClass(name);
				if (cl != null) {
					if (LOGGER.isLoggable(Level.FINE)) {
						LOGGER.log(Level.FINE, "{0}: Found class ''{1}'' using URL loader.",
								new Object[] {
										this.id,
										name,
								});
					}
					return cl;
				}
			}
			catch (final Exception e) {
				if (LOGGER.isLoggable(Level.FINE)) {
					final LogRecord record= new LogRecord(Level.FINE, "{0}: Could not find class ''{1}'' using URL loader.");
					record.setParameters(new Object[] {
							this.id,
							name,
					});
					record.setThrown(e);
					LOGGER.log(record);
				}
				if (!this.useSecond && e instanceof ClassNotFoundException) {
					throw (ClassNotFoundException)e;
				}
			}
		}
		if (this.useSecond) {
			if (verbose) {
				System.out.println("RJavaClassLoader.findClass(\"" + name + "\")");
			}
			
			final String classFileName= classNameToFile(name) + ".class";
			InputStream ins= null;
			
			for (final Enumeration<UnixFile> e= this.classPath.elements(); e.hasMoreElements();) {
				final UnixFile cp= e.nextElement();
				
				if (verbose) {
					System.out.println("  - trying class path \"" + cp + "\"");
				}
				try {
					ins= null;
					if (cp instanceof UnixJarFile) {
						ins= ((UnixJarFile) cp).getResourceAsStream(classFileName);
						if (verbose) {
							System.out.println("    JAR file, can get '" + classFileName + "'? " + ((ins != null) ? "YES" : "NO"));
						}
					}
					else if (cp instanceof UnixDirectory) {
						final UnixFile class_f= new UnixFile(cp.getPath()+'/'+classFileName);
						if (class_f.isFile()) {
							ins= new FileInputStream(class_f);
						}
						if (verbose) {
							System.out.println("    Directory, can get '" + classFileName + "'? " + ((ins != null) ? "YES" : "NO"));
						}
					}
					if (ins != null) {
						int al= 128 * 1024;
						byte fc[]= new byte[al];
						int n= ins.read(fc);
						int rp= n;
						// System.out.println(" loading class file, initial n =
						// "+n);
						while (n > 0) {
							if (rp == al) {
								int nexa= al * 2;
								if (nexa < 512 * 1024) {
									nexa= 512 * 1024;
								}
								final byte la[]= new byte[nexa];
								System.arraycopy(fc, 0, la, 0, al);
								fc= la;
								al= nexa;
							}
							n= ins.read(fc, rp, fc.length - rp);
							// System.out.println(" next n= "+n+" (rp="+rp+",
							// al="+al+")");
							if (n > 0) {
								rp+= n;
							}
						}
						ins.close();
						n= rp;
						if (verbose) {
							System.out.println("RJavaClassLoader: loaded class " + name + ", " + n + " bytes");
						}
						try {
							cl= defineClass(name, fc, 0, n);
						} catch (final Exception dce) {
							throw new ClassNotFoundException("Class not found - candidate class binary found but could not be loaded", dce);
						}
 						if (verbose) {
							System.out.println("  defineClass('" + name +"') returned " + cl);
						}
					}
				}
				catch (final ClassNotFoundException ex) {
					throw ex;
				}
				catch (final Exception ex) {
					// System.out.println(" * won't work: "+ex.getMessage());
				}
			}
		}
		// System.out.println("=== giving up");
		if (cl == null) {
			throw (new ClassNotFoundException(name));
		}
		return cl;
	}
	
	public Class<?> loadRJavaClass(final String name) throws ClassNotFoundException {
		return loadClass(name, true);
	}
	
	
	@Override
	public URL findResource(final String name) {
		if (verbose) {
			LOGGER.entering("RJavaClassLoader", "findResource", name);
		}
		if (this.useSystem) {
			try {
				final URL url= super.findResource(name);
				if (url != null) {
					if (LOGGER.isLoggable(Level.FINE)) {
						LOGGER.log(Level.FINE, "{0}: Found resource ''{1}'' at ''{2}'' using URL loader.",
								new Object[] {
										this.id,
										name,
										url
								});
					}
					return url;
				}
			}
			catch (final Exception e) {
			}
		}
		if (this.useSecond) {
			for (final Enumeration<UnixFile> e= this.classPath.elements(); e.hasMoreElements();) {
				final UnixFile cp= e.nextElement();
				
				try {
					if (cp instanceof UnixJarFile) {
						final URL url= ((UnixJarFile)cp).getResource(name) ;
						if (url != null) {
							if (verbose) {
								System.out.println("  - found in a JAR file, URL " + url);
							}
							return url;
						}
					}
					else if (cp instanceof UnixDirectory) {
						final UnixFile res_f= new UnixFile(cp.getPath() + "/" + name);
						if (res_f.isFile()) {
							if (verbose) {
								System.out.println("  - find as a file: "+res_f);
							}
							return res_f.toURL();
						}
					}
				}
				catch (final Exception iox) {
				}
			}
		}
		return null;
	}
	
	
	protected final String classNameToFile(final String cls) {
		return cls.replace('.', '/');
	}
	
	
/*[ Serialization ]============================================================*/
	
	class RJavaObjectInputStream extends ObjectInputStream {
		
		public RJavaObjectInputStream(final InputStream in) throws IOException {
			super(in);
		}
		
		@Override
		protected Class resolveClass(final ObjectStreamClass desc) throws ClassNotFoundException {
			return Class.forName(desc.getName(), false, RJavaClassLoader.getPrimaryLoader());
		}
		
	}
	
	/** 
	 * Serializes an object to a byte array. (code by CB)
	 *
	 * @param object object to serialize
	 * @return byte array that represents the object
	 * @throws Exception 
	 */
	public static byte[] toByte(final Object object) throws Exception {
		final ByteArrayOutputStream os= new ByteArrayOutputStream();
		final ObjectOutputStream oos= new ObjectOutputStream(os);
		oos.writeObject(object);
		oos.close();
		return os.toByteArray();
	}
	
	/** 
	 * Deserializes an object from a byte array. (code by CB)
	 *
	 * @param byteArray
	 * @return the object that is represented by the byte array
	 * @throws Exception 
	 */
	public Object toObject(final byte[] byteArray) throws Exception {
		final InputStream is= new ByteArrayInputStream(byteArray);
		final RJavaObjectInputStream ois= new RJavaObjectInputStream(is);
		final Object o= ois.readObject();
		ois.close();
		return o;
	}
	
	/**
	 * Converts the byte array into an Object using the primary RJavaClassLoader.
	 */
	public static Object toObjectPL(final byte[] byteArray) throws Exception{
		return RJavaClassLoader.getPrimaryLoader().toObject(byteArray);
	}
	
}
