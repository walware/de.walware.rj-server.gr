# RJSrv for GNU R

RJSrv (R Server for RJ) implementation for GNU R.


## Installation

The server is provided as R packages.

For installation instructions see wiki page [Installation](https://gitlab.com/walware/de.walware.rj-server.gr/wikis/Installation).


## Known Issues

See wiki page [Known Issues](https://gitlab.com/walware/de.walware.rj-server.gr/wikis/Known-Issues).

## See Also

  * Eclipse Wiki: [StatET/R Console](https://wiki.eclipse.org/StatET/R_Console)
