import java.awt.Dimension;
import java.awt.FileDialog;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import org.rosuda.JRI.RConsoleOutputStream;
import org.rosuda.JRI.RMainLoopCallbacksUtf8;
import org.rosuda.JRI.Rengine;


class TextConsole2 implements RMainLoopCallbacksUtf8 {
	
	
	private static String toJString(final byte[] bytes) {
		return (bytes != null) ? new String(bytes, StandardCharsets.UTF_8) : null;
	}
	
	private static byte[] toRString(final String s) {
		return (s != null) ? s.getBytes(StandardCharsets.UTF_8) : null;
	}
	
	
    JFrame f;
	
    public JTextArea textarea = new JTextArea();

    public TextConsole2() {
        f = new JFrame();
        f.getContentPane().add(new JScrollPane(textarea));
        f.setSize(new Dimension(800,600));
        f.setVisible(true);
    }
	
	
	@Override
	public void rOnSetup(Rengine re) {
		System.out.println("rOnSetup");
	}
	
	@Override
	public void rOnExit(Rengine re) {
		System.out.println("rOnExit");
	}
	
	
    @Override
    public void rWriteConsole(Rengine re, ByteBuffer text, int oType) {
        text.flip();
        byte[] array= new byte[text.remaining()];
        text.get(array);
        textarea.append(toJString(array));
    }
    
    @Override
    public void rBusy(Rengine re, int which) {
        System.out.println("rBusy("+which+")");
    }
    
    @Override
    public byte[] rReadConsole(Rengine re, byte[] prompt, int addToHistory) {
        System.out.print(toJString(prompt));
        try {
            BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
            String s=br.readLine();
            return (s == null) ? null : toRString(s + "\n");
        } catch (Exception e) {
            System.out.println("jriReadConsole exception: "+e.getMessage());
        }
        return null;
    }
    
    @Override
    public void rShowMessage(Rengine re, byte[] message) {
        System.out.println("rShowMessage \"" + toJString(message) + "\"");
    }
    
    @Override
    public void rFlushConsole(Rengine re) {
    }
    
    @Override
    public byte[] rChooseFile(Rengine re, int newFile) {
        FileDialog fd = new FileDialog(f, (newFile==0)?"Select a file":"Select a new file", (newFile==0)?FileDialog.LOAD:FileDialog.SAVE);
        fd.setVisible(true);
        String res=null;
        if (fd.getDirectory()!=null) res=fd.getDirectory();
        if (fd.getFile()!=null) res=(res==null)?fd.getFile():(res+fd.getFile());
        return toRString(res);
    }
    
    @Override
    public void rShowFiles(Rengine re, byte[][] filePaths, byte[] fileEncoding,
    		byte[] title, byte[][] headers, boolean deleteFiles, byte[] pager) {
    }
    
    @Override
    public long rExecJCommand(Rengine re, byte[] commandIdBytes, long argsP, int options) {
        System.out.println("rExecJCommand \"" + toJString(commandIdBytes) + "\"");
        return 0;
    }
    
    @Override
    public void rProcessJEvents(Rengine re) {
    }
    
}

public class rtest2 {
    public static void main(String[] args) {
        System.out.println("Press <Enter> to continue (time to attach the debugger if necessary)");
        try { System.in.read(); } catch(Exception e) {};
        System.out.println("Creating Rengine (with arguments)");
		Rengine re=new Rengine(args, true, new TextConsole2());
        System.out.println("Rengine created, waiting for R");
        if (!re.waitForR()) {
            System.out.println("Cannot load R");
            return;
        }
		System.out.println("re-routing stdout/err into R console");
		System.setOut(new PrintStream(new RConsoleOutputStream(re, 0)));
		System.setErr(new PrintStream(new RConsoleOutputStream(re, 1)));
		
		System.out.println("Letting go; use main loop from now on");
    }
}
