#include <R.h>
#include <Rinternals.h>
#include <R_ext/Rdynload.h>
#include "Rinit.h"
#include "Rcallbacks.h"
#include "Rdecl.h"


static const R_CallMethodDef callMethods[]  = {
	{ "rj_execJCommand", (DL_FUNC) &rj_execJCommand, 3 },
	{ NULL, NULL, 0 }
};

static void registerDllRoutines(void) {
	DllInfo *dllInfo= R_getEmbeddingDllInfo();
	R_registerRoutines(dllInfo, NULL, callMethods, NULL, NULL);
	R_useDynamicSymbols(dllInfo, FALSE);
}

static void runOnExitFinalizer(SEXP s) {
	Rf_KillAllDevices();
	rj_onExit();
}


/*-------------------------------------------------------------------*
 * UNIX initialization (includes Darwin/Mac OS X)                    *
 *-------------------------------------------------------------------*/

#ifndef Win32

#define R_INTERFACE_PTRS 1
#define CSTACK_DEFNS 1
#include <Rinterface.h>
#include <R_ext/eventloop.h>
/* and SaveAction is not officially exported */
extern SA_TYPE SaveAction;


int initR(int argc, char **argv,
		uint_fast64_t maxMemSize, unsigned long stackSize) {
    int ref;
    structRstart rp;
    Rstart Rp = &rp;
    /* getenv("R_HOME","/Library/Frameworks/R.framework/Resources",1); */
    if (!getenv("R_HOME")) {
        fprintf(stderr, "R_HOME is not set. Please set all required environment variables before running this program.\n");
        return 20201;
    }

    /* this is probably unnecessary, but we could set any other parameters here */
    R_DefParams(Rp);
    Rp->NoRenviron = 0;
    R_SetParams(Rp);

#ifdef RIF_HAS_RSIGHAND
    R_SignalHandlers=0;
#endif
    R_Interactive = 1;
    SaveAction = SA_SAVEASK;
    {
        int stat = Rf_initialize_R(argc, argv);
        if (stat < 0) {
            printf("Failed to initialize embedded R! (stat=%d)\n", stat);
            return 20401;
      }
    }

#ifdef RIF_HAS_RSIGHAND
    R_SignalHandlers=0;
#endif
    if (!stackSize) {
        /* disable stack checking, because threads will thow it off */
        R_CStackLimit = (uintptr_t) -1;
    } else {
        /* stack start: current position with tolerance for existing stack */
        int dir = ((uintptr_t) &ref < (uintptr_t) &dir) ? -1 : 1;
        R_CStackStart = (uintptr_t) &ref + 8192 * dir;
        R_CStackLimit = (uintptr_t) stackSize;
    }

#ifdef JGR_DEBUG
    printf("R primary initialization done. Setting up parameters.\n");
#endif

    R_Outputfile = NULL;
    R_Consolefile = NULL;
    if (!R_Interactive) {
        R_Interactive = 1;
        SaveAction = SA_SAVEASK;
    }

    /* ptr_R_Suicide = rj_suicide; */
    /* ptr_R_CleanUp = rj_cleanUp; */
	R_PolledEvents= rj_handleEvents;
	
	ptr_R_ShowMessage = rj_showMessage;
	ptr_R_Busy= rj_busy;
	
    ptr_R_ReadConsole = rj_readConsole;
    ptr_R_WriteConsoleEx = rj_writeConsoleEx;
    ptr_R_WriteConsole = NULL;
    ptr_R_ResetConsole = rj_resetConsole;
    ptr_R_FlushConsole = rj_flushConsole;
    ptr_R_ClearerrConsole = rj_clearerrConsole;
    ptr_R_loadhistory = rj_loadHistory;
    ptr_R_savehistory = rj_saveHistory;
	
	ptr_R_ChooseFile= rj_chooseFile;
	ptr_R_ShowFiles= rj_showFiles;
	
#ifdef JGR_DEBUG
    printf("Setting up R event loop\n");
#endif

    setup_Rmainloop();
	
	registerDllRoutines();
	
	R_MakeWeakRefC(R_BaseEnv, R_NilValue, (R_CFinalizer_t)runOnExitFinalizer, TRUE);
	
#ifdef JGR_DEBUG
	printf("R initialized.\n");
#endif
	return 0;
}

void initRinside(void) {
    /* disable stack checking, because threads will thow it off */
    R_CStackLimit = (uintptr_t) -1;
}

#else

/*-------------------------------------------------------------------*
 * Windows initialization is different and uses Startup.h            *
 *-------------------------------------------------------------------*/

#define NONAMELESSUNION
#include <windows.h>
#include <winreg.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

/* before we include RStatup.h we need to work around a bug in it for Win64:
   it defines wrong R_size_t if R_SIZE_T_DEFINED is not set */
#if defined(WIN64) && ! defined(R_SIZE_T_DEFINED)
#include <stdint.h>
#define R_size_t uintptr_t
#define R_SIZE_T_DEFINED 1
#endif

#include "R_ext/RStartup.h"

#ifndef WIN64
/* according to fixed/config.h Windows has uintptr_t, my windows hasn't */
#if !defined(HAVE_UINTPTR_T) && !defined(uintptr_t) && !defined(_STDINT_H)
typedef unsigned uintptr_t;
#endif
#endif
extern __declspec(dllimport) uintptr_t R_CStackLimit; /* C stack limit */
extern __declspec(dllimport) uintptr_t R_CStackStart; /* Initial stack address */

/* for signal-handling code */
/* #include "psignal.h" - it's not included, so just get SIGBREAK */
#define	SIGBREAK 21	/* to readers pgrp upon background tty read */

/* one way to allow user interrupts: called in ProcessEvents */
#ifdef _MSC_VER
__declspec(dllimport) int UserBreak;
#else
#ifndef WIN64
#define UserBreak     (*_imp__UserBreak)
#endif
extern int UserBreak;
#endif

/* calls into the R DLL */
extern char *getDLLVersion(void);
extern void R_DefParams(Rstart);
extern void R_SetParams(Rstart);
extern void cmdlineoptions(int, char**);
extern void setup_term_ui(void);
extern void ProcessEvents(void);
extern void run_Rmainloop(void);
extern void end_Rmainloop(void);

#ifndef YES
#define YES    1
#endif
#ifndef NO
#define NO    -1
#endif
#ifndef CANCEL
#define CANCEL 0
#endif

int myYesNoCancel(const char *s) {
	char ss[192];
	RCSIGN char a[3];
	unsigned int l= (unsigned int)strlen(s);
	if (l + 12 > sizeof(ss)) {
		memcpy(ss, s, sizeof(ss) - 15);
		strcpy(ss + sizeof(ss) - 15, "... [y/n/c]: ");
	} else {
		memcpy(ss, s, l);
		strcpy(ss + l, " [y/n/c]: ");
	}
    rj_readConsole(ss, a, 3, 0);
    switch (a[0]) {
    case 'y':
    case 'Y':
	return YES;
    case 'n':
    case 'N':
	return NO;
    default:
	return CANCEL;
    }
}

static void my_onintr(int sig)
{
    UserBreak = 1;
}

static char Rversion[25], RUser[MAX_PATH], RHome[MAX_PATH];

int initR(int argc, char **argv,
		uint_fast64_t maxMemSize, unsigned long stackSize) {
    structRstart rp;
    Rstart Rp = &rp;
    char *p;
    char cb[MAX_PATH + 10];
    DWORD t, s = MAX_PATH;
    HKEY k;
	
	snprintf(Rversion, sizeof(Rversion), "%s.%s", R_MAJOR, R_MINOR);
	if (strncmp(getDLLVersion(), Rversion, strlen(Rversion)-2) != 0) {
		char msg[512];
		snprintf(msg, sizeof(msg), "Error: R.ddl version does not match (DLL: %s, JRI: %s)\n", getDLLVersion(), Rversion);
		fprintf(stderr, msg);
		MessageBox(0, msg, "Version mismatch", MB_OK|MB_ICONERROR);
		return 20101;
	}
	
	// R_HOME
    if(getenv("R_HOME")) {
        strcpy(RHome, getenv("R_HOME"));
    } else { /* fetch R_HOME from the registry - try preferred architecture first */
#ifdef WIN64
        const char *pref_path = "SOFTWARE\\R-core\\R64";
#else
        const char *pref_path = "SOFTWARE\\R-core\\R32";
#endif
        if ((RegOpenKeyEx(HKEY_LOCAL_MACHINE, pref_path, 0, KEY_QUERY_VALUE, &k) != ERROR_SUCCESS ||
             RegQueryValueEx(k, "InstallPath", 0, &t, (LPBYTE) RHome, &s) != ERROR_SUCCESS) &&
            (RegOpenKeyEx(HKEY_CURRENT_USER, pref_path, 0, KEY_QUERY_VALUE, &k) != ERROR_SUCCESS ||
             RegQueryValueEx(k, "InstallPath", 0, &t, (LPBYTE) RHome, &s) != ERROR_SUCCESS) &&
            (RegOpenKeyEx(HKEY_LOCAL_MACHINE, "SOFTWARE\\R-core\\R", 0, KEY_QUERY_VALUE, &k) != ERROR_SUCCESS ||
             RegQueryValueEx(k, "InstallPath", 0, &t, (LPBYTE) RHome, &s) != ERROR_SUCCESS) &&
            (RegOpenKeyEx(HKEY_CURRENT_USER, "SOFTWARE\\R-core\\R", 0, KEY_QUERY_VALUE, &k) != ERROR_SUCCESS ||
             RegQueryValueEx(k, "InstallPath", 0, &t, (LPBYTE) RHome, &s) != ERROR_SUCCESS)) {
            fprintf(stderr, "R_HOME must be set or R properly installed (\\Software\\R-core\\R\\InstallPath registry entry must exist).\n");
            MessageBox(0, "R_HOME must be set or R properly installed (\\Software\\R-core\\R\\InstallPath registry entry must exist).\n", "Can't find R home", MB_OK|MB_ICONERROR);
            return 20201;
        }
        snprintf(cb, sizeof(cb), "R_HOME=%s", RHome);
        putenv(cb);
    }
	
	// R_USER
    if (getenv("R_USER")) {
        strcpy(RUser, getenv("R_USER"));
    } else if (getenv("HOME")) {
        strcpy(RUser, getenv("HOME"));
    } else if (getenv("HOMEDRIVE")) {
        strcpy(RUser, getenv("HOMEDRIVE"));
        strcat(RUser, getenv("HOMEPATH"));
    } else
        GetCurrentDirectory(MAX_PATH, RUser);
    p = RUser + (strlen(RUser) - 1);
    if (*p == '/' || *p == '\\') *p = '\0';
	
#if R_VERSION < R_Version(4,2,0)
	// R_MAX_MEM_SIZE
	if (maxMemSize > 0) {
		if (maxMemSize < 67108864 || maxMemSize > SIZE_MAX) {
			return 20205;
		}
		const char *unit= ""; // R parses only C long
		if (maxMemSize > LONG_MAX || maxMemSize % 1024 == 0) {
			maxMemSize= maxMemSize / 1024;
			unit= "K";
			if (maxMemSize > LONG_MAX || maxMemSize % 1024 == 0) {
				maxMemSize= maxMemSize / 1024;
				unit= "M";
				if (maxMemSize > LONG_MAX || maxMemSize % 1024 == 0) {
					maxMemSize= maxMemSize / 1024;
					unit= "G";
				}
			}
		}
		snprintf(cb, sizeof(cb), "R_MAX_MEM_SIZE=%" PRIuFAST64 "%s", maxMemSize, unit);
		putenv(cb);
	}
#endif
	
	R_DefParams(Rp);
	
	Rp->rhome = RHome;
	Rp->home = RUser;
	
    Rp->ReadConsole = rj_readConsole;
    Rp->WriteConsole = NULL;
    Rp->WriteConsoleEx = rj_writeConsoleEx;

    Rp->Busy = rj_busy;
    Rp->ShowMessage = rj_showMessage;
    Rp->YesNoCancel = myYesNoCancel;
    Rp->CallBack = rj_handleEvents;
    Rp->CharacterMode = LinkDLL;
#if R_VERSION >= R_Version(4,0,0)
	Rp->EmitEmbeddedUTF8 = FALSE;
#endif

    Rp->R_Quiet = FALSE;
    Rp->R_Interactive = TRUE;
    Rp->RestoreAction = SA_RESTORE;
    Rp->SaveAction = SA_SAVEASK;
	
	const char* rargv[] = { "R.exe" , "--vanilla" };
	cmdlineoptions(2, (char**)rargv);
	
	Rp->NoRenviron = 0;
	R_SetParams(Rp); /* so R_ShowMessage is set and other R GUI stuff is removed */
	
	Rp->NoRenviron = 1;
	R_set_command_line_arguments(argc, argv);
	/* process common command line options */
	R_common_command_line(&argc, argv, Rp);
	
	R_SizeFromEnv(Rp);
	R_SetParams(Rp);
	
	if (!stackSize) {
		/* R_SetParams implicitly calls R_SetWin32 which sets the
		   stack start/limit which we need to override */
		R_CStackLimit = (uintptr_t) -1;
	}

    FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));

    signal(SIGBREAK, my_onintr);
    setup_term_ui();
    setup_Rmainloop();
	
	registerDllRoutines();
	
	R_MakeWeakRefC(R_BaseEnv, R_NilValue, (R_CFinalizer_t)runOnExitFinalizer, TRUE);
	
#ifdef JGR_DEBUG
	printf("R initialized.\n");
#endif
	return 0;
}

void initRinside(void) {
    /* disable stack checking, because threads will thow it off */
    R_CStackLimit = (uintptr_t) -1;
}

#endif

