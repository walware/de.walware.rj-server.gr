/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the Apache License, Version 2.0 which is available at
 # https://www.apache.org/licenses/LICENSE-2.0.
 # 
 # SPDX-License-Identifier: Apache-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

package org.rosuda.JRI;


/**
 * Advanced configuration parameters for the Rengine
 */
public class RConfig {
	
	
	/**
	 * Max memory size for R in bytes (Windows & R < 4.2.0 only).
	 * 
	 * <code>0</code> = default
	 */
	public long Mem_MaxSize = 0;
	
	/**
	 * Size of stack of R main thread, Java dependent.
	 * 
	 * @see Thread#Thread(ThreadGroup, Runnable, String, long)
	 * <code>0</code> = java default
	 */
	public long MainCStack_Size = 0;
	
	/**
	 * Flag if stack limit should be set in R.
	 * 
	 * Requires {@link #MainCStack_Size}
	 */
	public boolean MainCStack_SetLimit = true;
	
	
}
