/*=============================================================================#
 # Copyright (c) 2005, 2025 Simon Urbanek and others.
 # 
 # This program and the accompanying materials are made available under the
 # terms of the GNU Lesser General Public License v2.1 or newer which
 # accompanies this distribution, and is available at
 # http://www.gnu.org/licenses/lgpl.html
 # 
 # Contributors:
 #     Simon Urbanek <simon.urbanek@r-project.org> - initial API and implementation
 #=============================================================================*/

package org.rosuda.JRI;

import java.nio.ByteBuffer;


/** Interface which must be implmented by any class that wants to pose as the call-back handler for R event loop callbacks. It is legal to return immediately except when user interaction is required: {@link #rReadConsole} and {@link #rChooseFile} are expected to block until the user performs the desired action. */
public interface RMainLoopCallbacksUtf8 {
	
	
	/**
	 * Called when setup of R engine ({@link Rengine#setupR(String[])} completed, before finally entering the main loop.
	 * 
	 * @param re the calling engine
	 */
	public void rOnSetup(Rengine re);
	
	public void rOnExit(Rengine re);
	
	
	/**
	 * Called to process events.
	 * 
	 * <p>Must be enabled by {@link Rengine#rniSetProcessJEvents(int)}.</p>
	 * 
	 * @param re the calling engine
	 */
	public void rProcessJEvents(Rengine re);
	
	
	/**
	 * Called when R want to show a warning/error message (not to be confused with messages displayed in the console output).
	 * 
	 * @param re the calling engine
	 * @param message the message to display
	 */
	public void rShowMessage(Rengine re, byte[] message);
	
	/**
	 * Called when R enters or exits a longer evaluation. It is usually a good idea to signal this state to the user, e.g. by changing the cursor to a "hourglass" and back.
	 * 
	 * @param re the calling engine
	 * @param which identifies whether R enters (1) or exits (0) the busy state
	 */
	public void rBusy(Rengine re, int which);
	
	
	/**
	 * Called when R waits for user input. During the duration of this callback it is safe to re-enter R, and very often it is also the only time. The implementation is free to block on this call until the user hits Enter, but in JRI it is a good idea to call {@link Rengine.rniIdle()} occasionally to allow other event handlers (e.g graphics device UIs) to run. Implementations should NEVER return immediately even if there is no input - such behavior will result in a fast cycling event loop which makes the use of R pretty much impossible.
	 * 
	 * @param re the calling engine
	 * @param prompt the prompt to be displayed at the console prior to user's input
	 * @param addToHistory flag telling the handler whether the input should be considered for adding to history (!=0) or not (0)
	 * @return the user's input to be passed to R for evaluation
	 */
	public byte[] rReadConsole(Rengine re, byte[] prompt, int addToHistory);
	
	/**
	 * Called when R prints output to the console.
	 * 
	 * @param re the calling engine
	 * @param text the text to display in the console
	 * @param oType the output type (0=regular, 1=error/warning)
	*/
	public void rWriteConsole(Rengine re, ByteBuffer text, int oType);
	
	/**
	 * Called when R requests the console to flush any buffered output.
	 * 
	 * @param re the calling engine
	 */	
	public void rFlushConsole(Rengine re);
	
	
	/**
	 * Called when R expects the user to choose a file.
	 * 
	 * @param re the calling engine
	 * @param newFile flag determining whether an existing or new file is to be selecteed
	 * @return the path/name of the selected file
	 */
	public byte[] rChooseFile(Rengine re, int newFile);
	
	/**
	 * Called by R to show the content of plain text files.
	 * 
	 * @param re the calling engine
	 * @param filePaths the path of the files to show
	 * @param fileEncoding the encoding of the files
	 * @param title an overall title to display
	 * @param headers headers to display for each file to show
	 * @param deleteFiles {@code true} if files should be deleted when no longer required by the
	 *     callback handler
	 * @param pager the pager to use or {@code null}
	 */
	public void rShowFiles(Rengine re,
			byte [] /*@NonNull*/ [] filePaths, byte /*@Nullable*/ [] fileEncoding,
			byte[] title, byte [] /*@Nullable*/ [] headers,
			boolean deleteFiles,
			byte /*@Nullable*/ [] pager);
	
	
	/**
	 * Called to exec a Java command.
	 * 
	 * <p>'ExecJCommand' provides a flexible, generic mechanism to execute Java commands using R.
	 * The final meaning of the parameters are not (yet) specified and application dependent.</p>
	 * 
	 * @param re the calling engine
	 * @param commandId the id to identify the command
	 * @param argsP optional command arguments as SEXP pointer
	 * @param options flags controlling the execution of the command
	 * @return command return value as SEXP pointer or 0
	 */
	public long rExecJCommand(Rengine re,
			byte[] commandId, long argsP, int options);
	
}
