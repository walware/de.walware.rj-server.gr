/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # All rights reserved. This program and the accompanying materials
 # are made available under the terms of the Apache License v2.0
 # which accompanies this distribution, and is available at
 # https://www.apache.org/licenses/LICENSE-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

// Utility methods for R package libs using JNI with R-Java classloader (from JRI or compatible)


#ifndef RJUTIL_H
#define RJUTIL_H

#include <Rdefines.h>
#include <jni.h>


// static const unsigned int
#define RJ_ERROR_MASK         0b00000111
#define RJ_ERROR_RWARNING     0b00000010
#define RJ_ERROR_RERROR       0b00000011
#define RJ_ERROR_CWARNING     0b00000100
#define RJ_GLOBAL_REF         0b00010000
#define RJ_ERR_ON_CONV        0b00100000

// static const int
#define ERR_OVERFLOW             -2
#define ERR_CONV_FAILED          -3
#define ERR_ICONV              -101


int initRJUtil(JNIEnv *env);
JNIEnv *getJEnv(void);

extern jclass jcClass;
extern jclass jcByteArray;
extern jbyteArray joByteArrayEmpty;
extern jbyteArray joByteArrayNA;


jbyteArray newJUtf8StringC(JNIEnv *env, const char *s);
jbyteArray newJUtf8StringS(JNIEnv *env, SEXP cS);

int copyJUtf8StringToBufferC(JNIEnv *env, jbyteArray bytes,
		char *dst, const int dstLength,
		unsigned int flags);

inline Rboolean isAsciiC(const char *c, const size_t length) {
	for (size_t i= 0; i < length; i++) {
		if (c[i] & 0x80) {
			return FALSE;
		}
	}
	return TRUE;
}

inline void deleteJUtf8StringLocalRef(JNIEnv *env, jbyteArray ref) {
	if (ref
			&& ref != joByteArrayEmpty && ref != joByteArrayNA) {
		(*env)->DeleteLocalRef(env, ref);
	}
}


jmethodID getJMethod(JNIEnv *env, jclass jClass, const char *name, const char *sig,
		unsigned int flags);


void handleJError(JNIEnv *env, unsigned int flags, const char *message, ...);
void checkJError(JNIEnv *env, unsigned int flags, const char *message, ...);
void clearJError(JNIEnv *env);
void handleCError(JNIEnv *env, unsigned int flags, const char *message, ...);

#define JAVA_CREATE_STRING_ERROR_MESSAGE "Failed to create Java string."


#endif
