
#include "rjutil.h"
#include "jri.h"
#include "globals.h"
#include "Rdecl.h"
#include "Rcallbacks.h"

#include "org_rosuda_JRI_Rengine.h"
#include <R_ext/Parse.h>
#include <R_ext/Riconv.h>

#include <ctype.h>
#include <stdio.h>
#include <errno.h>

#ifndef Win32
#include <R_ext/eventloop.h>
#include <R_ext/GraphicsEngine.h>
#endif

#ifdef Win32
#include <R_ext/RStartup.h>
#else
/* from Defn.h (do we still need it? rj_cleanUp is commented out ...)
   extern Rboolean R_Interactive; */
#endif

void Rf_checkArityCall(SEXP op, SEXP args, SEXP call); /* include/Defn.h */

#ifndef errorcall
#define errorcall                Rf_errorcall
#endif

#define RJ_CALLBACK_ERROR_MESSAGE "An error occurred in Java callback."


#define BYTE_BUFFER_LENGTH 0x4000
static char byteBuffer[BYTE_BUFFER_LENGTH];

static jmethodID jmRengine_jriOnExit;
static jmethodID jmRengine_jriProcessJEvents;
static jmethodID jmRengine_jriShowMessage;
static jmethodID jmRengine_jriBusy;
static jmethodID jmRengine_jriReadConsole;
static jmethodID jmRengine_jriWriteConsole;
static jmethodID jmRengine_jriFlushConsole;
static jmethodID jmRengine_jriChooseFile;
static jmethodID jmRengine_jriShowFiles;
static jmethodID jmRengine_jriExecJCommand;


int rj_initCallbacks(JNIEnv *env) {
	jmRengine_jriOnExit= getJMethod(env,
			engineClass, "jriOnExit", "()V",
			RJ_ERROR_CWARNING );
	jmRengine_jriProcessJEvents= getJMethod(env,
			engineClass, "jriProcessJEvents", "()V",
			RJ_ERROR_CWARNING );
	jmRengine_jriShowMessage= getJMethod(env,
			engineClass, "jriShowMessage", "([B)V",
			RJ_ERROR_CWARNING );
	jmRengine_jriBusy= getJMethod(env,
			engineClass, "jriBusy", "(I)V",
			RJ_ERROR_CWARNING );
	jmRengine_jriReadConsole= getJMethod(env,
			engineClass, "jriReadConsole", "([BI)[B",
			RJ_ERROR_CWARNING );
	jmRengine_jriWriteConsole= getJMethod(env,
			engineClass, "jriWriteConsole", "(Ljava/nio/ByteBuffer;I)V",
			RJ_ERROR_CWARNING );
	jmRengine_jriFlushConsole= getJMethod(env,
			engineClass, "jriFlushConsole", "()V",
			RJ_ERROR_CWARNING );
	jmRengine_jriChooseFile= getJMethod(env,
			engineClass, "jriChooseFile", "(I)[B",
			RJ_ERROR_CWARNING );
	jmRengine_jriShowFiles= getJMethod(env,
			engineClass, "jriShowFiles", "([[B[B[B[[BZ[B)V",
			RJ_ERROR_CWARNING );
	jmRengine_jriExecJCommand= getJMethod(env,
			engineClass, "jriExecJCommand", "([BJI)J",
			RJ_ERROR_CWARNING );
	
	return (jmRengine_jriOnExit
			&& jmRengine_jriProcessJEvents
			&& jmRengine_jriShowMessage
			&& jmRengine_jriBusy
			&& jmRengine_jriReadConsole && jmRengine_jriWriteConsole
			&& jmRengine_jriFlushConsole
			&& jmRengine_jriChooseFile && jmRengine_jriShowFiles
			&& jmRengine_jriExecJCommand );
}

void rj_onExit(void) {
	JNIEnv *env= getJEnv();
	if (!env) {
		return;
	}
	clearJError(env);
	
	(*env)->CallVoidMethod(env, engineObj, jmRengine_jriOnExit);
}

/*
 R_CleanUp is invoked at the end of the session to give the user the
 option of saving their data.
 If ask == SA_SAVEASK the user should be asked if possible (and this
                                                            option should not occur in non-interactive use).
 If ask = SA_SAVE or SA_NOSAVE the decision is known.
 If ask = SA_DEFAULT use the SaveAction set at startup.
 In all these cases run .Last() unless quitting is cancelled.
 If ask = SA_SUICIDE, no save, no .Last, possibly other things.
 */

/*
void rj_cleanUp(SA_TYPE saveact, int status, int runLast)
{
    unsigned char buf[1024];
    char * tmpdir;

    if(saveact == SA_DEFAULT)
        saveact = SaveAction;

    if(saveact == SA_SAVEASK) {
        if(R_Interactive) {
qask:
            R_ClearerrConsole();
            R_FlushConsole();
            R_ReadConsole("Save workspace image? [y/n/c]: ",
                          buf, 128, 0);
            switch (buf[0]) {
                case 'y':
                case 'Y':
                    saveact = SA_SAVE;
                    break;
                case 'n':
                case 'N':
                    saveact = SA_NOSAVE;
                    break;
                case 'c':
                case 'C':
                    jump_to_toplevel();
                    break;
                default:
                    goto qask;
            }
        } else
            saveact = SaveAction;
    }
    switch (saveact) {
        case SA_SAVE:
            if(runLast) R_dot_Last();
            if(R_DirtyImage) R_SaveGlobalEnv();
                 stifle_history(R_HistorySize);
                 write_history(R_HistoryFile);
                 break;
        case SA_NOSAVE:
            if(runLast) R_dot_Last();
            break;
        case SA_SUICIDE:
        default:
            break;
    }
    R_RunExitFinalizers();
    CleanEd();
    if(saveact != SA_SUICIDE) KillAllDevices();
    if((tmpdir = getenv("R_SESSION_TMPDIR"))) {
        snprintf((char *)buf, 1024, "rm -rf %s", tmpdir);
        R_system((char *)buf);
    }
    if(saveact != SA_SUICIDE && R_CollectWarnings)
        PrintWarnings();
    fpu_setup(FALSE);

    exit(status);
}

void Rstd_Suicide(char *s)
{
    REprintf("Fatal error: %s\n", s);
    R_CleanUp(SA_SUICIDE, 2, 0);
}
*/


int rj_processJEventsRequested;

static void processJEvents(void) {
	JNIEnv *env= getJEnv();
	if (!env) {
		return;
	}
	clearJError(env);
	
	rj_processJEventsRequested= 0;
	(*env)->CallVoidMethod(env, engineObj, jmRengine_jriProcessJEvents);
	
	checkJError(env, RJ_ERROR_CWARNING, "rj_handleEvents: " RJ_CALLBACK_ERROR_MESSAGE);
}

void rj_handleEvents(void) {
	if (rj_processJEventsRequested) {
		processJEvents();
	}
}


void rj_showMessage(const char *buf) {
#ifdef JRI_DEBUG
	printf("rj_showMessage ENTRY msg= \"%s\"\n", buf); fflush(stdout);
#endif
	JNIEnv *env=getJEnv();
	if (!env) {
		return;
	}
	clearJError(env);
	
	jbyteArray jMessage= newJUtf8StringC(env, buf);
	if (!jMessage) {
		handleJError(env, RJ_ERROR_CWARNING, "rj_showMessage.message: " JAVA_CREATE_STRING_ERROR_MESSAGE);
		return;
	}
	(*env)->CallVoidMethod(eenv, engineObj, jmRengine_jriShowMessage,
			jMessage );
	deleteJUtf8StringLocalRef(env, jMessage);
	
	checkJError(env, RJ_ERROR_CWARNING, "rj_showMessage: " RJ_CALLBACK_ERROR_MESSAGE);
}

void rj_busy(int which) {
	JNIEnv *env= getJEnv();
	if (!env) {
		return;
	}
	clearJError(env);
	
	(*env)->CallVoidMethod(env, engineObj, jmRengine_jriBusy,
			which );
	
	checkJError(env, RJ_ERROR_CWARNING, "rj_busy: " RJ_CALLBACK_ERROR_MESSAGE);
}


int rj_readConsole(const char *prompt, RCSIGN char *buf, int len,
		int addtohistory) {
#ifdef JRI_DEBUG
	printf("rj_readConsole ENTRY prompt= \"%s\"\n", prompt); fflush(stdout);
#endif
	JNIEnv *env= getJEnv();
	if (!env) {
		return 0;
	}
	clearJError(env);
	
	jbyteArray jPrompt= newJUtf8StringC(env, prompt);
	if (!jPrompt) {
		handleJError(env, RJ_ERROR_CWARNING, "rj_readConsole.prompt: " JAVA_CREATE_STRING_ERROR_MESSAGE);
		jPrompt= joByteArrayNA;
	}
	
	jbyteArray jInput= (jbyteArray)(*env)->CallObjectMethod(env, engineObj, jmRengine_jriReadConsole,
			jPrompt, addtohistory );
	deleteJUtf8StringLocalRef(env, jPrompt);
	
	checkJError(env, RJ_ERROR_CWARNING, "rj_readConsole: " RJ_CALLBACK_ERROR_MESSAGE);
	if (!jInput) {
		return 0;
	}
	int r= copyJUtf8StringToBufferC(env, jInput, (char *)buf, len, 0);
	deleteJUtf8StringLocalRef(env, jInput);
#ifndef Win32
	if (R_interrupts_pending) {
		R_CheckUserInterrupt();
	}
#endif
	if (r >= 0) {
#ifdef JRI_DEBUG
		printf("rj_readConsole succeeded: \"%s\"\n", buf);
#endif
		return r;
	}
	else if (r == ERR_OVERFLOW) {
		handleJError(env, RJ_ERROR_CWARNING, "rj_readConsole.input: String too long.");
		return 0;
	}
	else if (r == ERR_ICONV) {
		handleJError(env, RJ_ERROR_CWARNING, "rj_readConsole.input: Init iconv failed.");
		return 0;
	}
	else {
		handleJError(env, RJ_ERROR_CWARNING, "rj_readConsole.input: Getting string failed.");
		return 0;
	}
}

static void writeConsoleUtf8(JNIEnv *env, const char *src, int srcLength, int oType) {
	jobject jOutput= (*env)->NewDirectByteBuffer(env, (char *)src, srcLength);
	
	(*env)->CallVoidMethod(env, engineObj, jmRengine_jriWriteConsole,
			jOutput, oType );
	(*env)->DeleteLocalRef(env, jOutput);
	
	checkJError(env, RJ_ERROR_CWARNING, "rj_writeConsole: " RJ_CALLBACK_ERROR_MESSAGE);
}

void rj_writeConsoleEx(const char *buf, int len, int oType) {
#ifdef JRI_DEBUG
	printf("rj_writeConsole ENTRY len= %d oType= %d\n", len, oType); fflush(stdout);
#endif
	JNIEnv *env= getJEnv();
	if (!env) {
		return;
	}
	clearJError(env);
	
	if (utf8locale || isAsciiC(buf, len)) {
		writeConsoleUtf8(env, buf, len, oType);
	}
	else {
		void *iconv= Riconv_open("UTF-8", "");
		if (iconv == (void *)(-1)) {
			handleCError(env, RJ_ERROR_CWARNING, "rj_writeConsole.output: Init iconv failed.");
		}
		else {
			const char *in= buf;
			size_t inRemaining= len;
			char *out= byteBuffer;
			size_t outRemaining= BYTE_BUFFER_LENGTH;
			while (inRemaining > 0) {
				size_t res= Riconv(iconv, &in, &inRemaining, &out, &outRemaining);
				if (res == -1) {
					if (errno == E2BIG) {
						writeConsoleUtf8(env, byteBuffer, BYTE_BUFFER_LENGTH - outRemaining, oType);
						out= byteBuffer;
						outRemaining= BYTE_BUFFER_LENGTH;
						continue;
					}
					if (errno == EILSEQ || errno == EINVAL) {
						if (outRemaining < 5) {
							writeConsoleUtf8(env, byteBuffer, BYTE_BUFFER_LENGTH - outRemaining, oType);
							out= byteBuffer;
							outRemaining= BYTE_BUFFER_LENGTH;
						}
						snprintf(out, 5, "<%02X>", (unsigned char)in[0]);
						out+= 4; outRemaining-= 4;
						in++; inRemaining--;
						continue;
					}
				}
				writeConsoleUtf8(env, byteBuffer, BYTE_BUFFER_LENGTH - outRemaining, oType);
				break;
			}
			Riconv_close(iconv);
		}
	}
}

/* old-style WriteConsole (for old R versions only) */
void rj_writeConsole(const char *buf, int len) {
	rj_writeConsoleEx(buf, len, 0);
}

/* Indicate that input is coming from the console */
void rj_resetConsole(void) {
}

/* Stdio support to ensure the console file buffer is flushed */
void rj_flushConsole(void) {
	JNIEnv *env=getJEnv();
	if (!env) {
		return;
	}
	clearJError(env);
	
	(*env)->CallVoidMethod(env, engineObj, jmRengine_jriFlushConsole);
	
	checkJError(env, RJ_ERROR_CWARNING, "rj_flushConsole: " RJ_CALLBACK_ERROR_MESSAGE);
}

/* Reset stdin if the user types EOF on the console. */
void rj_clearerrConsole(void) {
}


int rj_chooseFile(int new, char *buf, int len) {
	JNIEnv *env= getJEnv();
	if (!env) {
		return 0;
	}
	clearJError(env);
	
	jbyteArray jPath= (jbyteArray)(*env)->CallObjectMethod(env, engineObj,
			jmRengine_jriChooseFile, new );
	
	checkJError(env, RJ_ERROR_RERROR, "rj_chooseFile: " RJ_CALLBACK_ERROR_MESSAGE);
	if (!jPath) {
		return 0;
	}
	int r= copyJUtf8StringToBufferC(env, jPath, buf, len, RJ_ERR_ON_CONV);
	deleteJUtf8StringLocalRef(env, jPath);
	if (r >= 0) {
#ifdef JRI_DEBUG
		printf("rj_chooseFile succeeded: \"%s\"\n", buf);
#endif
		return r;
	}
	else if (r == ERR_OVERFLOW) {
		return len;
	}
	else {
		handleJError(env, RJ_ERROR_RERROR, "Conversion of path to native encoding failed (%d).", r);
		return 0;
	}
}

int rj_showFiles(int nfile, const char **filePaths,
		const char **headers, const char *title,
		Rboolean deleteFiles,
		const char *pager) {
	JNIEnv *env= getJEnv();
	if (!env) {
		return 0;
	}
	
	jobjectArray jFileNames= (*env)->NewObjectArray(env, nfile, jcByteArray, NULL);
	if (!jFileNames) {
		handleJError(env, RJ_ERROR_RERROR, "rj_showFiles.filePaths: jni.NewObjectArray(%d, byte[]) failed.", nfile);
		return 0;
	}
	for (int i = 0; i < nfile; i++) {
		jbyteArray js= newJUtf8StringC(env, filePaths[i]);
		if (!js) {
			(*env)->DeleteLocalRef(env, jFileNames);
			handleJError(env, RJ_ERROR_RERROR, "rj_showFiles.filePaths[i]: " JAVA_CREATE_STRING_ERROR_MESSAGE);
			return 0;
		}
		(*env)->SetObjectArrayElement(env, jFileNames, i, js);
		deleteJUtf8StringLocalRef(env, js);
	}
	jbyteArray jTitle= newJUtf8StringC(env, title);
	if (!jTitle) {
		(*env)->DeleteLocalRef(env, jFileNames);
		handleJError(env, RJ_ERROR_RERROR, "rj_showFiles.headers[i]: " JAVA_CREATE_STRING_ERROR_MESSAGE);
		return 0;
	}
	jobjectArray jHeaders=  (*env)->NewObjectArray(env, nfile, jcByteArray, NULL);
	if (!jHeaders) {
		(*env)->DeleteLocalRef(env, jFileNames);
		handleJError(env, RJ_ERROR_RERROR, "rj_showFiles.headers: jni.NewObjectArray(%d, byte[]) failed.", nfile);
		return 0;
	}
	for (int i = 0; i < nfile; i++) {
		if (headers[i] && *headers[i]) {
			jbyteArray js= newJUtf8StringC(env, headers[i]);
			if (!js) {
				handleJError(env, RJ_ERROR_RWARNING, "rj_showFiles.headers[i]: " JAVA_CREATE_STRING_ERROR_MESSAGE);
				continue;
			}
			if (js == joByteArrayNA) {
				continue;
			}
			(*env)->SetObjectArrayElement(env, jHeaders, i, js);
			deleteJUtf8StringLocalRef(env, js);
		}
	}
	jbyteArray jPager= NULL;
	if (pager && *pager) {
		jPager= newJUtf8StringC(env, pager);
		if (!jPager) {
			handleJError(env, RJ_ERROR_RWARNING, "rj_showFiles.pager: " JAVA_CREATE_STRING_ERROR_MESSAGE);
		}
	}
	
	(*env)->CallVoidMethod(env, engineObj, jmRengine_jriShowFiles,
			jFileNames, NULL,
			jTitle, jHeaders,
			(deleteFiles) ? JNI_TRUE : JNI_FALSE,
			jPager );
	(*env)->DeleteLocalRef(env, jFileNames);
	(*env)->DeleteLocalRef(env, jHeaders);
	deleteJUtf8StringLocalRef(env, jTitle);
	deleteJUtf8StringLocalRef(env, jPager);
	
	checkJError(env, RJ_ERROR_RERROR, "rj_showFiles: " RJ_CALLBACK_ERROR_MESSAGE);
	return 1;
}


SEXP rj_execJCommand(SEXP commandId, SEXP args, SEXP options) {
	if (!Rf_isString(commandId) || XLENGTH(commandId) != 1) {
		error("comandId is not a single string");
	}
	if (!Rf_isInteger(options) || XLENGTH(options) != 1) {
		error("options is not a single integer");
	}
	
	JNIEnv *env= getJEnv();
	if (!env) {
		return R_NilValue;
	}
	clearJError(env);
	
	jbyteArray jCommandId= newJUtf8StringS(env, STRING_ELT(commandId, 0));
	if (!jCommandId) {
		handleJError(env, RJ_ERROR_RERROR, "rj_execJCommand.commandId: " JAVA_CREATE_STRING_ERROR_MESSAGE);
		return R_NilValue;
	}
	
	SEXP result= 0;
	if (jCommandId != joByteArrayNA) {
		result= L2SEXP(
				(*env)->CallLongMethod(env, engineObj, jmRengine_jriExecJCommand,
						jCommandId, SEXP2L(args), (jint)INTEGER_ELT(options, 0) ));
		deleteJUtf8StringLocalRef(env, jCommandId);
		
		checkJError(env, RJ_ERROR_RERROR, "rj_execJCommand: " RJ_CALLBACK_ERROR_MESSAGE);
	}
	
	return (result) ? result : R_NilValue;
}


void rj_loadHistory(SEXP call, SEXP op, SEXP args, SEXP rho) {
	SEXP sfile;
	const char *p;

	Rf_checkArityCall(op, args, call);
	sfile = CAR(args);
	if (!Rf_isString(sfile) || XLENGTH(sfile) < 1)
		errorcall(call, "invalid file argument");
	p = R_ExpandFileName(Rf_translateChar(STRING_ELT(sfile, 0)));
	if (strlen(p) > PATH_MAX - 1)
		errorcall(call, "file argument is too long");
	
	SEXP commandId, commandArgs, options;
	PROTECT(commandId= Rf_ScalarString(Rf_mkChar("ext:console/loadHistory")));
	PROTECT(commandArgs= Rf_allocVector(VECSXP, 1));
	SET_VECTOR_ELT(commandArgs, 0, Rf_ScalarString(Rf_mkChar(p)));
	Rf_setAttrib(commandArgs, R_NamesSymbol, Rf_ScalarString(Rf_mkChar("filename")));
	PROTECT(options= Rf_ScalarInteger(0));
	
	rj_execJCommand(commandId, commandArgs, options);
	
	UNPROTECT(3);
}

void rj_saveHistory(SEXP call, SEXP op, SEXP args, SEXP rho) {
	SEXP sfile;
	const char *p;
	
	Rf_checkArityCall(op, args, call);
	sfile = CAR(args);
	if (!Rf_isString(sfile) || XLENGTH(sfile) < 1)
		errorcall(call, "invalid file argument");
	p = R_ExpandFileName(Rf_translateChar(STRING_ELT(sfile, 0)));
	if (strlen(p) > PATH_MAX - 1)
		Rf_errorcall(call, "file argument is too long");
	
	SEXP commandId, commandArgs, options;
	PROTECT(commandId= Rf_ScalarString(Rf_mkChar("ext:console/saveHistory")));
	PROTECT(commandArgs= Rf_allocVector(VECSXP, 1));
	SET_VECTOR_ELT(commandArgs, 0, Rf_ScalarString(Rf_mkChar(p)));
	Rf_setAttrib(commandArgs, R_NamesSymbol, Rf_ScalarString(Rf_mkChar("filename")));
	PROTECT(options= Rf_ScalarInteger(0));
	
	rj_execJCommand(commandId, commandArgs, options);
	
	UNPROTECT(3);
}
