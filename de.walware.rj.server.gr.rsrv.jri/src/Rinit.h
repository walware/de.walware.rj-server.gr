#ifndef __R_INIT__H__
#define __R_INIT__H__

#include <inttypes.h>

int initR(int argc, char **argv,
		uint_fast64_t maxMemSize, unsigned long stackSize);
void initRinside(void);

#endif
