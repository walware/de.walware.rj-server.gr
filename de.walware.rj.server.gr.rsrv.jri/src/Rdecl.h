#ifndef __RDECL_H__
#define __RDECL_H__

/* declarations from R internals or other include files */
/* last update: R 2.4.0 */

extern Rboolean utf8locale; /* include/Defn.h */
void run_Rmainloop(void); /* main/main.c */
int  Rf_initialize_R(int ac, char **av); /* include/Rembedded.h */
void Rf_KillAllDevices(void); /* include/Rembedded.h */

#endif
