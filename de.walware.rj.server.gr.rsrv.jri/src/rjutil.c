/*=============================================================================#
 # Copyright (c) 2011, 2025 Stephan Wahlbrink and others.
 # 
 # All rights reserved. This program and the accompanying materials
 # are made available under the terms of the Apache License v2.0
 # which accompanies this distribution, and is available at
 # https://www.apache.org/licenses/LICENSE-2.0
 # 
 # Contributors:
 #     Stephan Wahlbrink <sw@wahlbrink.eu> - initial API and implementation
 #=============================================================================*/

#include "rjutil.h"
#include "Rdecl.h"

#include <R_ext/Error.h>
#include <R_ext/Riconv.h>

#include <errno.h>


#define RJ_MESSAGE_PREFIX "[RJ-RSrv.JRI/JNI] "


static int rjinit= 0;

static JavaVM *jVM;

jclass jcClass= NULL;
jclass jcByteArray= NULL;

jbyteArray joByteArrayEmpty= NULL;
jbyteArray joByteArrayNA= NULL;


static int rj_init(JNIEnv *env) {
	jclass jc;
	jobject jo;
	
	(*env)->GetJavaVM(env, &jVM);
	
	const char *errorMessage= "Failed to init JNI utils (%s).";
	
	jc= (*env)->FindClass(env, "java/lang/Class");
	if (!jc) {
		handleJError(env, RJ_ERROR_CWARNING, errorMessage, "jni.FindClass(Class)");
		return -1;
	}
	jcClass= (*env)->NewGlobalRef(env, jc);
	if (!jcClass) {
		handleJError(env, RJ_ERROR_CWARNING, errorMessage, "jni.NewGlobalRef(jcClass)");
		return -1;
	}
	(*env)->DeleteLocalRef(env, jc);
	
	jc= (*env)->FindClass(env, "[B");
	if (!jc) {
		handleJError(env, RJ_ERROR_CWARNING, errorMessage, "jni.FindClass(byte[])");
		return -1;
	}
	jcByteArray= (*env)->NewGlobalRef(env, jc);
	if (!jcByteArray) {
		handleJError(env, RJ_ERROR_CWARNING, errorMessage, "jni.NewGlobalRef(jcByteArray)");
		return -1;
	}
	(*env)->DeleteLocalRef(env, jc);
	
	jo= (*env)->NewByteArray(env, 0);
	if (!jo) {
		handleJError(env, RJ_ERROR_CWARNING, errorMessage, "jni.NewByteArray(0)");
		return -31;
	}
	joByteArrayEmpty= (*env)->NewGlobalRef(env, jo);
	if (!joByteArrayEmpty) {
		handleJError(env, RJ_ERROR_CWARNING, errorMessage, "jni.NewGlobalRef(joByteArrayEmpty)");
		return -31;
	}
	(*env)->DeleteLocalRef(env, jo);
	
	jo= (*env)->NewByteArray(env, 0);
	if (!jo) {
		handleJError(env, RJ_ERROR_CWARNING, errorMessage, "jni.NewByteArray(0)");
		return -32;
	}
	joByteArrayNA= (*env)->NewGlobalRef(env, jo);
	if (!joByteArrayNA) {
		handleJError(env, RJ_ERROR_CWARNING, errorMessage, "jni.NewGlobalRef(joByteArrayNA)");
		return -32;
	}
	(*env)->DeleteLocalRef(env, jo);
	
	return 1;
}

int initRJUtil(JNIEnv *env) {
	if (rjinit != 1) {
		rjinit= rj_init(env);
#ifdef JRI_DEBUG
		printf("initRJUtil: %d\n", rjinit);
		fflush(stdout);
#endif
	}
	return (rjinit == 1);
}

JNIEnv *getJEnv(void) {
	JNIEnv *env;
	jint rc;
	
	if (!jVM) {
		jsize n;
		if ((rc= JNI_GetCreatedJavaVMs(&jVM, 1, &n)) != JNI_OK) {
			handleJError(0, RJ_ERROR_CWARNING, "Failed to get Java VM (return.code= %d).", (int) rc);
			return NULL;
		}
		if (n < 1) {
			return NULL;
		}
	}
	
	if ((rc= (*jVM)->AttachCurrentThread(jVM, (void**) &env, 0)) != JNI_OK) {
		handleJError(0, RJ_ERROR_CWARNING, "Failed to attach thread to Java VM (return.code= %d).", (int) rc);
		return NULL;
	}
	return env;
}


static int getCodepointUtf8(const unsigned char *c, const size_t length) {
	if (length > 0) {
		if (c[0] & (unsigned char)0x80) {
			if (c[0] & (unsigned char)0x40) {
				if (c[0] & (unsigned char)0x20) {
					if (c[0] & (unsigned char)0x10) {
						if (!(c[0] & (unsigned char)0x08) && length >= 4 // 11110xxx
								&& (c[1] & (unsigned char)0xC0) == (unsigned char)0x80
								&& (c[2] & (unsigned char)0xC0) == (unsigned char)0x80
								&& (c[3] & (unsigned char)0xC0) == (unsigned char)0x80 ) {
							return (((c[0] << 6
									^ c[1]) << 6
									^ c[2]) << 6
									^ c[3])
									^ ((((unsigned char)0xF0 << 6 ^ (unsigned char)0x80) << 6 ^ (unsigned char)0x80) << 6 ^ (unsigned char)0x80);
						}
					}
					else if (length >= 3  // 1110xxxx
							&& (c[1] & (unsigned char)0xC0) == (unsigned char)0x80
							&& (c[2] & (unsigned char)0xC0) == (unsigned char)0x80 ) {
						return ((c[0] << 6
								^ c[1]) << 6
								^ c[2])
								^ (((unsigned char)0xE0 << 6 ^ (unsigned char)0x80) << 6 ^ (unsigned char)0x80);
					}
				}
				else if (length >= 2  // 110xxxxx
						&& (c[1] & (unsigned char)0xC0) == (unsigned char)0x80 ) {
					return (c[0] << 6
							^ c[1])
							^ ((unsigned char)0xC0 << 6 ^ (unsigned char)0x80);
				}
			}
		}
		else { // 0xxxxxxx
			return c[0];
		}
	}
	return -1;
}

static int getCodepointUtf8Length(const int cp) {
	if (cp <= 0) {
		return 0;
	}
	else if (cp <= 0x007F) {
		return 1;
	}
	else if (cp <= 0x07FF) {
		return 2;
	}
	else if (cp <= 0xFFFF) {
		return 3;
	}
	else {
		return 4;
	}
}

jbyteArray newJUtf8StringC(JNIEnv *env, const char *nc) {
	if (!nc[0]) {
		return joByteArrayEmpty;
	}
	const void *vmax= vmaxget();
	const char *c= (utf8locale) ? nc : Rf_reEnc(nc, CE_NATIVE, CE_UTF8, 1);
	const size_t length= strlen(c);
	if (!length) {
		vmaxset(vmax);
		return joByteArrayEmpty;
	}
	jbyteArray bytes= (*env)->NewByteArray(env, length);
	if (bytes) {
		(*env)->SetByteArrayRegion(env, bytes, 0, length, (jbyte *)c);
	}
	vmaxset(vmax);
	return bytes;
}

jbyteArray newJUtf8StringS(JNIEnv *env, SEXP cS) {
	if (!cS) {
		return NULL;
	}
	if (cS == R_NaString) {
		return joByteArrayNA;
	}
	if (cS == R_BlankString) {
		return joByteArrayEmpty;
	}
	const void *vmax= vmaxget();
	const char *rc= CHAR(cS);
	cetype_t cType= Rf_getCharCE(cS);
	const char *c= (cType == CE_UTF8 || (utf8locale && cType == CE_NATIVE)) ?
			rc : Rf_reEnc(rc, cType, CE_UTF8, 1);
	const size_t length= (c == rc) ? LENGTH(cS) : strlen(c);
	jbyteArray bytes= (*env)->NewByteArray(env, length);
	if (!bytes) {
		vmaxset(vmax);
		return NULL;
	}
	(*env)->SetByteArrayRegion(env, bytes, 0, length, (jbyte *)c);
	vmaxset(vmax);
	return bytes;
}

int copyJUtf8StringToBufferC(JNIEnv *env, jbyteArray bytes,
		char *dst, const int dstLength,
		unsigned int flags) {
	const size_t length= (*env)->GetArrayLength(env, bytes);
	if (!length) {
		dst[0]= '\0';
		return 0;
	}
	const char *c= (const char *)(*env)->GetByteArrayElements(env, bytes, NULL);
	if (!c) {
		dst[0]= '\0';
		return -1;
	}
	int r= 0;
	if (utf8locale || isAsciiC(c, length)) {
		if (length > dstLength - 1) {
			r= ERR_OVERFLOW;
		}
		else {
			strncpy(dst, c, length);
			r= length;
		}
	}
	else {
		void *iconv= Riconv_open("", "UTF-8");
		if (iconv == (void *)(-1)) {
			r= ERR_ICONV;
		}
		else {
			const char *in= c;
			size_t inRemaining= length;
			char *out= dst;
			size_t outRemaining= dstLength - 1;
			while (inRemaining > 0) {
				size_t res= Riconv(iconv, &in, &inRemaining, &out, &outRemaining);
				if (res == -1) {
					if (errno == E2BIG) {
						r= ERR_OVERFLOW;
						break;
					}
					if (errno == EILSEQ || errno == EINVAL) {
						if ((flags & RJ_ERR_ON_CONV)) {
							r= ERR_CONV_FAILED;
							break;
						}
						int cp= getCodepointUtf8((const unsigned char *)in, inRemaining);
						if (cp > 0) {
							if (outRemaining < 15) {
								r= ERR_OVERFLOW;
								break;
							}
							int n= snprintf(out, 15, "\\U{%04X}", (unsigned int)cp);
							out+= n; outRemaining-= n;
							n= getCodepointUtf8Length(cp);
							in+= n; inRemaining-= n;
						}
						else {
							if (outRemaining < 5) {
								r= ERR_OVERFLOW;
								break;
							}
							snprintf(out, 5, "<%02X>", (unsigned char)in[0]);
							out+= 4; outRemaining-= 4;
							in++; inRemaining--;
						}
						continue;
					}
				}
				r= dstLength - 1 - outRemaining;
				break;
			}
			Riconv_close(iconv);
		}
	}
	dst[(r >= 0) ? r : 0]= '\0';
	(*env)->ReleaseByteArrayElements(env, bytes, (jbyte *)c, JNI_ABORT);
	return r;
}


jmethodID getJMethod(JNIEnv *env, jclass jClass, const char *name, const char *sig,
		unsigned int flags) {
	jmethodID jm;
	
	jm= (*env)->GetMethodID(env, jClass, name, sig);
	if (!jm) {
		handleJError(env, flags, "getJMethod: Failed to get Java method '%s'.", name);
		return NULL;
	}
	
#ifdef JRI_DEBUG
	printf("getJMethod: '%s' -> class= %p, mid= %p\n", sig, jClass, jm);
	fflush(stdout);
#endif
	
	return jm;
}


static void handleError(unsigned int flags, const char *msg) {
	switch (flags & RJ_ERROR_MASK) {
	case RJ_ERROR_RWARNING:
		Rf_warning(RJ_MESSAGE_PREFIX "%s", msg);
		return;
	case RJ_ERROR_RERROR:
		Rf_error(RJ_MESSAGE_PREFIX "%s", msg);
		return;
	default:
		fprintf(stderr, RJ_MESSAGE_PREFIX "%s\n", msg);
		fflush(stderr);
		return;
	}
}

void handleJError(JNIEnv *env, unsigned int flags, const char *message, ...) {
	if (env && (*env)->ExceptionCheck(env) == JNI_TRUE) {
#ifdef JRI_DEBUG
		(*env)->ExceptionDescribe(env);
#endif
		(*env)->ExceptionClear(env);
	}
	
	if ((flags & RJ_ERROR_MASK)) {
		va_list ap;
		char msg[1024];
		msg[1023]= 0;
		
		va_start(ap, message);
		vsnprintf(msg, sizeof(msg), message, ap);
		va_end(ap);
		
		handleError(flags, msg);
	}
}

void checkJError(JNIEnv *env, unsigned int flags, const char *message, ...) {
	if (env && (*env)->ExceptionCheck(env) == JNI_TRUE) {
#ifdef JRI_DEBUG
		(*env)->ExceptionDescribe(env);
#endif
		(*env)->ExceptionClear(env);
	}
	else {
		return;
	}
	
	if ((flags & RJ_ERROR_MASK)) {
		va_list ap;
		char msg[1024];
		msg[1023]= 0;
		
		va_start(ap, message);
		vsnprintf(msg, sizeof(msg), message, ap);
		va_end(ap);
		
		handleError(flags, msg);
	}
}

void clearJError(JNIEnv *env) {
	if (env && (*env)->ExceptionCheck(env) == JNI_TRUE) {
#ifdef JRI_DEBUG
		(*env)->ExceptionDescribe(env);
#endif
		(*env)->ExceptionClear(env);
	}
}

void handleCError(JNIEnv *env, unsigned int flags, const char *message, ...) {
	if ((flags & RJ_ERROR_MASK)) {
		va_list ap;
		char msg[1024];
		msg[1023]= 0;
		
		va_start(ap, message);
		vsnprintf(msg, sizeof(msg), message, ap);
		va_end(ap);
		
		handleError(flags, msg);
	}
}

