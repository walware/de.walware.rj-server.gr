#ifndef __R_CALLBACKS__H__
#define __R_CALLBACKS__H__

#include <R.h>
#include <Rinternals.h>
#include <Rversion.h>
#include <jni.h>

/* functions provided as R callbacks */

#if defined Win32 && R_VERSION < R_Version(4,2,0)
#define RCSIGN
#else
#define RCSIGN unsigned
#endif

int rj_initCallbacks(JNIEnv *env);
void rj_onExit(void);

extern int rj_processJEventsRequested;
void rj_handleEvents(void);

void rj_showMessage(const char *buf);
void rj_busy(int which);

int  rj_readConsole(const char *prompt, RCSIGN char *buf, int len,
		int addtohistory);
void rj_writeConsoleEx(const char *buf, int len, int oType);
void rj_writeConsole(const char *buf, int len);
void rj_resetConsole(void);
void rj_flushConsole(void);
void rj_clearerrConsole(void);
void rj_loadHistory(SEXP call, SEXP op, SEXP args, SEXP rho);
void rj_saveHistory(SEXP call, SEXP op, SEXP args, SEXP rho);

int  rj_chooseFile(int new, char *buf, int len);
int  rj_showFiles(int nfile, const char **filePaths, const char **headers,
		const char *title, Rboolean deleteFiles, const char *pager);

SEXP rj_execJCommand(SEXP commandId, SEXP args, SEXP options);

#endif
