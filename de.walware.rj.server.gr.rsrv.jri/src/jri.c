#define USE_RINTERNALS 1  /* for efficiency */

#include "jri.h"
#include "rjutil.h"

#include <R_ext/Parse.h>

#include <stdarg.h>


/* debugging output (enable with -DRJ_DEBUG) */
#ifdef RJ_DEBUG
static void rjprintf(char *fmt, ...) {
  va_list v;
  va_start(v,fmt);
  vprintf(fmt,v);
  va_end(v);
}
#define _dbg(X) X
#else
#define _dbg(X)
#endif

void jri_error(char *fmt, ...) {
    va_list v;
    va_start(v,fmt);
    vprintf(fmt,v);
    va_end(v);
}

/* profiling code (enable with -DRJ_PROFILE) */
#ifdef RJ_PROFILE
#include <sys/time.h>

static long time_ms(void) {
#ifdef Win32
  return 0; /* in Win32 we have no gettimeofday :( */
#else
  struct timeval tv;
  gettimeofday(&tv,0);
  return (tv.tv_usec/1000)+(tv.tv_sec*1000);
#endif
}

long profilerTime;

#define profStart() profilerTime=time_ms()
static void profReport(char *fmt, ...) {
  long npt=time_ms();
  va_list v;
  va_start(v,fmt);
  vprintf(fmt,v);
  va_end(v);
  printf(" %ld ms\n",npt-profilerTime);
  profilerTime=npt;
}
#define _prof(X) X
#else
#define profStart()
#define _prof(X)
#endif


jintArray jri_putBoolArrayI(JNIEnv *env, SEXP e)
{
    if (TYPEOF(e)!=LGLSXP) return 0;
    _dbg(rjprintf(" logical vector of length %d\n", XLENGTH(e)));
    {
        unsigned len=LENGTH(e);
        jintArray da=(*env)->NewIntArray(env,len);
        jint *dae;

        if (!da) {
            jri_error("newIntArray.new(%d) failed",len);
            return 0;
        }
        
        if (len>0) {
            dae=(*env)->GetIntArrayElements(env, da, 0);
            if (!dae) {
                (*env)->DeleteLocalRef(env,da);
                jri_error("newIntArray.GetIntArrayElements failed");
                return 0;
            }
            memcpy(dae,INTEGER(e),sizeof(jint)*len);
            (*env)->ReleaseIntArrayElements(env, da, dae, 0);
        }
        return da;
    }
}

jintArray jri_putIntArray(JNIEnv *env, SEXP e)
{
    if (TYPEOF(e)!=INTSXP) return 0;
    _dbg(rjprintf(" integer vector of length %d\n", XLENGTH(e)));
    {
        unsigned len=LENGTH(e);
        jintArray da=(*env)->NewIntArray(env,len);
        jint *dae;

        if (!da) {
            jri_error("newIntArray.new(%d) failed",len);
            return 0;
        }
        
        if (len>0) {
            dae=(*env)->GetIntArrayElements(env, da, 0);
            if (!dae) {
                (*env)->DeleteLocalRef(env,da);
                jri_error("newIntArray.GetIntArrayElements failed");
                return 0;
            }
            memcpy(dae,INTEGER(e),sizeof(jint)*len);
            (*env)->ReleaseIntArrayElements(env, da, dae, 0);
        }
        return da;
    }
}

jdoubleArray jri_putDoubleArray(JNIEnv *env, SEXP e)
{
    if (TYPEOF(e)!=REALSXP) return 0;
    _dbg(rjprintf(" real vector of length %d\n", XLENGTH(e)));
    {
        unsigned len=LENGTH(e);
        jdoubleArray da=(*env)->NewDoubleArray(env,len);
        jdouble *dae;

        if (!da) {
            jri_error("newDoubleArray.new(%d) failed",len);
            return 0;
        }
        if (len>0) {
            dae=(*env)->GetDoubleArrayElements(env, da, 0);
            if (!dae) {
                (*env)->DeleteLocalRef(env,da);
                jri_error("newDoubleArray.GetDoubleArrayElements failed");
                return 0;
            }
            memcpy(dae,REAL(e),sizeof(jdouble)*len);
            (*env)->ReleaseDoubleArrayElements(env, da, dae, 0);
        }
        return da;
    }
}

jbyteArray jri_putStringUtf8(JNIEnv *env, SEXP e, int ix) {
	if (TYPEOF(e) != STRSXP || XLENGTH(e) <= ix) {
		return NULL;
	}
	jbyteArray s= newJUtf8StringS(env, STRING_ELT(e, ix));
	if (!s || s == joByteArrayNA) {
		return NULL;
	}
	return s;
}

jobjectArray jri_putStringUtf8Array(JNIEnv *env, SEXP e) {
	if (TYPEOF(e) != STRSXP) {
		return NULL;
	}
	int l= LENGTH(e);
	jobjectArray sa= (*env)->NewObjectArray(env, l, jcByteArray, NULL);
	if (!sa) {
		handleJError(env, RJ_ERROR_CWARNING, "putStringUtf8Array: jni.NewObjectArray(%d) failed.", l);
		return NULL;
	}
	for (int i= 0; i < l; i++) {
		jbyteArray s= newJUtf8StringS(env, STRING_ELT(e, i));
		if (!s) {
			return NULL;
		}
		if (s != joByteArrayNA) {
			(*env)->SetObjectArrayElement(env, sa, i, s);
		}
	}
	return sa;
}

jbyteArray jri_putByteArray(JNIEnv *env, SEXP e)
{
    if (TYPEOF(e) != RAWSXP) return 0;
    _dbg(rjprintf(" raw vector of length %d\n", XLENGTH(e)));
    {
        unsigned len = LENGTH(e);
        jbyteArray da = (*env)->NewByteArray(env,len);
        jbyte *dae;

        if (!da) {
            jri_error("newByteArray.new(%d) failed",len);
            return 0;
        }
        
        if (len > 0) {
            dae = (*env)->GetByteArrayElements(env, da, 0);
            if (!dae) {
                (*env)->DeleteLocalRef(env, da);
                jri_error("newByteArray.GetByteArrayElements failed");
                return 0;
            }
            memcpy(dae, RAW(e), len);
            (*env)->ReleaseByteArrayElements(env, da, dae, 0);
        }
        return da;
    }
}

jlongArray jri_putSEXPLArray(JNIEnv *env, SEXP e)
{
    _dbg(rjprintf(" general vector of length %d\n", XLENGTH(e)));
    {
        unsigned len=LENGTH(e);
        jlongArray da=(*env)->NewLongArray(env,len);
        jlong *dae;
        
        if (!da) {
            jri_error("newLongArray.new(%d) failed",len);
            return 0;
        }
        
        if (len>0) {
            int i=0;
            
            dae=(*env)->GetLongArrayElements(env, da, 0);
            if (!dae) {
                (*env)->DeleteLocalRef(env,da);
                jri_error("newLongArray.GetLongArrayElements failed");
                return 0;
            }
            while (i<len) {
                dae[i] = SEXP2L(VECTOR_ELT(e, i));
                i++;
            }
            (*env)->ReleaseLongArrayElements(env, da, dae, 0);
        }
        return da;
    }
}

jbyteArray jri_putSymbolNameUtf8(JNIEnv *env, SEXP e) {
	if (TYPEOF(e) != SYMSXP) {
		return NULL;
	}
	return newJUtf8StringS(env, PRINTNAME(e));
}


/** get contents of the integer array object (int) into a logical R vector */
SEXP jri_getBoolArrayI(JNIEnv *env, jintArray array) {
	SEXP ar;
	jint *ap;
	
	profStart();
	_dbg(rjprintf(" jarray %d\n",o));
	if (!array) return R_NilValue;
	int l= (int)(*env)->GetArrayLength(env, array);
	_dbg(rjprintf("convert int array of length %d into R bool\n",l));
	ap= (jint*)(*env)->GetIntArrayElements(env, array, 0);
	if (!ap) {
		jri_error("RgetBoolArrayICont: can't fetch array contents");
		return 0;
	}
	ar= Rf_allocVector(LGLSXP, l);
	memcpy(LOGICAL(ar), ap, sizeof(jint) * l);
	(*env)->ReleaseIntArrayElements(env, array, ap, JNI_ABORT);
	_prof(profReport("RgetBoolArrayICont[%d]:",o));
	return ar;
}

/** get contents of the boolean array object into a logical R vector */
SEXP jri_getBoolArray(JNIEnv *env, jbooleanArray array) {
	SEXP ar;
	jboolean *ap;
	
	profStart();
	_dbg(rjprintf(" jarray %d\n",o));
	if (!array) return R_NilValue;
	int l= (int)(*env)->GetArrayLength(env, array);
	_dbg(rjprintf("convert boolean array of length %d into R bool\n",l));
	ap= (jboolean*)(*env)->GetBooleanArrayElements(env, array, 0);
	if (!ap) {
		jri_error("RgetBoolArrayCont: can't fetch array contents");
		return 0;
	}
	ar= Rf_allocVector(LGLSXP, l);
	{	int *lgl= LOGICAL(ar);
		for (int i= 0; i < l; i++) {
			lgl[i]= ap[i] ? 1 : 0;
		}
	}
	(*env)->ReleaseBooleanArrayElements(env, array, ap, JNI_ABORT);
	_prof(profReport("RgetBoolArrayCont[%d]:",o));
	return ar;
}

/** get contents of the integer array object (int) */
SEXP jri_getIntArray(JNIEnv *env, jintArray array) {
	SEXP ar;
	jint *ap;
	
	profStart();
	_dbg(rjprintf(" jarray %d\n",o));
	if (!array) return R_NilValue;
	int l= (int)(*env)->GetArrayLength(env, array);
	_dbg(rjprintf("convert int array of length %d\n",l));
	ap= (jint*)(*env)->GetIntArrayElements(env, array, 0);
	if (!ap) {
		jri_error("RgetIntArrayCont: can't fetch array contents");
		return 0;
	}
	ar= Rf_allocVector(INTSXP, l);
	memcpy(INTEGER(ar), ap, sizeof(jint) * l);
	(*env)->ReleaseIntArrayElements(env, array, ap, JNI_ABORT);
	_prof(profReport("RgetIntArrayCont[%d]:",o));
	return ar;
}

/** get contents of the double array object (int) */
SEXP jri_getDoubleArray(JNIEnv *env, jdoubleArray array) {
	SEXP ar;
	jdouble *ap;
	
	profStart();
	_dbg(rjprintf(" jarray %d\n",o));
	if (!array) return R_NilValue;
	int l= (int)(*env)->GetArrayLength(env, array);
	_dbg(rjprintf("convert double array of length %d\n",l));
	ap= (jdouble*)(*env)->GetDoubleArrayElements(env, array, 0);
	if (!ap) {
		jri_error("RgetDoubleArrayCont: can't fetch array contents");
		return 0;
	}
	ar= Rf_allocVector(REALSXP, l);
	memcpy(REAL(ar), ap, sizeof(jdouble) * l);
	(*env)->ReleaseDoubleArrayElements(env, array, ap, JNI_ABORT);
	_prof(profReport("RgetDoubleArrayCont[%d]:",o));
	return ar;
}

/** jobjRefInt object : string */
SEXP jri_getStringUtf8(JNIEnv *env, jbyteArray s) {
	if (!s) {
		return Rf_ScalarString(R_NaString);
	}
	const size_t length= (*env)->GetArrayLength(env, s);
	if (!length) {
		return Rf_ScalarString(R_BlankString);
	}
	profStart();
	const char *c= (const char *)(*env)->GetByteArrayElements(env, s, NULL);
	if (!c) {
		jri_error("jri_getString: can't retrieve string content");
		return R_NilValue;
	}
	SEXP r;
	PROTECT(r= Rf_allocVector(STRSXP, 1));
	SET_STRING_ELT(r, 0, Rf_mkCharLenCE(c, length, CE_UTF8));
	UNPROTECT(1);
	(*env)->ReleaseByteArrayElements(env, s, (jbyte *)c, JNI_ABORT);
	_prof(profReport("jri_getStringUtf8:"));
	return r;
}

/** get contents of the object array in the form of int* */
SEXP jri_getStringUtf8Array(JNIEnv *env, jobjectArray array) {
	SEXP ar;
	
	profStart();
	_dbg(rjprintf(" jarray %d\n",o));
	if (!array) {
		return R_NilValue;
	}
	const int l= (int)(*env)->GetArrayLength(env, array);
	_dbg(rjprintf("convert string array of length %d\n",l));
	PROTECT(ar= Rf_allocVector(STRSXP, l));
	for (int i= 0; i < l; i++) {
		jbyteArray s= (jbyteArray)((*env)->GetObjectArrayElement(env, array, i));
		if (!s) {
			SET_STRING_ELT(ar, i, R_NaString); /* this is probably redundant since the vector is pre-filled with NAs, but just in case ... */
			continue;
		}
		const size_t length= (*env)->GetArrayLength(env, s);
		if (!length) {
			SET_STRING_ELT(ar, i, R_BlankString);
			continue;
		}
		const char *c= (const char *)(*env)->GetByteArrayElements(env, s, NULL);
		if (!c) {
			jri_error("jri_getStringArray: can't retrieve string content");
			SET_STRING_ELT(ar, i, R_NaString); // ?
			continue;
		}
		SET_STRING_ELT(ar, i, Rf_mkCharLenCE(c, length, CE_UTF8));
		(*env)->ReleaseByteArrayElements(env, s, (jbyte *)c, JNI_ABORT);
	}
	UNPROTECT(1);
	_prof(profReport("jri_getStringUtf8Array[%d]:", o));
	return ar;
}

/** get contents of the integer array object (int) */
SEXP jri_getByteArray(JNIEnv *env, jbyteArray array) {
	SEXP ar;
	jbyte *ap;
	
	profStart();
	_dbg(rjprintf(" jarray %d\n",o));
	if (!array) return R_NilValue;
	int l= (int)(*env)->GetArrayLength(env, array);
	_dbg(rjprintf("convert byte array of length %d\n",l));
	ap= (jbyte*)(*env)->GetByteArrayElements(env, array, 0);
	if (!ap) {
		jri_error("jri_getByteArray: can't fetch array contents");
		return 0;
	}
	ar= Rf_allocVector(RAWSXP, l);
	memcpy(RAW(ar), ap, l);
	(*env)->ReleaseByteArrayElements(env, array, ap, JNI_ABORT);
	_prof(profReport("RgetByteArrayCont[%d]:",o));
	return ar;
}

SEXP jri_getSEXPLArray(JNIEnv *env, jlongArray array) {
	SEXP ar;
	jlong *ap;
	
	profStart();
	_dbg(rjprintf(" jarray %d\n",o));
	if (!array) return R_NilValue;
	int l= (int)(*env)->GetArrayLength(env, array);
	_dbg(rjprintf("convert SEXPL array of length %d\n",l));
	ap= (jlong*)(*env)->GetLongArrayElements(env, array, 0);
	if (!ap) {
		jri_error("getSEXPLArray: can't fetch array contents");
		return 0;
	}
	PROTECT(ar= Rf_allocVector(VECSXP, l));
	for (int i= 0; i < l; i++) {
		SET_VECTOR_ELT(ar, i, L2SEXP(ap[i]));
	}
	UNPROTECT(1);
	(*env)->ReleaseLongArrayElements(env, array, ap, JNI_ABORT);
	_prof(profReport("jri_getSEXPLArray[%d]:",o));
	return ar;
}

SEXP jri_installSymbolUtf8(JNIEnv *env, jbyteArray s) {
	if (!s) {
		return R_NilValue;
	}
	const size_t length= (*env)->GetArrayLength(env, s);
	if (!length) {
		return R_NilValue;
	}
	profStart();
	const char *c= (const char *)(*env)->GetByteArrayElements(env, s, NULL);
	if (!c) {
		jri_error("jri_installString: can't retrieve string content");
		return R_NilValue;
	}
	SEXP r;
	r= Rf_installTrChar(Rf_mkCharLenCE(c, length, CE_UTF8));
	(*env)->ReleaseByteArrayElements(env, s, (jbyte *)c, JNI_ABORT);
	_prof(profReport("jri_installSymbolUtf8:"));
	return r;
}


void jri_checkExceptions(JNIEnv *env, int describe)
{
    jthrowable t=(*env)->ExceptionOccurred(env);
    if (t) {
#ifndef JRI_DEBUG
        if (describe)
#endif
            (*env)->ExceptionDescribe(env);
        (*env)->ExceptionClear(env);
    }
}
