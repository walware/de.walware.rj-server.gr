#ifndef __JRI_H__
#define __JRI_H__

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <R.h>
#include <Rinternals.h>
#include <Rdefines.h>
#include <Rversion.h>
#include <jni.h>

#include <stdint.h>


/* the viewpoint is from R, i.e. "get" means "Java->R" whereas "put" means "R->Java" */

#define JRI_VERSION 0x0507 /* JRI v0.5-7 */
#define JRI_API     0x010a /* API-version 1.10 */

#ifdef __cplusplus
extern "C" {
#endif

/* jlong can always hold a pointer to avoid warnings we go ptr -> uintptr_t -> jlong */
#define SEXP2L(s) ((jlong)((uintptr_t)(s)))
#define L2SEXP(s) ((SEXP)((uintptr_t)(s)))


jintArray jri_putBoolArrayI(JNIEnv *env, SEXP e);
jintArray jri_putIntArray(JNIEnv *env, SEXP e);
jdoubleArray jri_putDoubleArray(JNIEnv *env, SEXP e);
jbyteArray jri_putStringUtf8(JNIEnv *env, SEXP e, int ix); /* ix=index, 0=1st */
jobjectArray jri_putStringUtf8Array(JNIEnv *env, SEXP e);
jbyteArray jri_putByteArray(JNIEnv *env, SEXP e);

jlongArray jri_putSEXPLArray(JNIEnv *env, SEXP e); /* SEXPs are strored as "long"s */

jbyteArray jri_putSymbolNameUtf8(JNIEnv *env, SEXP e);

SEXP jri_getBoolArrayI(JNIEnv *env, jintArray array);
SEXP jri_getBoolArray(JNIEnv *env, jbooleanArray array);
SEXP jri_getIntArray(JNIEnv *env, jintArray array);
SEXP jri_getDoubleArray(JNIEnv *env, jdoubleArray array);
SEXP jri_getStringUtf8(JNIEnv *env, jbyteArray s);
SEXP jri_getStringUtf8Array(JNIEnv *env, jobjectArray array);
SEXP jri_getByteArray(JNIEnv *env, jbyteArray array);

SEXP jri_getSEXPLArray(JNIEnv *env, jlongArray array);

SEXP jri_installSymbolUtf8(JNIEnv *env, jbyteArray s); /* as Rf_install, just for Java strings */


void jri_checkExceptions(JNIEnv *env, int describe);

void jri_error(char *fmt, ...);


#ifdef __cplusplus
}
#endif

#endif

/*
   API version changes:
 -----------------------
   1.3 (initial public API version)
 [ 1.4 never publicly released - added put/getenv but was abandoned ]
   1.5 JRI 0.3-0
       + rniGetTAG
       + rniInherits
       + rniGetSymbolName
       + rniInstallSymbol
       + rniJavaToXref, rniXrefToJava
   1.6 JRI 0.3-2
       + rniPutBoolArray, rniPutBoolArrayI, rniGetBoolArrayI
   1.7 JRI 0.3-7
       + rniCons(+2 args)
   1.8 JRI 0.4-0
       + rniPrint
   1.9 JRI 0.4-3
       + rniPreserve, rniRelease
       + rniParentEnv, rniFindVar, rniListEnv
       + rniSpecialObject(0-7)
       + rniPrintValue
    1.10 JRI 0.5-1
       * rniAssign returns jboolean instead of void
*/
