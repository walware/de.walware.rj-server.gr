# Contributing to RJSrv for GNU R

Thanks for your interest in this project.

  * Project home: https://gitlab.com/walware/de.walware.rj-server.gr


## Build
### Create R Source Packages

#### Prerequisite:

  - Maven (3.6.3)
  - R
  - The repository/project code of RJSrv for GNU R

#### Prepare the R Package Sources

To generate the R package sources, run in the project root ('de.walware.rj-server.gr'):

```
mvn package
```

The R package sources are now located in:
  * rj: `de.walware.rj.server.gr.rsrv/target/rpkg-sources`
  * rj.gd: `de.walware.rj.server.gr.rgd/target/rpkg-sources`

#### Build R Source Packages

The R source packages can be created from the R package sources listed above using R as usual:

```
R CMD build de.walware.rj.server.gr.rsrv/target/rpkg-sources
R CMD build de.walware.rj.server.gr.rgd/target/rpkg-sources
```
